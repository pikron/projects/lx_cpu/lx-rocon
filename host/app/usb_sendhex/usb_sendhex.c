/* usb_sendhex - program for manage device firmware by USB
 * R.Bartosinski <bartosr@centrum.cz> (C)2004
 *
 * Based on 'ul_sendhex'
 *
 * Version 1.1 - 2013/08/26
 */
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <usb.h>
#include <sys/time.h>
#include <stdint.h>
#include <endian.h>

#if !defined(_WIN32) && !defined(__DJGPP__) && !defined(HAS_GETDELIM)
#define HAS_GETDELIM
#endif
#define HAS_GETOPT_LONG

#define USB_DEV_VID 0xDEAD
#define USB_DEV_PID 0x1000

#define USB_TIMEOUT 500

// USB vendor defines

#define USB_VENDOR_GET_CAPABILITIES  0x00 // get capabilities
#define USB_VENDOR_RESET_DEVICE      0x08
// #define USB_VENDOR_SET_BYTE          0x10
// #define USB_VENDOR_SET_WORD          0x20
#define USB_VENDOR_GET_SET_MEMORY    0x30
#define USB_VENDOR_ERASE_MEMORY      0x40 // erase memory for 1 Byte
#define USB_VENDOR_ERASE_1KB_MEMORY  0x48 // erase memory for 1 KB
#define USB_VENDOR_MASS_ERASE        0x50 // erase all device memory
#define USB_VENDOR_GOTO              0x60
#define USB_VENDOR_CALL              0x70
#define USB_VENDOR_GET_STATUS        0xF0
#define USB_VENDOR_MASK              0xF8 // mask for vendor commands

#define USB_VENDOR_MEMORY_BY_BULK    0x80

int vid = USB_DEV_VID;
int pid = USB_DEV_PID;
int upload_flg = 0;
int mem_type   = 2;
unsigned long mem_start  = 0;
unsigned long mem_length = 0xff00;
int mem_length_flg = 0;
unsigned long max_block  = 1024;
unsigned long go_addr    = 3;
int go_flg     = 0;
int call       = 0xFFFF;
int arg        = 0;
int call_flg   = 0;
int reset_flg  = 0;
int prt_modules = 0;
int debugk     = 0;
int debugk_flg = 0;
int verbose    = 0;
int wait_for_device_flg = 0;
char *file_format = NULL;
int regerase_flg = 0;
int masserase_flg = 0;
unsigned long masserase_mode = 0;
int fill_flg;
void *fill_pat_val;
int fill_pat_len;
int val_bytes = 1;
int val_endian = 0;

/*****************************************************************************/

typedef struct tform_file
{
  unsigned char *buf;
  int   buf_len;
  int   buf_addr;
  int   buf_bytes;
  FILE *file;
  unsigned char *line_buf;
  int  line_addr;
  int  ext_base_addr;
  int  line_bytes;
  int  line_offs;
  int  start_addr;
  int  limit_addr;
  int (*read)(struct tform_file *tform);
  int (*done)(struct tform_file *tform);
} tform_file;

/*****************************************************************************/

#if __BYTE_ORDER == __BIG_ENDIAN
uint16_t usb_swab16(uint16_t x)
{
  return x << 8 | x >> 8;
}
#else
uint16_t usb_swab16(uint16_t x)
{
  return x;
}
#endif

#ifndef HAS_GETDELIM
int getdelim(char **line, size_t *linelen, char delim, FILE *F)
{
  char c;
  size_t l = 0;

  do
  {
    if (l + 1 >= *linelen)
    {
      *linelen = l + 20;

      if (!*line)
        *line = (char *)malloc(*linelen);
      else
        *line = (char *)realloc(*line, *linelen);
    }

    c = fgetc(F);

    if (feof(F))
    {
      if (l)
        break;
      else
        return -1;
    }

    if (c != '\r')
      (*line)[l++] = c;
  }
  while (c != delim);

  (*line)[l] = 0;
  return l;
}
#endif

int get_hex(char **p, unsigned *v, int chars)
{
  unsigned u = 0;
  char c;
  *v = 0;

  while (**p == ' ')
    (*p)++;

  while (chars--)
  {
    u <<= 4;
    c = **p;

    if ((c >= '0') && (c <= '9'))
      u += c - '0';
    else if ((c >= 'A') && (c <= 'F'))
      u += c - 'A' + 10;
    else
      return -1;

    (*p)++;
  };

  *v = u;

  return 0;
}

int tform_init(tform_file *tform, int buf_len)
{
  if (!buf_len) buf_len = 1024;

  tform->file = NULL;
  tform->buf_len = buf_len;
  tform->buf = malloc(tform->buf_len);
  tform->buf_addr = 0;
  tform->line_buf = NULL;
  tform->line_offs = 0;
  tform->line_bytes = 0;
  tform->start_addr = -1;
  tform->buf_bytes = 0;
  tform->read = NULL;
  tform->done = NULL;
  tform->ext_base_addr = 0;
  return 0;
}

int tform_done(tform_file *tform)
{
  if (tform->done)
    return tform->done(tform);

  if (tform->file != NULL)
    fclose(tform->file);

  if (tform->buf)
    free(tform->buf);

  if (tform->line_buf)
    free(tform->line_buf);

  return 0;
};

int tform_read(tform_file *tform)
{
  return tform->read(tform);
}

int tform_read_ihex(tform_file *tform)
{
  int cn, len = 0;
  int addr = 0;
  unsigned u, v;
  char *p;
  unsigned char *r;
  char *line = NULL;
  size_t line_len = 0;

  while (len < tform->buf_len)
  {
    if (!tform->line_bytes)
    {
      int checksum = 0;
      int ihex_type = 0;

      tform->line_offs = 0;

      if (getdelim(&line, &line_len, '\n', tform->file) == -1)
        break;

      p = line;

      if (*p++ != ':')
        printf("tform_read : strange line %s\n", line);
      else
      {
        if (get_hex(&p, &u, 2) < 0)
        {
          printf("tform_read_ihex : bad ihex cnt\n");
          return -1;
        }

        checksum += cn = tform->line_bytes = u;

        if (!tform->line_buf)
          tform->line_buf = malloc(cn);
        else
          tform->line_buf = realloc(tform->line_buf, cn);

        if (get_hex(&p, &u, 2) < 0)
        {
          printf("tform_read_ihex : bad ihex addr\n");
          return -1;
        }

        if (get_hex(&p, &v, 2) < 0)
        {
          printf("tform_read_ihex : bad ihex addr\n");
          return -1;
        }

        checksum += u + v;
        tform->line_addr = (u << 8) + v;

        if (get_hex(&p, &u, 2) < 0)
        {
          printf("tform_read_ihex : bad ihex type\n");
          return -1;
        }

        checksum += ihex_type = u;

        if ((ihex_type >= 0) && (ihex_type <= 5))
        {
          r = tform->line_buf;

          while (cn--)
          {
            if (get_hex(&p, &u, 2) < 0)
            {
              printf("tform_read_ihex : bad ihex data\n");
              return -1;
            }

            checksum += *r++ = u;
          }

          if (get_hex(&p, &u, 2) < 0)
          {
            printf("tform_read_ihex : bad ihex csum\n");
            return -1;
          }

          checksum += u;

          if (checksum & 0xff)
          {
            printf("tform_read_ihex : error ihex csum %d\n",
                   checksum);
            return -1;
          }

          while ((u = *p++)) if (u != ' ' && u != '\n' && u != '\r')
          {
            printf("tform_read_ihex : residual chars on line\n");
            return -1;
          }
        }

        if (ihex_type == 1)
        {
          tform->line_bytes = 0;
          if(tform->start_addr == -1)
            tform->start_addr = tform->line_addr;
        }

        tform->line_addr+=tform->ext_base_addr;

        if((ihex_type >= 2) && (ihex_type <= 5))
        {
          cn = tform->line_bytes;
          r = tform->line_buf;
          addr = 0;

          while(cn-- > 0)
          {
            addr <<= 8;
            addr += *(r++);
          }
          if(ihex_type == 2)
            tform->ext_base_addr=addr << 4;
          else if(ihex_type == 4)
            tform->ext_base_addr = addr << 16;
          else if(ihex_type == 5)
            tform->start_addr = addr;

          tform->line_bytes = 0;
        }
      }
    }

    if (tform->line_bytes)
    {
      if (!len)
        addr = tform->buf_addr = tform->line_addr + tform->line_offs;
      else if (addr != tform->line_addr + tform->line_offs)
        break;

      cn = tform->line_bytes - tform->line_offs;

      if (cn + len > tform->buf_len) cn = tform->buf_len - len;

      memcpy(tform->buf + len, tform->line_buf + tform->line_offs, cn);
      len += cn;
      addr += cn;
      tform->line_offs += cn;

      if (tform->line_bytes == tform->line_offs)
        tform->line_bytes = 0;
    }
  }

  tform->buf_bytes = len;

  return len;
}

int tform_read_binary(tform_file *tform)
{
  int len = 0;

  tform->buf_addr += tform->buf_bytes;
  len = fread(tform->buf, 1, tform->buf_len, tform->file);

  if (len < 0)
  {
    perror("tform_read_binary : read error");
    return -1;
  }

  tform->buf_bytes = len;
  return len;
}

int tform_open(tform_file *tform, char *file_name,
               char *format, int buf_len, int wr_fl)
{
  FILE *file;

  if (!format || !strcmp("ihex", format))
  {
    if ((file = fopen(file_name, "r")) == NULL)
    {
      perror("download_file : hex file open");
      return -1;
    }

    tform_init(tform, buf_len);
    tform->file = file;
    tform->read = tform_read_ihex;
  }
  else if (!strcmp("binary", format))
  {
    if ((file = fopen(file_name, "rb")) == NULL)
    {
      perror("download_file : binary file open");
      return -1;
    };

    tform_init(tform, buf_len);
    tform->file = file;
    tform->read = tform_read_binary;
  }
  else
  {
    fprintf(stderr, "requested unknown format %s\n", format);
    return -1;
  }

  return 1;
}

/*****************************************************************************/

int tform_read_pattern_data(tform_file *tform)
{
  int len = 0;
  int l;
  unsigned char *bptr = tform->buf;
  unsigned char *bend;

  tform->buf_addr += tform->buf_bytes;
  len = tform->buf_len;
  if (len > tform->limit_addr - tform->buf_addr)
    len = tform->limit_addr - tform->buf_addr;
  if (!len)
    return 0;

  bend = bptr + len;
  do {
    l = tform->line_bytes - tform->line_offs;
    if (l > bend - bptr)
      l = bend - bptr;
    memcpy(bptr, tform->line_buf + tform->line_offs, l);
    bptr += l;
    tform->line_offs += l;
    if (tform->line_offs == tform->line_bytes)
      tform->line_offs = 0;
  } while (bptr != bend);

  tform->buf_bytes = len;
  return len;
}

int tform_setup_pattern(tform_file *tform, unsigned long len, int buf_len,
                        void *fill_pat_val, int fill_pat_len)
{
  tform_init(tform, buf_len);
  tform->file = NULL;
  tform->read = tform_read_pattern_data;
  tform->line_buf = fill_pat_val;
  tform->line_bytes = fill_pat_len;
  tform->line_offs = 0;
  tform->limit_addr = len;
  return 1;
}

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/* USB functions */
void print_devices(void)
{
  struct usb_bus *bus;
  struct usb_device *dev;
  int i = 0;

  usb_init(); // NO for more devices
  usb_find_busses();
  usb_find_devices();

  printf("All connected usb devices\n");
  printf("  bus/device    idVendor/idProduct\n");

  for (bus = usb_busses; bus; bus = bus->next)
  {
    for (dev = bus->devices; dev; dev = dev->next)
    {
      i++;
      printf("    %s/%s     0x%04X/0x%04X\n", bus->dirname, dev->filename, dev->descriptor.idVendor, dev->descriptor.idProduct);
    }
  }

  if (!i)
    printf(" -- no device.\n");
}

struct usb_device *find_usb_device(int vendor, int product)
{
  struct usb_bus *bus;
  struct usb_device *dev;

  for (bus = usb_busses; bus; bus = bus->next)
  {
    for (dev = bus->devices; dev; dev = dev->next)
    {
      if ((dev->descriptor.idVendor == vendor) &&
          (dev->descriptor.idProduct) == product)
        return dev;
    }
  }

  return NULL;
}

usb_dev_handle *usb_open_device(int uvid, int upid)
{
  struct usb_device *dev;
  usb_dev_handle *hdev;

  usb_init(); // NO for more devices
  usb_find_busses();
  usb_find_devices();

  dev = find_usb_device(uvid, upid);

  if (!dev)
  {
    if (verbose)
      printf("!!! Cannot find device 0x%04X:0x%04X\n", uvid, upid);

    return NULL;
  }

  if ((hdev = usb_open(dev)) == NULL)
  {
    if (verbose)
      printf("!!! USB device wasn't opened !!!\n");

    return NULL;
  }

  usb_claim_interface(hdev, 0);

  if (verbose)
    printf(" USB Device 0x%04X:0x%04X '%s' is open.\n", uvid, upid, dev->filename);

  return hdev;
}

int usb_close_device(usb_dev_handle *hdev)
{
  int bRes = 1;
  usb_release_interface(hdev, 0);
  bRes = usb_close(hdev);

  if (bRes && verbose)
    printf("!!! USB Device wasn't closed !!!\n");

  return bRes;
}

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

int download_data(tform_file *tform)
{
  usb_dev_handle *hdev;
  int len;
  int i;
  int ret = 0;
//  int stamp;
  unsigned long shift_addr = 0;

  struct timeval time1, time2;
  struct timezone tz;

  shift_addr = mem_start;

  hdev = usb_open_device(vid, pid);

  if (!hdev)
  {
    perror("download_file : usb device open failed");
    return -1;
  }

  /* ul_drv_debflg(ul_fd,0x11); */ /* 0x9 0x11 */

  gettimeofday(&time1, &tz);

  do
  {
    len = tform_read(tform);

    if (!len) break;

    if (len < 0)
    {
      perror("download_file : ihex");
      ret = -1;
      break;
    }

    printf("addr %4lX len %4X\n", tform->buf_addr + shift_addr, len);

    /* send data */

    //  if((stamp=ul_new_memrq_write(ul_fd,module,mem_type,
    //     tform.buf_addr+shift_addr,len,tform.buf))<0)
    //  { printf("download_file : send message error\n");
    //    ret=-1; break;
    //  };
    i = 3;

    do
    {
      ret = usb_control_msg(hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, USB_VENDOR_GET_SET_MEMORY | mem_type /*USB_VENDOR_TARGET_XDATA*/,
                            (unsigned long)(tform->buf_addr + shift_addr) & 0xffff, (((unsigned long)(tform->buf_addr + shift_addr)) >> 16) & 0xffff, (void *)tform->buf, len, 150 + len); //USB_TIMEOUT);
//      ret = usb_bulk_write( hdev, USB_ENDPOINT_IN | 0x02, buf, len, 1000);

      if (verbose && ret < 0)  printf("Mem write error %d - again\n", ret);

      i--;
    }
    while (ret < 0 && i);

  }
  while (1);

  gettimeofday(&time2, &tz);

  if (verbose)
  {
    long dus = (time2.tv_sec * 1000000 + time2.tv_usec) - (time1.tv_sec * 1000000 + time1.tv_usec);
    printf("Upload time %lu.%lu s\n", dus / 1000000, dus % 1000000);
  }


  if (tform->start_addr != -1)
    printf("Found start address %4X\n", tform->start_addr);

  usb_close_device(hdev);
  return ret;
}

int download_file(char *file_name, char *format)
{
  tform_file tform;
  int ret;

  if (tform_open(&tform, file_name, format, max_block, 0) < 0)
    return -1;

  ret = download_data(&tform);

  tform_done(&tform);
  return ret;
}

int pattern_fill(unsigned long len, void *fill_pat_val, int fill_pat_len)
{
  tform_file tform;
  int ret;

  if (tform_setup_pattern(&tform, len, max_block,
                          fill_pat_val, fill_pat_len) < 0)
    return -1;

  ret = download_data(&tform);

  tform_done(&tform);
  return ret;
}

int send_cmd_call(int cmd, int val)
{
  int ret, resp_val;
  usb_dev_handle *hdev;
  char resp[10];

  hdev = usb_open_device(vid, pid);

  if (!hdev)
  {
    perror("send_cmd_call : USB open failed");
    return -1;
  }

  ret = usb_control_msg(hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
                        USB_VENDOR_CALL, val & 0xffff, cmd & 0xffff, resp, sizeof(resp), USB_TIMEOUT);
  resp_val = usb_swab16(*((uint16_t *)(resp)));

  if (ret < 0)
    printf("Call %4X (%4X) ERROR %d: %s\n", (uint16_t)(cmd & 0xffff), (uint16_t)(val & 0xffff), ret, usb_strerror());
  else
    printf("Call %4X (%4X): %4X\n", (uint16_t)(cmd & 0xffff), (uint16_t)(val & 0xffff), resp_val);

  usb_close_device(hdev);
  return ret;
}

int send_cmd_go(unsigned long addr)
{
  int ret;
  usb_dev_handle *hdev;

  hdev = usb_open_device(vid, pid);

  if (!hdev)
  {
    perror("send_cmd_go : USB open failed");
    return -1;
  }

  ret = usb_control_msg(hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
                        USB_VENDOR_GOTO, addr & 0xffff, (addr >> 16) & 0xffff, NULL, 0, USB_TIMEOUT);

  if (ret < 0) printf("Goto to %4lX ERROR %d: %s\n", addr, ret, usb_strerror());
  else printf("Goto to %4lX OK\n", addr);

  usb_close_device(hdev);
  return ret;
}

int send_cmd_reset()
{
  int ret;
  usb_dev_handle *hdev;

  hdev = usb_open_device(vid, pid);

  if (!hdev)
  {
    perror("send_cmd_reset : USB open failed");
    return -1;
  }

  ret = usb_control_msg(hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
                        USB_VENDOR_RESET_DEVICE, 0, 0, NULL, 0, USB_TIMEOUT);

  if (ret < 0)
    printf("Reset device ERROR %d: %s\n", ret, usb_strerror());
  else
    printf("Reset OK\n");

  usb_close_device(hdev);
  return ret;
};


int send_cmd_regerase(unsigned long addr, unsigned long len)
{
  int ret;
  usb_dev_handle *hdev;

  hdev = usb_open_device(vid, pid);

  if (!hdev)
  {
    perror("send_cmd_regerase : USB open failed");
    return -1;
  }

  if (addr + len < 0x10000)
  {
    ret = usb_control_msg(hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
                          USB_VENDOR_ERASE_MEMORY, addr, len, NULL, 0, USB_TIMEOUT * 5);
  }
  else
  {
    len += addr & 0x3ff;
    ret = usb_control_msg(hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
                          USB_VENDOR_ERASE_1KB_MEMORY, addr >> 10, len >> 10, NULL, 0, USB_TIMEOUT * 10);
  }

  if (ret < 0)
    printf("Region Erase from %4lX ERROR %d: %s\n", addr, ret, usb_strerror());
  else
    printf("Region Erase from %4lX OK\n", addr);

  usb_close_device(hdev);
  return ret;
};



int send_cmd_masserase(unsigned long mode)
{
  int ret;
  usb_dev_handle *hdev;

  hdev = usb_open_device(vid, pid);

  if (!hdev)
  {
    perror("send_cmd_masserase : USB open failed");
    return -1;
  };

  ret = usb_control_msg(hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
                        USB_VENDOR_MASS_ERASE, mode & 0xffff, (mode >> 16) & 0xffff, NULL, 0, USB_TIMEOUT * 20);

  if (ret < 0)
    printf("Mass Erase to %4lX ERROR %d: %s\n", mode, ret, usb_strerror());
  else printf("Mass Erase to %4lX OK\n", mode);

  usb_close_device(hdev);
  return ret;
};

int print_hex_line(FILE *out, void *buf, unsigned long len, int valbytes, int valendian)
{
  volatile unsigned char *p = buf;
  int i;
  unsigned long val;
  if (valbytes > sizeof(val))
    valbytes = sizeof(val);
  if (valbytes == 0)
    valbytes = 1;

  while(len)
  {
    if (valbytes > len)
      valbytes = len;
    len -= valbytes;
    val = 0;
    if (valendian)
    {
      for (i = 0; i < valbytes; i++) {
        val |= (unsigned long)p[i] << ((valbytes - i - 1) * 8);
      }
    }
    else
    {
      for (i = 0; i < valbytes; i++)
      {
        val |= (unsigned long)p[i] << (i * 8);
      }
    }
    if (fprintf(out, len? "%0*lX ": "%0*lX", valbytes * 2, val) < 0)
      return -1;
    p = p + valbytes;
  }
  return 0;
}

int format_to_bytes_and_endian(const char *format, int *pvalbytes,
                               int *pvalendian)
{
  char *p2;

  if (!strncmp("dump", format, 4))
    format = format + 4;

  if ((*format >= '0') && (*format <= '9'))
  {
    *pvalbytes = strtol(format, &p2, 10) / 8;
    format = p2;
  }
  if (*format == 'b')
    *pvalendian = 1;
  if (*format == 'l')
    *pvalendian = 0;
  return 1;
}

int upload_file(char *file_name, char *format)
{
  usb_dev_handle *hdev;
  FILE *file;
  int ret = 0;
  unsigned char buf[0x400];
  unsigned char *p;
  char *mode = "w";
  enum {fmt_ihex, fmt_binary, fmt_dump} fmt;
  int valbytes = 1;
  int valendian = 0;
  int i, l, csum;
  unsigned long mem_adr = mem_start;
  unsigned long mem_len = mem_length;
  unsigned long len;
  unsigned long ext_addr = 0; /* for Intel HEX format */

  struct timeval time1, time2;
  struct timezone tz;

  if (max_block > 0x400)
    max_block = 0x400;

  if (!format || !strcmp("ihex", format))
    fmt = fmt_ihex;
  else if (!strcmp("binary", format))
  {
    mode = "wb";
    fmt = fmt_binary;
  }
  else if (!strncmp("dump", format, 4)) {
    fmt = fmt_dump;
    format_to_bytes_and_endian(format + 4, &valbytes, &valendian);
  }
  else
  {
    fprintf(stderr, "requested unknown format %s\n", format);
    return -1;
  }

  if (!strcmp(file_name, "-"))
    file_name = NULL;

  hdev = usb_open_device(vid, pid);

  if (!hdev)
  {
    perror("upload_file : open failed");
    return -1;
  };

  /* ul_drv_debflg(ul_fd,0x11); */ /* 0x9 0x11 */

  if (file_name)
  {
    if ((file = fopen(file_name, mode)) == NULL)
    {
      perror("upload_file : file open");
      usb_close_device(hdev);
      return -1;
    }
  }
  else file = stdout;

  gettimeofday(&time1, &tz);

//  ret = usb_control_msg( hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, USB_VENDOR_MEMORY_BY_BULK | USB_VENDOR_TARGET_XDATA,
//                            mem_adr & 0xffff, mem_len, NULL, 0, 2000); //USB_TIMEOUT);

  if (ret < 0)
  {
    printf("ERR ctrl msg\n");
    mem_len = 0;
  }

  while (mem_len)
  {
    len = mem_len < max_block ? mem_len : max_block;
    /* read data */
    i = 3;

    do
    {
      ret = usb_control_msg(hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, USB_VENDOR_GET_SET_MEMORY | mem_type /*USB_VENDOR_TARGET_XDATA*/,
                            mem_adr & 0xffff, (mem_adr >> 16) & 0xffff, (void *)buf, len, 100 + len); //USB_TIMEOUT);
//      ret = usb_bulk_read( hdev, USB_ENDPOINT_IN | 0x02, buf, len, 1000);

      if (verbose && ret < 0)
        printf("Mem read error - again\n");

      i--;
    }
    while (ret < 0 && i);

    if (ret < 0)
    {
      printf("Mem read returns %d: %s\n", ret, usb_strerror());
      break;
    }

    if (file_name) printf("%04lX\n", mem_adr);

    switch (fmt)
    {
      case fmt_ihex:
        i = 0;

        while (i < len)
        {
          unsigned long a = mem_adr + i;

          if ((a & ~0xFFFF) != ext_addr)
          {
            unsigned long val;
            unsigned char b;

            ext_addr = a & ~0xFFFF;
            val = ext_addr >> 16;
            l = 2;
            val >>= 16;
            while (val != 0)
            {
              val >>= 8;
              l++;
            }
            fprintf(file, ":%02X000004", l);
            csum = l + 4;
            val = ext_addr >> 16;
            while (l--)
            {
              b = (val >> (8 * l)) & 0xff;
              fprintf(file, "%02X", b);
              csum += b;
            }
            fprintf(file, "%02X\n", (-csum) & 0xFF);
          }

          l = len - i;

          if (l > 16) l = 16;

          csum = l + a + (a >> 8);
          fprintf(file, ":%02X%04lX00", l, a & 0xffff);

          while (l--)
          {
            fprintf(file, "%02X", buf[i]);
            csum += buf[i++];
          };

          fprintf(file, "%02X\n", (-csum) & 0xFF);
        }

        break;

      case fmt_binary:
        if (fwrite(buf, len, 1, file) != 1)
        {
          perror("upload_file : file write");
          return -1;
        }

        break;

      case fmt_dump:
        p = buf;
        l = 16;
        for (i = 0; i < len; i += l, p += l)
        {
          if (l > len - i)
            l = len - i;
          fprintf(file, "%04lX:", mem_adr + i);
          print_hex_line(file, p, l, valbytes, valendian);
          fprintf(file, "\n");
        }
        break;
    }

    mem_adr += len;
    mem_len -= len;
  }

  gettimeofday(&time2, &tz);

  if (verbose)
  {
    long dus = (time2.tv_sec * 1000000 + time2.tv_usec) - (time1.tv_sec * 1000000 + time1.tv_usec);
    printf("Upload time %lu.%lu s\n", dus / 1000000, dus % 1000000);
  }

  if (fmt == fmt_ihex)
    fprintf(file, ":00000001FF\n");

  if (file_name)
    fclose(file);

  usb_close_device(hdev);

  return 0;
};

static int wait_for_device(int timeout)
{
  usb_dev_handle *hdev;

  while (timeout)
  {
    hdev = usb_open_device(vid, pid);

    if (hdev)
      break;

#ifdef _WIN32
    Sleep(1000);
#else
    sleep(1);
#endif
    timeout--;
  }

  return timeout ? 0 : -1;
}

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

int si_long(char **ps, long *val, int base)
{
  char *p;
  *val = strtol(*ps, &p, base);
  if (*ps == p) return -1;
  *ps = p;
  return 1;
}

int si_ulong(char **ps, unsigned long *val, int base)
{
  char *p;
  *val = strtoul(*ps, &p, base);
  if (*ps == p) return -1;
  *ps = p;
  return 1;
}

int add_to_arr(void **pdata, int *plen, int base, char *str,
               int valbytes, int valendian)
{
  char *s = str;
  unsigned long val;
  unsigned char *p;
  int i;
  int dir = valendian? -1: 1;

  if (!valbytes)
    valbytes = 1;
  if (valbytes > 4)
    return -1;

  do{
    while (*s && strchr(", \t:;" ,*s)) s++;
    if (!*s) break;
    if (si_ulong(&s, &val, base) < 0)
    {
      return -1;
    }
    if(*pdata == NULL)
    {
      *plen = 0;
      *pdata = p = malloc(valbytes);
    }
    else
    {
      p = realloc(*pdata, *plen + valbytes);
      if(p == NULL) return -1;
      *pdata = p;
    }

    p += *plen;
    if (dir < 0)
      p += valbytes - 1;
    for(i = valbytes; i--; )
    {
      *p = val;
      val >>= 8;
      p += dir;
    }
    (*plen) += valbytes;
  } while(1);
  return 1;
}

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

static void
usage(void)
{
  printf("Usage: usb_sendhex <parameters> <hex_file>\n");
  printf("  -d, --vid <num>          device vendor id (VID) [0x%04X]\n", USB_DEV_VID);
  printf("  -i, --pid <num>          product id (PID) [0x%04X]\n", USB_DEV_PID);
  printf("  -t, --type <num>         target module memory space\n");
  printf("  -s, --start <addr>       start address of transfer\n");
  printf("  -l, --length <num>       length of upload block\n");
  printf("  -b, --block <num>        maximal block length\n");
  printf("  -F, --fill <pattern>     comma separated values to fill from -s\n");
  printf("  -c, --call <num>         vendor custom call\n");
  printf("  -a, --argument <num>     argument for vendor custom call\n");
  printf("  -g, --go <addr>          start program from address\n");
  printf("  -r, --reset              reset before download\n");
  printf("  -E, --mass-erase <mode>  full device erase\n");
  printf("  -e, --erase		     erase region defined by -s -l\n");
  printf("  -u, --upload             upload memory block [download]\n");
  printf("  -w, --wait               wait for device to be on\n");
  printf("  -f, --format <format>    format of data [file], ihex, binary, dump, dump32be\n");
  printf("  -p, --print              print devices\n");
  printf("      --debug-kernel <m>   flags to debug kernel\n");
  printf("  -v, --verbose            verbose program\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc, char *argv[])
{
  static struct option long_opts[] =
  {
    { "vid", 1, 0, 'd' },
    { "pid", 1, 0, 'i' },
    { "type",  1, 0, 't' },
    { "start", 1, 0, 's' },
    { "length", 1, 0, 'l' },
    { "block", 1, 0, 'b' },
    { "fill",  1, 0, 'F' },
    { "call", 1, 0, 'c' },
    { "argument", 1, 0, 'a' },
    { "go",    1, 0, 'g' },
    { "reset", 0, 0, 'r' },
    { "mass-erase", 1, 0, 'E' },
    { "erase", 0, 0, 'e' },
    { "upload", 0, 0, 'u' },
    { "wait", 0, 0, 'w' },
    { "format", 1, 0, 'f' },
    { "print", 0, 0, 'p' },
    { "verbose", 0, 0, 'v' },
    { "version", 0, 0, 'V' },
    { "help",  0, 0, 'h' },
    { "debug-kernel", 1, 0, 'D' },
    { 0, 0, 0, 0}
  };
  int opt;

#ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:i:t:s:l:b:F:c:a::g:rE:euwf:pvVhD:")) != EOF)
#else
  while ((opt = getopt_long(argc, argv, "d:i:t:s:l:b:F:c:a:g:rE:euwf:pvVh", &long_opts[0], NULL)) != EOF)
#endif

    switch (opt)
    {
      char *p;
      case 'd':
        vid = strtol(optarg, &p, 16);

        if (!p || (p == optarg))
        {
          printf("usb_sendhex : vendor ID is not hexadecimal number\n");
          exit(1);
        }

        if (*p == ':')
        {
          char *r = p + 1;
          pid = strtol(r, &p, 16);

          if (!p || (p == r))
          {
            printf("usb_sendhex : product ID is not hexadecimal number\n");
            exit(1);
          }
        }

        break;
      case 'i':
        pid = strtol(optarg, &p, 16);
        break;
      case 't':
        mem_type = strtol(optarg, NULL, 0);
        break;
      case 's':
        mem_start = strtoul(optarg, NULL, 0);
        break;
      case 'l':
        mem_length = strtoul(optarg, NULL, 0);
        mem_length_flg = 1;
        break;
      case 'b':
        max_block = strtoul(optarg, NULL, 0);
        break;
      case 'F':
        fill_flg=1;
        if (file_format != NULL)
          format_to_bytes_and_endian(file_format, &val_bytes, &val_endian);
        if(add_to_arr((void**)&fill_pat_val, &fill_pat_len, 0, optarg,
           val_bytes, val_endian)<0)
        {
          fprintf(stderr, "%s: incorrect patern data \"%s\"\n", argv[0], optarg);
          exit(2);
        }
        if(!fill_pat_len)
        {
          fprintf(stderr, "%s: incorrect patern data - empty value\n", argv[0]);
          exit(2);
        }
        break;
      case 'c':
        call = strtoul(optarg, NULL, 0);
        call_flg = 1;
        break;
      case 'a':
        arg = strtoul(optarg, NULL, 0);
        break;
      case 'g':
        go_addr = strtoul(optarg, NULL, 0);
        go_flg = 1;
        break;
      case 'r':
        reset_flg = 1;
        break;
      case 'E':
        masserase_mode = strtoul(optarg, NULL, 0);
        masserase_flg = 1;
        break;
      case 'e':
        regerase_flg = 1;
        break;
      case 'u':
        upload_flg = 1;
        break;
      case 'w':
        wait_for_device_flg = 1;
        break;
      case 'f':
        file_format = optarg;
        break;
      case 'p':
        prt_modules = 1;
        break;
      case 'D':
        debugk = strtol(optarg, NULL, 0);
        debugk_flg = 1;
        break;
      case 'v':
        verbose = 1;
        break;
      case 'V':
        fputs("USB sendhex v.1.1\n", stdout);
        exit(0);
      case 'h':
      default:
        usage();
        exit(opt == 'h' ? 0 : 1);
    }

  if ((optind >= argc) && !go_flg && !call_flg && !prt_modules && !debugk_flg
      && !masserase_flg && !regerase_flg && !reset_flg && !fill_flg)
  {
    usage();
    exit(1);
  }

  if (prt_modules)
    print_devices();

  if (wait_for_device_flg)
    if (wait_for_device(100) < 0)
      exit(2);

  if (reset_flg)
    send_cmd_reset();

  if (regerase_flg)
    send_cmd_regerase(mem_start, mem_length);

  if (masserase_flg)
    send_cmd_masserase(masserase_mode);

  if (fill_flg)
  {
    if (pattern_fill(mem_length_flg? mem_length: fill_pat_len,
                     fill_pat_val, fill_pat_len) < 0)
      exit(2);
  }

  if (!upload_flg)
  {
    while (optind < argc)
      if (download_file(argv[optind++], file_format) < 0)
        exit(2);
  }
  else
  {
    if (optind + 1 != argc)
    {
      printf("upload_file : needs exactly one filename\n");
      exit(1);
    }

    if (upload_file(argv[optind], file_format) < 0)
      exit(2);
  }

  if (call_flg)
  {
    if (send_cmd_call(call, arg) < 0)
      exit(2);
  }

  if (go_flg)
  {
    if (send_cmd_go(go_addr) < 0)
      exit(2);
  }

  return 0;
}
