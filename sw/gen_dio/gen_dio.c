/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  mo_diohw.c - programmable digital IO subsystem with support
               of position comparators and event triggers

  Copyright (C) 2001-2022 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2022 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#include <types.h>
#include <system_def.h>
#include <cpu_def.h>
#include <pxmc.h>
#include <string.h>
#include <malloc.h>
#include <gen_dio.h>
#include <ul_list.h>

volatile char dio_trig_active;
volatile char dio_trig_occured;

struct cmd_io *dio_last_used_cmd_io;

UL_LIST_CUST_DEC(dio_cmp_notify_list, dio_cmp_t, dio_cmp_notify_t,
		 notify_list, node)


UL_LIST_CUST_DEC(dio_trig_notify_list, dio_trig_t, dio_trig_notify_t,
		 notify_list, node)

unsigned dio_get16(struct dio_hw_des* des)
{
  return *(uint16_t*)des->rdaddr;
}

void dio_set16(struct dio_hw_des* des, unsigned val)
{
  *(uint16_t*)des->wraddr=val;
}

void dio_mod16(struct dio_hw_des* des, unsigned  mask, unsigned  xor)
{
  uint16_t *p=(uint16_t*)des->wraddr;
 #if 0
  uint16_t old=*p;
  uint16_t clear=(mask&~xor)|(~mask&xor&old);
  uint16_t set=(mask&xor)|(~mask&xor&~old);
  atomic_clear_mask_w(clear, p);
  atomic_set_mask_w(set, p);
 #else
  unsigned long flags=0;
  save_and_cli(flags);
  *p=(*p&~mask)^xor;
  restore_flags(flags);
 #endif
}

unsigned dio_backrd16(struct dio_hw_des* des)
{
  return *(uint16_t*)des->wraddr;
}

int dio_init16(struct dio_hw_des* des, uint16_t *rdaddr, uint16_t *wraddr)
{
  des->rdaddr=rdaddr;
  des->wraddr=wraddr;
  des->get=dio_get16;
  des->set=dio_set16;
  des->mod=dio_mod16;
  des->backrd=dio_backrd16;
  return 0;
}

#if defined(USD_IRC_MARK0) && defined(USD_IRC_MARK1)

unsigned dio_get_markrd(struct dio_hw_des* des)
{
  return (*USD_IRC_MARK0) | ((unsigned)(*USD_IRC_MARK1)<<8);
}

int dio_init_markrd(struct dio_hw_des* des)
{
  des->rdaddr=NULL;
  des->wraddr=NULL;
  des->get=dio_get_markrd;
  des->set=NULL;
  des->mod=NULL;
  return 0;
}

#endif

#ifdef WITH_TPU_TRIG

int tpu_dio_trig_init(int chan, int edges)
{

  int control=0;
  if(edges&DIO_TRIG_RISING)  control|=NITC_CHCTRL_RISING_EDGE;
  if(edges&DIO_TRIG_FALLING) control|=NITC_CHCTRL_FALLING_EDGE;

  return tpu_nitc_init(chan, 2, control, 1, 0, 1, 0);
}

#endif /* WITH_TPU_TRIG */

void dio_cmp_notify_insert(dio_cmp_t *cmp, dio_cmp_notify_t *notify)
{
  if(cmp->notify_list.next == NULL)
  {
    dio_cmp_notify_list_init_head(cmp);
  }
   dio_cmp_notify_list_insert(cmp, notify);
}

void dio_cmp_notify_del_item(dio_cmp_notify_t *notify)
{
  dio_cmp_notify_list_del_item(notify);
}

void dio_cmp_notify_init_detached(dio_cmp_notify_t *notify)
{
  dio_cmp_notify_list_init_detached(notify);
}

void dio_trig_notify_insert(dio_trig_t *trig, dio_trig_notify_t *notify)
{
  if(trig->notify_list.next == NULL)
  {
    dio_trig_notify_list_init_head(trig);
  }
  dio_trig_notify_list_insert(trig, notify);
}

void dio_trig_notify_del_item(dio_trig_notify_t *notify)
{
  dio_trig_notify_list_del_item(notify);
}

void dio_trig_notify_init_detached(dio_trig_notify_t *notify)
{
  dio_trig_notify_list_init_detached(notify);
}

static inline
void dio_trig_proc(dio_trig_t *trig)
{
  unsigned motmask;
  unsigned motnum;
  int i;

  if(trig->flags&DIO_TRIG_SNGL)
    dio_clear_flags(trig, DIO_TRIG_RUN);

  if(trig->flags&DIO_TRIG_SETDIO)
     dio_mod(&dio_hw_des[trig->dionum],trig->diomask,trig->dioxor);

  if(trig->flags&DIO_TRIG_OCC){
    dio_set_flags(trig, DIO_TRIG_OVERRUN);
    return;
  }

  if(trig->flags&DIO_TRIG_GETDIO)
     trig->dioin=dio_get(&dio_hw_des[trig->dionum]);

  motnum=DIO_TRIG_MOTNUM;
  if(motnum>pxmc_main_list.pxml_cnt)
    motnum=pxmc_main_list.pxml_cnt;
  motmask=(trig->motmask>>8)&((1<<DIO_TRIG_MOTNUM)-1);
  for(i=0;motmask&&(i<motnum);i++,motmask>>=1){
    pxmc_state_t *mcs;
    if(motmask&1){
      mcs=pxmc_main_list.pxml_arr[i];
      if(!mcs) continue;
      trig->motpos[i]=mcs->pxms_ap;
    }
  }
  motmask=trig->motmask&((1<<DIO_TRIG_MOTNUM)-1);
  for(i=0;motmask&&(i<motnum);i++,motmask>>=1){
    pxmc_state_t *mcs;
    if(motmask&1){
      mcs=pxmc_main_list.pxml_arr[i];
      if(!mcs) continue;
      pxmc_stop(mcs,0);
    }
  }

  dio_set_flags(trig, DIO_TRIG_OCC);
  dio_trig_occured=1;
}

static inline
void dio_cmp_proc(dio_cmp_t *cmp)
{
  if(!(cmp->flags&DIO_CMP_REPEAT)){
    dio_clear_flags(cmp, DIO_CMP_RUN);
  }else{
    dio_clear_flags(cmp, DIO_CMP_GTHIS|DIO_CMP_LTHIS);
    cmp->cmppos+=cmp->repoffs;
  }

  if(cmp->flags&DIO_CMP_SETDIO)
     dio_mod(&dio_hw_des[cmp->dionum],cmp->diomask,cmp->dioxor);

  if(cmp->flags&DIO_CMP_GETDIO)
     cmp->dioin=dio_get(&dio_hw_des[cmp->dionum]);

  dio_set_flags(cmp, DIO_CMP_OCC);
  dio_cmp_occured=1;
}

void do_dio_proc(void)
{
  int i;
  short s;
  char active;
  unsigned dio_val=0;
  int dio_chan=-1;

  /* Triggers processing */
  active=0;
  if(dio_trig_active)
  for(i=0;i<DIO_TRIG_COUNT;i++){
    dio_trig_t *trig=&dio_trig[i];
    if(trig->flags&DIO_TRIG_RUN){
      active=1;
      s=trig->src;
     #ifdef WITH_TPU_TRIG
      if(s<2){
        if(!tpu_test_cisr(s)) continue;
	tpu_clear_cisr(s);
      }else
     #endif /* WITH_TPU_TRIG */
      {
       #ifdef WITH_TRIG_DENOISE
	short dencnt=trig->dencnt;
	trig->dencnt=0;
       #endif /*WITH_TRIG_DENOISE*/
        if(dio_chan!=(s>>DIO_TRIG_SRC_GRP_SHIFT)){
	  dio_chan=(s>>DIO_TRIG_SRC_GRP_SHIFT);
	  dio_val=dio_get(&dio_hw_des[dio_chan]);
	}
        if(dio_val&(1<<(s&((1<<DIO_TRIG_SRC_GRP_SHIFT)-1)))){
	  if(trig->flags&DIO_TRIG_INLV) continue;
	 #ifdef WITH_TRIG_DENOISE
          if(trig->flags&DIO_TRIG_DENOISE){
	    if(dencnt<3){
	      trig->dencnt=dencnt+1;
	      continue;
	    }
	  }
	 #endif /*WITH_TRIG_DENOISE*/
          dio_set_flags(trig, DIO_TRIG_INLV);
	  if(!(trig->flags&DIO_TRIG_RISING)) continue;
	}else{
	  if(!(trig->flags&DIO_TRIG_INLV)) continue;
	 #ifdef WITH_TRIG_DENOISE
          if(trig->flags&DIO_TRIG_DENOISE){
	    if(dencnt<3){
	      trig->dencnt=dencnt+1;
	      continue;
	    }
	  }
	 #endif /*WITH_TRIG_DENOISE*/
          dio_clear_flags(trig, DIO_TRIG_INLV);
	  if(!(trig->flags&DIO_TRIG_FALLING)) continue;
	}
      }
      dio_trig_proc(trig);
    }
  }
  dio_trig_active=active;

  /* Comparator processing */
  active=0;
  if(dio_cmp_active)
  for(i=0;i<DIO_CMP_COUNT;i++){
    dio_cmp_t *cmp=&dio_cmp[i];
    short flags;
    if((flags=cmp->flags)&DIO_CMP_RUN){
      int n;
      pxmc_state_t *mcs;
      active=1;
      n=cmp->regnum;
      if(n>=pxmc_main_list.pxml_cnt) continue;
      mcs=pxmc_main_list.pxml_arr[n];
      if(!mcs) continue;
      if(mcs->pxms_cfg&PXMS_CFG_CYCL_m){
        /* Comparation for axis with cyclic flag set */
	if((mcs->pxms_ap-cmp->cmppos)>0){
          cmp->flags|=DIO_CMP_GTHIS;
          if(!(flags&DIO_CMP_LTHIS)||
	     !(flags&DIO_CMP_GT)){
	    if(flags&DIO_CMP_SEQ) break;
	    continue;
	  }
	}else if((mcs->pxms_ap-cmp->cmppos)<0){
          cmp->flags|=DIO_CMP_LTHIS;
          if(!(flags&DIO_CMP_GTHIS)||
	     !(flags&DIO_CMP_LT)){
	    if(flags&DIO_CMP_SEQ) break;
	    continue;
	  }
	}
      }else{
        /* Comparation for absolute axis in full long range */
	if(mcs->pxms_ap>cmp->cmppos){
          cmp->flags|=DIO_CMP_GTHIS;
          if(!(flags&DIO_CMP_LTHIS)||
	     !(flags&DIO_CMP_GT)){
	    if(flags&DIO_CMP_SEQ) break;
	    continue;
	  }
	}else if(mcs->pxms_ap<cmp->cmppos){
          cmp->flags|=DIO_CMP_LTHIS;
          if(!(flags&DIO_CMP_GTHIS)||
	     !(flags&DIO_CMP_LT)){
	    if(flags&DIO_CMP_SEQ) break;
	    continue;
	  }
	}
      }
      dio_cmp_proc(cmp);
    }
  }
  dio_cmp_active=active;
}

/********************************************************************/
/* high level and test routines */

#include <cmd_proc.h>
#include <stdio.h>
#include <utils.h>
#include <stdlib.h>


dio_hw_des_t *cmd_opchar_getdio(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned chan;
  dio_hw_des_t *dio_des;
  if(des->info[0]!=NULL) return (dio_hw_des_t *)des->info[0];
  chan=*param[1]-'0';
  if(chan>=DIO_COUNT) return NULL;
  dio_des=&dio_hw_des[chan];
  if(!dio_des) return NULL;
  return dio_des;
}

int cmd_do_digi(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int opchar;
  long val;
  dio_hw_des_t *dio_des;
  dio_last_used_cmd_io = cmd_io;
  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((dio_des=cmd_opchar_getdio(cmd_io,des,param))==NULL) return -CMDERR_BADDIO;
  if(!dio_des->get) return -CMDERR_BADDIO;
  val=dio_des->get(dio_des);
  return cmd_opchar_replong(cmd_io, param, val, 0, 0);
}

int cmd_do_digo(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int opchar;
  long val;
  dio_hw_des_t *dio_des;
  dio_last_used_cmd_io = cmd_io;
  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((dio_des=cmd_opchar_getdio(cmd_io,des,param))==NULL) return -CMDERR_BADDIO;
  switch(opchar){
    case '?':
      if(!dio_des->backrd) return -CMDERR_BADDIO;
      val=dio_des->backrd(dio_des);
      return cmd_opchar_replong(cmd_io, param, val, 0, 0);
    case ':':
      if(!dio_des->set) return -CMDERR_BADDIO;
      val=atoi(param[3]);
      dio_des->set(dio_des,(unsigned)val);
      return 0;
  }
  return -CMDERR_OPCHAR;
}

int cmd_do_digm(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *p;
  int opchar;
  long mask,xor;
  dio_hw_des_t *dio_des;
  dio_last_used_cmd_io = cmd_io;
  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if((dio_des=cmd_opchar_getdio(cmd_io,des,param))==NULL) return -CMDERR_BADDIO;
  switch(opchar){
    case ':':
      if(!dio_des->mod) return -CMDERR_BADDIO;
      p=param[3];
      si_skspace(&p);
      if(si_long(&p,&mask,0)<0) return -CMDERR_BADPAR;
      if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
      si_skspace(&p);
      if(si_long(&p,&xor,0)<0) return -CMDERR_BADPAR;
      dio_des->mod(dio_des,(unsigned)mask,(unsigned)xor);
      return 0;
  }
  return -CMDERR_OPCHAR;
}

int cmd_do_trig(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned trign;
  dio_trig_t *trig;
  char *p;
  int opchar;
  long src,motmask;
  long dionum,diomask,dioxor;
  unsigned int src_grp;

  dio_last_used_cmd_io = cmd_io;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;

  if(cmd_num_suffix(cmd_io, des, param, &trign)<0)
    return -CMDERR_BADSUF;
  if(trign>=DIO_TRIG_COUNT)
    return -CMDERR_BADSUF;
  trig=&dio_trig[trign];

  switch(opchar){
    case ':':
      p=param[3];
      if(si_long(&p,&src,0)<0) return -CMDERR_BADPAR;
      si_skspace(&p);
      if(!*p&&(src<=0)){
        trig->flags=0;
	return 0;
      }
      if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
      if(si_long(&p,&motmask,0)<0) return -CMDERR_BADPAR;
      if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
      if(si_long(&p,&dionum,0)<0) return -CMDERR_BADPAR;
      si_skspace(&p); if(!*p){
        dioxor=dionum;
	diomask=~0l;
        dionum=0;
      } else {
	if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
	if(si_long(&p,&diomask,0)<0) return -CMDERR_BADPAR;
	if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
	if(si_long(&p,&dioxor,0)<0) return -CMDERR_BADPAR;
	si_skspace(&p); if(*p) return -CMDERR_GARBAG;
      }

      src_grp=(src>>12)&0xf;

      if((dionum>=DIO_COUNT)||(src_grp>=DIO_COUNT))
        return -CMDERR_BADDIO;

      trig->flags=0;
      trig->src=src&0xf;
      trig->src|=src_grp<<DIO_TRIG_SRC_GRP_SHIFT;
      trig->motmask=motmask;
      trig->dionum=dionum;
      trig->diomask=diomask;
      trig->dioxor=dioxor;

      if(src>0){
        if(src&0x010) trig->flags|=DIO_TRIG_RISING;
	if(src&0x020) trig->flags|=DIO_TRIG_FALLING;
	if(src&0x040) trig->flags|=DIO_TRIG_GETDIO;
        if(src&0x080) trig->flags|=DIO_TRIG_SETDIO;
	if(src&0x100) trig->flags|=DIO_TRIG_DENOISE;
       #ifdef WITH_TPU_TRIG
	if((trig->src)<2)
          tpu_dio_trig_init(trig->src, trig->flags);
	else
       #endif /* WITH_TPU_TRIG */
        {
	  if(dio_get(&dio_hw_des[trig->src>>DIO_TRIG_SRC_GRP_SHIFT])&
		     (1<<(trig->src&0xf)))
            dio_set_flags(trig, DIO_TRIG_INLV);
	}
        dio_set_flags(trig, DIO_TRIG_RUN);
	__memory_barrier();
	dio_trig_active=1;
      }
      return 0;
  }
  return -CMDERR_OPCHAR;
}

void do_dio_trig_send(struct cmd_io *cmd_io)
{
  int i;
  unsigned motmask;
  char s[20];
  dio_trig_t *trig;

  dio_trig_occured=0;
  i=0;
  do{
    if(i>=DIO_TRIG_COUNT) return;
    trig=&dio_trig[i];
    if(trig->flags&DIO_TRIG_OCC) break;
    i++;
  }while(1);

  if(trig->notify_list.next != NULL)
  {
    ul_list_head_t saved_head;
    dio_trig_notify_t *notify;

    INIT_LIST_HEAD(&saved_head);
    list_splice_init(&trig->notify_list, &saved_head);

    while(!list_empty(&saved_head)){
      notify = UL_CONTAINEROF(saved_head.next, dio_trig_notify_t, node);
      list_move_tail(&notify->node, &trig->notify_list);
      if(notify->fnc)
        notify->fnc(notify, trig);
    }
  }

  cmd_io_putc(cmd_io,'T');
  cmd_io_putc(cmd_io,'G');
  if(i>=10)
    cmd_io_putc(cmd_io,'0'+i/10);
  cmd_io_putc(cmd_io,'0'+i%10);
  cmd_io_putc(cmd_io,'!');
  if(trig->flags&DIO_TRIG_GETDIO){
    i2str(s,trig->dioin,0,0);
    cmd_io_write(cmd_io,s,strlen(s));
  }else{
    cmd_io_putc(cmd_io,'N');
  }
  motmask=trig->motmask>>8;
  for(i=0;(i<DIO_TRIG_MOTNUM)&&motmask;i++,motmask>>=1){
    pxmc_state_t *mcs;
    if(!(motmask&1)) continue;
    cmd_io_putc(cmd_io,',');
    if(i>=pxmc_main_list.pxml_cnt) break;
    mcs=pxmc_main_list.pxml_arr[i];
    if(!mcs) continue;
    i2str(s,trig->motpos[i]>>mcs->pxms_subdiv,0,0);
    cmd_io_write(cmd_io,s,strlen(s));
  }
  dio_clear_flags(trig, DIO_TRIG_OCC);
  dio_trig_occured=1;
}

int cmd_do_cmp(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned cmpn;
  dio_cmp_t *cmp;
  char *p;
  int opchar;
  unsigned regnum;
  long flags,pos,dionum,diomask,dioxor;
  pxmc_state_t *mcs;

  dio_last_used_cmd_io = cmd_io;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(cmd_num_suffix(cmd_io, des, param, &cmpn)<0)
    return -CMDERR_BADSUF;
  if(cmpn>=DIO_CMP_COUNT)
    return -CMDERR_BADSUF;
  cmp=&dio_cmp[cmpn];

  switch(opchar){
    case ':':
      p=param[3];
      if(si_long(&p,&flags,0)<0) return -CMDERR_BADPAR;
      si_skspace(&p);
      if((!*p||(*p==','))&&(flags<=0)){
        cmp->flags=0;
	return 0;
      }
      if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
      si_skspace(&p);
      regnum=*(p++)-'A';
      if(regnum>=pxmc_main_list.pxml_cnt) return -CMDERR_BADPAR;
      if(!(mcs=pxmc_main_list.pxml_arr[regnum])) return -CMDERR_BADPAR;
      if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
      if(si_long(&p,&pos,0)<0) return -CMDERR_BADPAR;
      if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
      if(si_long(&p,&dionum,0)<0) return -CMDERR_BADPAR;
      si_skspace(&p); if(!*p){
        dioxor=dionum;
	diomask=~0l;
        dionum=0;
      } else {
	if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
	if(si_long(&p,&diomask,0)<0) return -CMDERR_BADPAR;
	if(si_fndsep(&p,",")<0) return -CMDERR_BADSEP;
	if(si_long(&p,&dioxor,0)<0) return -CMDERR_BADPAR;
	si_skspace(&p); if(*p) return -CMDERR_GARBAG;
      }

      if(dionum>=DIO_COUNT)
        return -CMDERR_BADDIO;

      cmp->flags=flags|DIO_CMP_GETDIO;
      cmp->regnum=regnum;
      cmp->cmppos=pos<<mcs->pxms_subdiv;
      cmp->dionum=dionum;
      cmp->diomask=diomask;
      cmp->dioxor=dioxor;

      dio_set_flags(cmp, DIO_CMP_RUN);
      __memory_barrier();
      dio_cmp_active=1;
      return 0;
  }
  return -CMDERR_OPCHAR;
}

int cmd_do_cmprepo(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned cmpn;
  dio_cmp_t *cmp;
  char *p;
  int opchar;
  long val;
  pxmc_state_t *mcs;
  unsigned regnum;

  dio_last_used_cmd_io = cmd_io;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;

  if(cmd_num_suffix(cmd_io, des, param, &cmpn)<0)
    return -CMDERR_BADSUF;
  if(cmpn>=DIO_CMP_COUNT)
    return -CMDERR_BADSUF;
  cmp=&dio_cmp[cmpn];

  switch(opchar){
    case ':':
      p=param[3];
      if(si_long(&p,&val,0)<0) return -CMDERR_BADPAR;
      si_skspace(&p); if(*p) return -CMDERR_GARBAG;
      regnum=cmp->regnum;
      if(regnum>=pxmc_main_list.pxml_cnt) return -CMDERR_BADPAR;
      if(!(mcs=pxmc_main_list.pxml_arr[regnum])) return -CMDERR_BADPAR;
      cmp->repoffs=val<<mcs->pxms_subdiv;
      return 0;
  }
  return -CMDERR_OPCHAR;
}

void do_dio_cmp_send(struct cmd_io *cmd_io)
{
  int i;
  char s[20];
  dio_cmp_t *cmp;

  dio_cmp_occured=0;
  __memory_barrier();

  i=0;
  do{
    if(i>=DIO_CMP_COUNT) return;
    cmp=&dio_cmp[i];
    if(cmp->flags&DIO_CMP_OCC) break;
    i++;
  }while(1);

  if(cmp->notify_list.next != NULL)
  {
    ul_list_head_t saved_head;
    dio_cmp_notify_t *notify;

    list_splice_init(&cmp->notify_list, &saved_head);

    while(!list_empty(&saved_head)){
      notify = UL_CONTAINEROF(saved_head.next, dio_cmp_notify_t, node);
      list_move_tail(&notify->node, &cmp->notify_list);
      if(notify->fnc)
        notify->fnc(notify, cmp);
    }
  }

  if(!(cmp->flags&DIO_CMP_SILENT)) {
    cmd_io_write(cmd_io,"CMP",3);
    if(i>=10)
      cmd_io_putc(cmd_io,'0'+i/10);
    cmd_io_putc(cmd_io,'0'+i%10);
    cmd_io_putc(cmd_io,'!');
    if(cmp->flags&DIO_CMP_GETDIO){
      i2str(s,cmp->dioin,0,0);
      cmd_io_write(cmd_io,s,strlen(s));
    }else{
      cmd_io_putc(cmd_io,'N');
    }
  }
  dio_clear_flags(cmp, DIO_CMP_OCC);
  dio_cmp_occured=1;
}

cmd_des_t const cmd_des_digi={0, CDESM_OPCHR|CDESM_RD,
			"DIGI","digital input",cmd_do_digi,
			{(char*)&dio_hw_des[0],
			 0}};
cmd_des_t const cmd_des_digo={0, CDESM_OPCHR|CDESM_RD|CDESM_WR,
			"DIGO","digital output",cmd_do_digo,
			{(char*)&dio_hw_des[0],
			 0}};
cmd_des_t const cmd_des_digm={0, CDESM_OPCHR|CDESM_WR,
			"DIGM","digital output modify",cmd_do_digm,
			{(char*)&dio_hw_des[0],
			 0}};
cmd_des_t const cmd_des_digix={0, CDESM_OPCHR|CDESM_RD,
			"DIGI#","digital input x",cmd_do_digi,
			{NULL,
			 0}};
cmd_des_t const cmd_des_digox={0, CDESM_OPCHR|CDESM_RD|CDESM_WR,
			"DIGO#","digital output x",cmd_do_digo,
			{NULL,
			 0}};
cmd_des_t const cmd_des_digmx={0, CDESM_OPCHR|CDESM_WR,
			"DIGM#","digital output x modify",cmd_do_digm,
			{NULL,
			 0}};
cmd_des_t const cmd_des_trig={0, CDESM_OPCHR|CDESM_WR,
			"TRIG#*","trigger s,m,{do|n,mask,xor}",cmd_do_trig,
			{NULL,
			 0}};
cmd_des_t const cmd_des_cmp={0, CDESM_OPCHR|CDESM_WR,
			"CMP#*","comparator f,m,p,{do|n,mask,xor}",cmd_do_cmp,
			{NULL,
			 0}};
cmd_des_t const cmd_des_cmprepo={0, CDESM_OPCHR|CDESM_WR,
			"CMPREPO#*","comparator position repeat offset",cmd_do_cmprepo,
			{NULL,
			 0}};

cmd_des_t const *cmd_dio_tab[]={
  &cmd_des_digix,
  &cmd_des_digox,
  &cmd_des_digmx,
  &cmd_des_digi,
  &cmd_des_digo,
  &cmd_des_digm,
  &cmd_des_trig,
  &cmd_des_cmp,
  &cmd_des_cmprepo,
  NULL
};


/*
Examples

ECHO:1

TRIG0:208,1792,0,3,1
TRIG1:225,1799,0,3,2
TRIG2:216,1792,21845
TRIG3:121,0,0,0,21845

CMP0:17,C,1000,0,12,8
CMP1:18,C,-1000,0,12,4
CMP2:27,C,-1500,0,12,0
CMP3:27,C,-500,0,12,12

 */
