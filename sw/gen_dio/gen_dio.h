/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  mo_dio.c - programmable digital IO subsystem with support
             of position comparators and event triggers

  Copyright (C) 2001-2022 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2022 by PiKRON Ltd. - originator
	                  http://www.pikron.com

  This code can be used only with written permission of PiKRON Ltd.
  Only permitted exception is use of the code for education purposes
  unrelated to commercial activities.
  The PiKRON Ltd. may considers to release code under GNU license
  for open source projects in future.

 *******************************************************************/

#ifndef _GEN_DIO_H_
#define _GEN_DIO_H_

#include <ul_list.h>

#ifndef DIO_COUNT
  #define DIO_COUNT 4
#endif

#ifndef DIO_TRIG_COUNT
  #define DIO_TRIG_COUNT 4
#endif

#ifndef DIO_CMP_COUNT
  #define DIO_CMP_COUNT 4
#endif

typedef struct dio_hw_des {
    long private;
    void *rdaddr;
    void *wraddr;
    unsigned *shadaddr;
    unsigned(*get)(struct dio_hw_des* des);
    void(*set)(struct dio_hw_des* des, unsigned  val);
    void(*mod)(struct dio_hw_des* des, unsigned  mask, unsigned  xor);
    unsigned(*backrd)(struct dio_hw_des* des);
  } dio_hw_des_t;

dio_hw_des_t dio_hw_des[DIO_COUNT];

#define DIO_TRIG_RISING  0x001
#define DIO_TRIG_FALLING 0x002
#define DIO_TRIG_SETDIO  0x004
#define DIO_TRIG_GETDIO  0x008
#define DIO_TRIG_RUN     0x010
#define DIO_TRIG_OCC     0x020
#define DIO_TRIG_SNGL    0x040
#define DIO_TRIG_INLV    0x080
#define DIO_TRIG_OVERRUN 0x100
#define DIO_TRIG_DENOISE 0x200

#define DIO_TRIG_MOTNUM  8

#define DIO_TRIG_SRC_GRP_SHIFT 5

#if defined (__m68k__) || defined(__mc68332__) || defined(__mc68000__) || defined (__H8300H__)
  #undef DIO_WITH_FLAGS_LONG_TYPE
#else
  #define DIO_WITH_FLAGS_LONG_TYPE 1
#endif

#ifdef DIO_WITH_FLAGS_LONG_TYPE

#define dio_clear_flags(dio_ent,mask) \
  atomic_clear_mask(mask,&((dio_ent)->flags))

#define dio_set_flags(dio_ent,mask) \
  atomic_set_mask(mask,&((dio_ent)->flags))

#else /*DIO_WITH_FLAGS_LONG_TYPE*/

#define dio_clear_flags(dio_ent,mask) \
  atomic_clear_mask_w(mask,&((dio_ent)->flags))

#define dio_set_flags(dio_ent,mask) \
  atomic_set_mask_w(mask,&((dio_ent)->flags))

#endif /*DIO_WITH_FLAGS_LONG_TYPE*/

struct dio_trig;

typedef struct dio_trig_notify {
    void (*fnc)(struct dio_trig_notify *notify, struct dio_trig *trig);
    ul_list_node_t node;
} dio_trig_notify_t;

typedef struct dio_trig {
  #ifdef DIO_WITH_FLAGS_LONG_TYPE
    unsigned long flags;
  #else
    short flags;
  #endif
    short src;
  #ifdef WITH_TRIG_DENOISE
    short dencnt;
  #endif /*WITH_TRIG_DENOISE*/
    long private;
    long motmask;
    long dionum;
    long diomask;
    long dioxor;
    long dioin;
    long motpos[DIO_TRIG_MOTNUM];
    ul_list_head_t notify_list;
  } dio_trig_t;

dio_trig_t dio_trig[DIO_TRIG_COUNT];
volatile char dio_trig_active;
volatile char dio_trig_occured;

#define DIO_CMP_GT      0x001
#define DIO_CMP_LT      0x002
#define DIO_CMP_SEQ     0x008
#define DIO_CMP_SETDIO  0x010
#define DIO_CMP_REPEAT  0x020
#define DIO_CMP_SILENT  0x040
#define DIO_CMP_GETDIO  0x080
#define DIO_CMP_OCC     0x100
#define DIO_CMP_RUN     0x200
#define DIO_CMP_GTHIS   0x400
#define DIO_CMP_LTHIS   0x800
#define DIO_CMP_REMOVE_NEAR 0x1000
#define DIO_CMP_INDIV_FLAGS 0x2000
#define DIO_CMP_CHKSEQ_OUT  0x4000
#define DIO_CMP_SKIP_NOISE  0x8000

struct dio_cmp;

typedef struct dio_cmp_notify {
    void (*fnc)(struct dio_cmp_notify *notify, struct dio_cmp *cmp);
    ul_list_node_t node;
} dio_cmp_notify_t;

typedef struct dio_cmp {
  #ifdef DIO_WITH_FLAGS_LONG_TYPE
    unsigned long flags;
  #else
    short flags;
  #endif
    short regnum;
    long cmppos;
    long repoffs;
    long private;
    long dionum;
    long diomask;
    long dioxor;
    long dioin;
    ul_list_head_t notify_list;
  } dio_cmp_t;

dio_cmp_t dio_cmp[DIO_CMP_COUNT];
volatile char dio_cmp_active;
volatile char dio_cmp_occured;

static inline unsigned dio_get(struct dio_hw_des* des)
{
  if(!des->get) return 0;
  return des->get(des);
}

static inline void dio_set(struct dio_hw_des* des, unsigned  val)
{
  if(!des->set) return;
  des->set(des,val);
}

static inline void dio_mod(struct dio_hw_des* des, unsigned  mask, unsigned  xor)
{
  if(!des->mod) return;
  des->mod(des,mask,xor);
}

static inline unsigned dio_backrd(struct dio_hw_des* des)
{
  if(!des->backrd) return 0;
  return des->backrd(des);
}

int dio_init16(struct dio_hw_des* des, uint16_t *rdaddr, uint16_t *wraddr);
int dio_init_markrd(struct dio_hw_des* des);

struct cmd_io;

extern struct cmd_io *dio_last_used_cmd_io;

void do_dio_proc(void);
void do_dio_trig_send(struct cmd_io *cmd_io);
void do_dio_cmp_send(struct cmd_io *cmd_io);

void dio_cmp_notify_insert(dio_cmp_t *cmp, dio_cmp_notify_t *notify);
void dio_cmp_notify_del_item(dio_cmp_notify_t *notify);
void dio_cmp_notify_init_detached(dio_cmp_notify_t *notify);

void dio_trig_notify_insert(dio_trig_t *trig, dio_trig_notify_t *notify);
void dio_trig_notify_del_item(dio_trig_notify_t *notify);
void dio_trig_notify_init_detached(dio_trig_notify_t *notify);

#endif /* _GEN_DIO_H_ */
