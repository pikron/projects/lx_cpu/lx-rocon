/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  appl_pxmc.c - position controller RoCoN hardware support

  Copyright (C) 2001-2013 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2013 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_lpc_qei.h>
#include <pxmc_internal.h>
#include <pxmc_inp_common.h>
#include <pxmc_gen_info.h>
#include <hal_gpio.h>
#include <hal_machperiph.h>
#include <stdlib.h>
#include <string.h>
#include <LPC17xx.h>
#include <lpcTIM.h>

#include "appl_defs.h"
#include "appl_fpga.h"
#include "appl_pxmc.h"
#include "pxmcc_types.h"
#include "pxmcc_interface.h"
#include "as5_spi.h"

#ifdef APPL_WITH_PSINDCTL
#ifdef APPL_WITH_PSINDCTL_PAST
#include "psindctl_spi.h"
#include "psindctl_past.h"
#endif /*APPL_WITH_PSINDCTL_PAST*/
#endif /*APPL_WITH_PSINDCTL*/

#ifdef APPL_WITH_DIO
#include <gen_dio.h>
#endif /*APPL_WITH_DIO*/

#ifdef APPL_WITH_CMPQUE
#include <pxmc_cmpque.h>
#endif /*APPL_WITH_CMPQUE*/

#define PXMC_AXIS_MODE_BLDC_PXMCC             (PXMC_AXIS_MODE_BLDC + 1) /*6*/
#define PXMC_AXIS_MODE_STEPPER_WITH_IRC_PXMCC (PXMC_AXIS_MODE_BLDC + 2) /*7*/
#define PXMC_AXIS_MODE_STEPPER_PXMCC          (PXMC_AXIS_MODE_BLDC + 3) /*8*/
#define PXMC_AXIS_MODE_BLDC_DQ                (PXMC_AXIS_MODE_BLDC + 4) /*9*/
#define PXMC_AXIS_MODE_DC_CURCTRL             (PXMC_AXIS_MODE_BLDC + 5) /*10*/

int pxmc_ptofs_from_index(pxmc_state_t *mcs, unsigned long irc,
                           unsigned long index_irc, int diff2err);

#ifndef pxmc_fast_tick_time
#define pxmc_fast_tick_time() (LPC_TIM0->TC)
#endif

#define PXML_MAIN_CNT 8

#define PXMC_WITH_PT_ZIC 1
#define PXMC_PT_ZIC_MASK 0x8000

#define LPCPWM1_FREQ 20000

#define HAL_ERR_SENSITIVITY 20
#define HAL_ERR_MAX_COUNT    5

#define LXPWR_WITH_SIROLADC  1

#define LX_MASTER_DATA_OFFS  8

#define PXMC_LXPWR_PWM_CYCLE 2500

unsigned pxmc_rocon_pwm_magnitude = PXMC_LXPWR_PWM_CYCLE;

long pxmc_rocon_irc_offset[PXML_MAIN_CNT];
unsigned pxmc_rocon_mark_filt[PXML_MAIN_CNT];
unsigned pxmc_rocon_irc_index_count[PXML_MAIN_CNT];
unsigned pxmc_rocon_lxpwr_chips = 0;

static inline
pxmc_rocon_state_t *pxmc_state2rocon_state(pxmc_state_t *mcs)
{
  pxmc_rocon_state_t *mcsrc;
 #ifdef UL_CONTAINEROF
  mcsrc = UL_CONTAINEROF(mcs, pxmc_rocon_state_t, base);
 #else /*UL_CONTAINEROF*/
  mcsrc = (pxmc_rocon_state_t*)((char*)mcs - __builtin_offsetof(pxmc_rocon_state_t, base));
 #endif /*UL_CONTAINEROF*/
  return mcsrc;
}

uint32_t pxmc_rocon_vin_adc_last;
int pxmc_rocon_vin_act;
int pxmc_rocon_vin_ofs = 20978;
int pxmc_rocon_vin_mul = 32905;
int pxmc_rocon_vin_shr = 14;
int pxmc_rocon_vin_uv_prot = 8000;
int pxmc_rocon_vin_uv_prot_cnt = 0;
int pxmc_rocon_regpwron_state = 1;

static inline
void pxmc_rocon_vin_compute(void)
{
  volatile uint32_t *vin_adc_reg;
  uint32_t vin_adc;
  int      vin_act;

  vin_adc_reg = fpga_lx_master_receiver_base;
  vin_adc_reg += LX_MASTER_DATA_OFFS + 1 + 8 * 2;

  vin_adc = *vin_adc_reg;

  vin_act = (int16_t)(vin_adc - pxmc_rocon_vin_adc_last);
  pxmc_rocon_vin_adc_last = vin_adc;

  vin_act = (pxmc_rocon_vin_ofs - vin_act) * pxmc_rocon_vin_mul;
  vin_act >>= pxmc_rocon_vin_shr;

  if (vin_act < pxmc_rocon_vin_uv_prot) {
    if (pxmc_rocon_vin_uv_prot_cnt < 1000)
      pxmc_rocon_vin_uv_prot_cnt += 1;
  } else {
    pxmc_rocon_vin_uv_prot_cnt = 0;
  }

  pxmc_rocon_vin_act = vin_act;
}

unsigned int pxmc_rocon_rx_done_sqn;
unsigned int pxmc_rocon_rx_done_sqn_inc;
unsigned int pxmc_rocon_rx_done_sqn_misscnt;
unsigned int pxmc_rocon_rx_done_sqn_missoffs;

static inline
void pxmc_rocon_rx_done_sqn_compute(void)
{
  uint32_t sqn_act;
  uint32_t sqn_offs;
  unsigned int sqn_expect = pxmc_rocon_rx_done_sqn + pxmc_rocon_rx_done_sqn_inc;

  sqn_act = *fpga_lx_master_receiver_done_div;
  sqn_offs = (sqn_act - sqn_expect) & 0x1f;
  if (sqn_offs) {
    if (pxmc_rocon_rx_done_sqn_missoffs != sqn_offs) {
      pxmc_rocon_rx_done_sqn_misscnt = 1;
    } else {
      pxmc_rocon_rx_done_sqn_misscnt++;
      if (pxmc_rocon_rx_done_sqn_misscnt >= 10)
        sqn_expect += 1 - ((sqn_offs >> 3) & 2);
    }
  } else {
    pxmc_rocon_rx_done_sqn_misscnt = 0;
  }
  pxmc_rocon_rx_done_sqn = sqn_expect;
  pxmc_rocon_rx_done_sqn_missoffs = sqn_offs;
}

uint32_t pxmc_rocon_rx_err_cnt_last;
uint32_t pxmc_rocon_rx_err_level;
uint32_t pxmc_rocon_mcc_rx_done_sqn_last;
uint32_t pxmc_rocon_mcc_stuck;

static inline
void pxmc_rocon_rx_error_check(void)
{
  uint32_t cnt;
  uint32_t mcc_sqn;
  pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();

  cnt = mcc_data->common.rx_err_cnt;
  pxmc_rocon_rx_err_level = cnt - pxmc_rocon_rx_err_cnt_last;
  pxmc_rocon_rx_err_cnt_last = cnt;

  mcc_sqn = mcc_data->common.rx_done_sqn;
  pxmc_rocon_mcc_stuck = mcc_sqn == pxmc_rocon_mcc_rx_done_sqn_last? 1: 0;
  pxmc_rocon_mcc_rx_done_sqn_last = mcc_sqn;
}

const uint8_t onesin10bits[1024]={
  0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4,1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  1,2,2,3,2,3,3,4,2,3,3,4,3,4,4,5,2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  2,3,3,4,3,4,4,5,3,4,4,5,4,5,5,6,3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  3,4,4,5,4,5,5,6,4,5,5,6,5,6,6,7,4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  4,5,5,6,5,6,6,7,5,6,6,7,6,7,7,8,5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,
  5,6,6,7,6,7,7,8,6,7,7,8,7,8,8,9,6,7,7,8,7,8,8,9,7,8,8,9,8,9,9,10
};

long
pxmc_rocon_read_irc(int chan)
{
  long irc;
  irc = fpga_irc[chan]->count;
  irc += pxmc_rocon_irc_offset[chan];
  return irc;
}

int
pxmc_rocon_irc_is_index_edge(int chan, int clear_event)
{
  uint32_t irc_state;
  int index;
  int ret;

  irc_state = *fpga_irc_state[chan];
  index = FPGA_IRC_STATE_INDEX_EVENT_MASK & irc_state;
  ret = index >> (ffs(FPGA_IRC_STATE_INDEX_EVENT_MASK) - 1);

  if (clear_event) {
    *fpga_irc_state[chan] = index;
    pxmc_rocon_irc_index_count[chan] += ret;;
  }

  return ret;
}

long
pxmc_rocon_read_irc_index(int chan, unsigned int *idx_couter_p, int clear_event)
{
  long irc_index;
  int index_event_fl;

  irc_index = fpga_irc[chan]->count_index;
  irc_index += pxmc_rocon_irc_offset[chan];

  if (idx_couter_p != 0) {
    unsigned idx_couter;

    index_event_fl = pxmc_rocon_irc_is_index_edge(chan, clear_event);
    idx_couter = pxmc_rocon_irc_index_count[chan];
    if (!clear_event && index_event_fl)
      idx_couter++;
    *idx_couter_p = idx_couter;
  }

  return irc_index;
}

int
pxmc_inp_rocon_inp(struct pxmc_state *mcs)
{
  int chan=mcs->pxms_inp_info;
  long irc;
  long pos;

  /* read position from hardware */
  irc = fpga_irc[chan]->count;
  irc += pxmc_rocon_irc_offset[chan];
  pos = irc << PXMC_SUBDIV(mcs);
  mcs->pxms_as = pos - mcs->pxms_ap;
  mcs->pxms_ap = pos;

  /* Running of the motor commutator */
  if (mcs->pxms_flg & PXMS_PTI_m)
    pxmc_irc_16bit_commindx(mcs, irc);

  return 0;
}

int
pxmc_inp_rocon_is_mark_direct(int chan)
{
  uint32_t irc_state;
  int mark;

  if (*fpga_irc_state[chan] == 0)
    return 0;

  irc_state = *fpga_irc_state[chan];

  mark = (irc_state >> (ffs(FPGA_IRC_STATE_MARK_MASK) - 1)) & 1;

  return mark;
}

int
pxmc_inp_rocon_is_mark(pxmc_state_t *mcs)
{
  uint32_t irc_state;
  int mark;
  unsigned filt;
  int chan=mcs->pxms_inp_info;

  irc_state = *fpga_irc_state[chan];

  mark = ((irc_state >> (ffs(FPGA_IRC_STATE_MARK_MASK) - 1)) ^
         (mcs->pxms_cfg >> PXMS_CFG_HPS_b)) & 1;

  filt = pxmc_rocon_mark_filt[chan];
  filt = (filt << 1) | mark;
  pxmc_rocon_mark_filt[chan] = filt;

  return onesin10bits[filt & 0x03ff];
}

static inline int
pxmc_inp_rocon_is_index_edge(pxmc_state_t *mcs, int clear_event)
{
  int chan=mcs->pxms_inp_info;
  return pxmc_rocon_irc_is_index_edge(chan, clear_event);
}

int
pxmc_inp_rocon_adjust_to_irc_change(struct pxmc_state *mcs, long irc_diff)
{
  long pos_diff = irc_diff << PXMC_SUBDIV(mcs);

  mcs->pxms_ptofs += irc_diff;

  mcs->pxms_ap += pos_diff;
  mcs->pxms_rp += pos_diff;
  return 0;
}

int
pxmc_inp_rocon_irc_offset_from_index(struct pxmc_state *mcs)
{
  int chan=mcs->pxms_inp_info;
  long irc_offset;
  long irc_diff;

  irc_offset = -fpga_irc[chan]->count_index;
  irc_diff = irc_offset - pxmc_rocon_irc_offset[chan];
  pxmc_rocon_irc_offset[chan] = irc_offset;
  return pxmc_inp_rocon_adjust_to_irc_change(mcs, irc_diff);
}

int
pxmc_inp_rocon_ap_zero(struct pxmc_state *mcs)
{
  int chan=mcs->pxms_inp_info;
  long irc_offset;
  long irc_diff;

  irc_offset = -fpga_irc[chan]->count;
  irc_diff = irc_offset - pxmc_rocon_irc_offset[chan];
  pxmc_rocon_irc_offset[chan] = irc_offset;
  return pxmc_inp_rocon_adjust_to_irc_change(mcs, irc_diff);
}

int
pxmc_inp_rocon_ap2hw(struct pxmc_state *mcs)
{
  int chan=mcs->pxms_inp_info;
  long irc;
  long pos_diff;

  irc = fpga_irc[chan]->count;
  pos_diff = mcs->pxms_ap - (irc << PXMC_SUBDIV(mcs));

  irc = pos_diff >> PXMC_SUBDIV(mcs);

  /* Adjust phase table alignemt to modified IRC readout  */
  mcs->pxms_ptofs += irc - pxmc_rocon_irc_offset[chan];

  pxmc_rocon_irc_offset[chan] = irc;
  return 0;
}

int
pxmc_inp_rocon_ptofs_from_index_poll(struct pxmc_state *mcs, int diff2err)
{
  int chan=mcs->pxms_inp_info;
  long irc;
  long index_irc;

  if (!(*fpga_irc_state[chan] & FPGA_IRC_STATE_INDEX_EVENT_MASK))
    return 0;

  irc = fpga_irc[chan]->count + pxmc_rocon_irc_offset[chan];
  index_irc = fpga_irc[chan]->count_index + pxmc_rocon_irc_offset[chan];

  return pxmc_ptofs_from_index(mcs, irc, index_irc, diff2err);
}

uint32_t pxmc_rocon_receiver_dummy_reg;

static inline volatile uint32_t *
pxmc_rocon_receiver_chan2reg(unsigned chan)
{
  volatile uint32_t *rec_reg;

  if (chan >= 16)
    return &pxmc_rocon_receiver_dummy_reg;

  rec_reg = fpga_lx_master_receiver_base;

 #ifdef LXPWR_WITH_SIROLADC
  rec_reg += LX_MASTER_DATA_OFFS + 1 + (chan >> 3) * 3 + chan * 2;
 #else /*LXPWR_WITH_SIROLADC*/
  rec_reg += LX_MASTER_DATA_OFFS + chan;
 #endif /*LXPWR_WITH_SIROLADC*/

  return rec_reg;
}

static inline unsigned
pxmc_rocon_bldc_hal_rd(pxmc_state_t *mcs)
{
  unsigned h = 0;
  volatile uint32_t *rec_reg_a, *rec_reg_b, *rec_reg_c;
  int chan = mcs->pxms_out_info;
  int hal_offs;

 #ifdef LXPWR_WITH_SIROLADC
  hal_offs = 1;
 #else /*LXPWR_WITH_SIROLADC*/
  hal_offs = 0;
 #endif /*LXPWR_WITH_SIROLADC*/

  rec_reg_a = pxmc_rocon_receiver_chan2reg(chan + 0);
  rec_reg_b = pxmc_rocon_receiver_chan2reg(chan + 1);
  rec_reg_c = pxmc_rocon_receiver_chan2reg(chan + 2);

  h  = (rec_reg_a[hal_offs] >> 14) & 1;
  h |= (rec_reg_b[hal_offs] >> 13) & 2;
  h |= (rec_reg_c[hal_offs] >> 12) & 4;

  /* return 3 bits corresponding to the HAL senzor input */
  return h;
}

#if 1
const unsigned char pxmc_lpc_bdc_hal_pos_table[8] =
{
  [0] = 0xff,
  [7] = 0xff,
  [1] = 0, /*0*/
  [5] = 1, /*1*/
  [4] = 2, /*2*/
  [6] = 3, /*3*/
  [2] = 4, /*4*/
  [3] = 5, /*5*/
};
#else
const unsigned char pxmc_lpc_bdc_hal_pos_table[8] =
{
  [0] = 0xff,
  [7] = 0xff,
  [1] = 0, /*0*/
  [5] = 5, /*1*/
  [4] = 4, /*2*/
  [6] = 3, /*3*/
  [2] = 2, /*4*/
  [3] = 1, /*5*/
};
#endif

uint32_t pxmc_rocon_pwm_dummy_reg;

static inline volatile uint32_t *
pxmc_rocon_pwm_chan2reg(unsigned chan)
{
  volatile uint32_t *pwm_reg;

  if (chan >= 16)
    return &pxmc_rocon_pwm_dummy_reg;

  pwm_reg = fpga_lx_master_transmitter_base;

 #ifdef LXPWR_WITH_SIROLADC
  pwm_reg += LX_MASTER_DATA_OFFS + 1 + (chan >> 3) + chan;
 #else /*LXPWR_WITH_SIROLADC*/
  pwm_reg += LX_MASTER_DATA_OFFS + chan;
 #endif /*LXPWR_WITH_SIROLADC*/

  return pwm_reg;
}

int pxmc_rocon_pwm_direct_wr(unsigned chan, unsigned pwm, int en)
{
  volatile uint32_t *pwm_reg;
  pwm_reg = pxmc_rocon_pwm_chan2reg(chan);

  if (pwm_reg == &pxmc_rocon_pwm_dummy_reg)
    return -1;

  *pwm_reg = pwm | (en? 0x4000: 0x8000);

  return 0;
}

/**
 * pxmc_rocon_pwm3ph_wr - Output of the 3-phase PWM to the hardware
 * @mcs:  Motion controller state information
 */
static /*inline*/ void
pxmc_rocon_pwm3ph_wr(pxmc_state_t *mcs, short pwm1, short pwm2, short pwm3)
{
  volatile uint32_t *pwm_reg_a, *pwm_reg_b, *pwm_reg_c;
  int chan = mcs->pxms_out_info;

  pwm_reg_c = pxmc_rocon_pwm_chan2reg(chan + 0);
  pwm_reg_b = pxmc_rocon_pwm_chan2reg(chan + 1);
  pwm_reg_a = pxmc_rocon_pwm_chan2reg(chan + 2);

  *pwm_reg_a = pwm1;
  *pwm_reg_b = pwm2;
  *pwm_reg_c = pwm3;
}

static inline void
pxmc_rocon_process_hal_error(struct pxmc_state *mcs)
{
  if (mcs->pxms_halerc >= HAL_ERR_SENSITIVITY * HAL_ERR_MAX_COUNT)
  {
    pxmc_set_errno(mcs, PXMS_E_HAL);
    mcs->pxms_ene = 0;
    mcs->pxms_halerc--;
  }
  else
    mcs->pxms_halerc += HAL_ERR_SENSITIVITY;
}

/**
 * pxmc_rocon_pwm3ph_out - Phase output for brush-less 3-phase motor
 * @mcs:  Motion controller state information
 */
int
pxmc_rocon_pwm3ph_out(pxmc_state_t *mcs)
{
  typeof(mcs->pxms_ptvang) ptvang;
  int sync_mode = 0;
  unsigned char hal_pos;
  short pwm1;
  short pwm2;
  short pwm3;
  int indx = 0;
  short ene;
  int wind_current[4];

  if (!(mcs->pxms_flg & PXMS_PTI_m) || !(mcs->pxms_flg & PXMS_PHA_m) ||
      (mcs->pxms_flg & PXMS_PRA_m))
  {
    short ptindx;
    short ptirc = mcs->pxms_ptirc;
    short divisor = mcs->pxms_ptper * 6;

   #if 0
    pxmc_irc_16bit_commindx(mcs, mcs->pxms_rp >> PXMC_SUBDIV(mcs));
    sync_mode = 1;
   #elif 0
    {
      int res;
      res = pxmc_inp_rocon_ptofs_from_index_poll(mcs, 0);
      if (res < 0) {
        pxmc_set_errno(mcs, PXMS_E_I2PT_TOOBIG);
      } else if (res) {
        pxmc_set_flag(mcs, PXMS_PTI_b);
        pxmc_set_flag(mcs, PXMS_PHA_b);
      }
    }
   #else

    hal_pos = pxmc_lpc_bdc_hal_pos_table[pxmc_rocon_bldc_hal_rd(mcs)];

    if (hal_pos == 0xff)
    {
      if (mcs->pxms_ene)
        pxmc_rocon_process_hal_error(mcs);
    }
    else
    {
      if (mcs->pxms_halerc)
        mcs->pxms_halerc--;

      ptindx = (hal_pos * ptirc + divisor / 2) / divisor;

      if (!(mcs->pxms_flg & PXMS_PTI_m) || (mcs->pxms_flg & PXMS_PRA_m))
      {
        if (((hal_pos != mcs->pxms_hal) && (mcs->pxms_hal != 0x40)) && 1)
        {
          short ptindx_prev = (mcs->pxms_hal * ptirc + divisor / 2) / divisor;;

          if ((ptindx > ptindx_prev + ptirc / 2) ||
              (ptindx_prev > ptindx + ptirc / 2))
          {
            ptindx = (ptindx_prev + ptindx - ptirc) / 2;

            if (ptindx < 0)
              ptindx += ptirc;
          }
          else
          {
            ptindx = (ptindx_prev + ptindx) / 2;
          }

          mcs->pxms_ptindx = ptindx;

          mcs->pxms_ptofs = (mcs->pxms_ap >> PXMC_SUBDIV(mcs)) + mcs->pxms_ptshift - ptindx;

          pxmc_set_flag(mcs, PXMS_PTI_b);
          pxmc_clear_flag(mcs, PXMS_PRA_b);
        }
        else
        {
          if (!(mcs->pxms_flg & PXMS_PTI_m))
            mcs->pxms_ptindx = ptindx;
        }
      } else {
        /* if phase table position to mask is know do fine phase table alignment */
        if (mcs->pxms_cfg & PXMS_CFG_I2PT_m) {
          int res;
          res = pxmc_inp_rocon_ptofs_from_index_poll(mcs, 0);
          if (res < 0) {
            pxmc_set_errno(mcs, PXMS_E_I2PT_TOOBIG);
          } else if (res) {
            pxmc_set_flag(mcs, PXMS_PTI_b);
            pxmc_set_flag(mcs, PXMS_PHA_b);
          }
        }
      }
      mcs->pxms_hal = hal_pos;
    }
   #endif
  }

  {
    /*wind_current[0]=(ADC->ADDR0 & 0xFFF0)>>4;*/
    /* FIXME - check winding current against limit */
    /* pxmc_set_errno(mcs, PXMS_E_WINDCURRENT); */
  }

  if (!sync_mode) {
    ptvang = mcs->pxms_ptvang;
    ene = mcs->pxms_ene;
  } else {
    ptvang = 0;
    ene = mcs->pxms_flg & PXMS_BSY_m? mcs->pxms_me: 0;
  }

  if (ene) {
    indx = mcs->pxms_ptindx;
#if 1
    /* tuning of magnetic field/voltage advance angle */
    indx += (mcs->pxms_s1 * mcs->pxms_as) >> (PXMC_SUBDIV(mcs) + 8);
#endif

    if (ene < 0)
    {
      /* Generating direction of stator mag. field for backward torque */
      ene = -ene;

      if ((indx -= ptvang) < 0)
        indx += mcs->pxms_ptirc;
    }
    else
    {
      /* Generating direction of stator mag. field for forward torque */
      if ((indx += ptvang) >= mcs->pxms_ptirc)
        indx -= mcs->pxms_ptirc;
    }

    if (mcs->pxms_ptscale_mult)
      indx = ((unsigned long)indx * mcs->pxms_ptscale_mult) >> mcs->pxms_ptscale_shift;

    pwm1 = mcs->pxms_ptptr1[indx];
    pwm2 = mcs->pxms_ptptr2[indx];
    pwm3 = mcs->pxms_ptptr3[indx];

#ifdef PXMC_WITH_PT_ZIC
    if (labs(mcs->pxms_as) < (10 << PXMC_SUBDIV(mcs)))
    {
      pwm1 &= ~PXMC_PT_ZIC_MASK;
      pwm2 &= ~PXMC_PT_ZIC_MASK;
      pwm3 &= ~PXMC_PT_ZIC_MASK;
    }
#endif /*PXMC_WITH_PT_ZIC*/

    /* Default phase-table amplitude is 0x7fff, ene max is 0x7fff */
    /* Initialized CTM4 PWM period is 0x200 => divide by value about 2097024 */
    {
      unsigned long pwm_dc = pxmc_rocon_pwm_magnitude * (unsigned long)ene;
      if (pwm1 & PXMC_PT_ZIC_MASK)
        pwm1 = 0x8000;
      else
        pwm1 = (((unsigned long long)pwm1 * pwm_dc) >> (15 + 15)) | 0x4000;
      if (pwm2 & PXMC_PT_ZIC_MASK)
        pwm2 = 0x8000;
      else
        pwm2 = (((unsigned long long)pwm2 * pwm_dc) >> (15 + 15)) | 0x4000;
      if (pwm3 & PXMC_PT_ZIC_MASK)
        pwm3 = 0x8000;
      else
        pwm3 = (((unsigned long long)pwm3 * pwm_dc) >> (15 + 15)) | 0x4000;
    }
    pxmc_rocon_pwm3ph_wr(mcs, pwm1, pwm2, pwm3);
  }
  else
  {
    pxmc_rocon_pwm3ph_wr(mcs, 0, 0, 0);
  }

  return 0;
}

/**
 * pxmc_rocon_pwm2ph_wr - Output of the 2-phase stepper motor PWM to the hardware
 * @mcs:	Motion controller state information
 */
static /*inline*/ void
pxmc_rocon_pwm2ph_wr(pxmc_state_t *mcs, short pwm1, short pwm2)
{
  volatile uint32_t *pwm_reg_ap, *pwm_reg_an, *pwm_reg_bp, *pwm_reg_bn;
  int chan = mcs->pxms_out_info;

  pwm_reg_bn = pxmc_rocon_pwm_chan2reg(chan + 0);
  pwm_reg_bp = pxmc_rocon_pwm_chan2reg(chan + 1);
  pwm_reg_an = pxmc_rocon_pwm_chan2reg(chan + 2);
  pwm_reg_ap = pxmc_rocon_pwm_chan2reg(chan + 3);

  if (pwm2 >= 0) {
    *pwm_reg_bp = pwm2 | 0x4000;
    *pwm_reg_bn = 0;
  } else {
    *pwm_reg_bp = 0;
    *pwm_reg_bn = -pwm2 | 0x4000;
  }

  if (pwm1 >= 0) {
    *pwm_reg_ap = pwm1 | 0x4000;
    *pwm_reg_an = 0;
  } else {
    *pwm_reg_ap = 0;
    *pwm_reg_an = -pwm1 | 0x4000;
  }
}

/**
 * pxmc_rocon_pwm2ph_out - Phase output of the 2-phase stepper motor PWM
 * @mcs:  Motion controller state information
 */
int
pxmc_rocon_pwm2ph_out(pxmc_state_t *mcs)
{
  typeof(mcs->pxms_ptvang) ptvang;
  int sync_mode = 0;
  short pwm1;
  short pwm2;
  int indx = 0;
  short ene;
  int wind_current[4];

  if(!(mcs->pxms_flg&PXMS_PTI_m) || !(mcs->pxms_flg&PXMS_PHA_m) ||
     (mcs->pxms_flg&PXMS_PRA_m)){
    short ptindx;
    short ptirc=mcs->pxms_ptirc;

   #if 0
    pxmc_irc_16bit_commindx(mcs, mcs->pxms_rp >> PXMC_SUBDIV(mcs));
    sync_mode = 1;
   #elif 1
    {
      int res;
      res = pxmc_inp_rocon_ptofs_from_index_poll(mcs, 0);
      if (res < 0) {
        pxmc_set_errno(mcs, PXMS_E_I2PT_TOOBIG);
      } else if (res) {
        pxmc_set_flag(mcs, PXMS_PTI_b);
        pxmc_set_flag(mcs, PXMS_PHA_b);
      }
    }
   #else

    ptindx = mcs->pxms_ptindx;

    if(0 && (!(mcs->pxms_flg&PXMS_PTI_m) || (mcs->pxms_flg&PXMS_PRA_m))) {

      mcs->pxms_ptindx = ptindx;

      mcs->pxms_ptofs = (mcs->pxms_ap >> PXMC_SUBDIV(mcs)) + mcs->pxms_ptshift - ptindx;

      pxmc_set_flag(mcs, PXMS_PTI_b);
      pxmc_clear_flag(mcs, PXMS_PRA_b);

      /* if phase table position to mask is know do fine phase table alignment */
      //if(mcs->pxms_cfg & PXMS_CFG_I2PT_m)
      //  lpc_qei_setup_index_catch(&lpc_qei_state);

    } else {
      if(!(mcs->pxms_flg&PXMS_PTI_m))
            mcs->pxms_ptindx = ptindx;
    }
   #endif
  }

  {
    /*wind_current[0]=(ADC->ADDR0 & 0xFFF0)>>4;*/
    /* FIXME - check winding current against limit */
    /* pxmc_set_errno(mcs, PXMS_E_WINDCURRENT); */
  }

  if (!sync_mode) {
    ptvang = mcs->pxms_ptvang;
    ene = mcs->pxms_ene;
  } else {
    ptvang = 0;
    ene = mcs->pxms_flg & PXMS_BSY_m? mcs->pxms_me: 0;
  }

  if (ene) {
    indx = mcs->pxms_ptindx;
   #if 0
    /* tuning of magnetic field/voltage advance angle */
    indx += (mcs->pxms_s1 * mcs->pxms_as) >> (PXMC_SUBDIV(mcs) + 8);
   #endif
    if (ene<0){
      /* Generating direction of stator mag. field for backward torque */
      ene = -ene;
      if ((indx -= ptvang)<0)
        indx += mcs->pxms_ptirc;
    }else{
      /* Generating direction of stator mag. field for forward torque */
      if ((indx += ptvang) >= mcs->pxms_ptirc)
        indx -= mcs->pxms_ptirc;
    }

    if (mcs->pxms_ptscale_mult)
      indx = ((unsigned long)indx * mcs->pxms_ptscale_mult) >> mcs->pxms_ptscale_shift;

    pwm1 = mcs->pxms_ptptr1[indx];
    pwm2 = mcs->pxms_ptptr2[indx];

    /* Default phase-table amplitude is 0x7fff, ene max is 0x7fff */
    /* Initialized PWM period is 0x200 => divide by value about 2097024 */
    {
      unsigned long pwm_dc = pxmc_rocon_pwm_magnitude * (unsigned long)ene;
      pwm1 = ((unsigned long long)pwm1 * pwm_dc) >> (15+15);
      pwm2 = ((unsigned long long)pwm2 * pwm_dc) >> (15+15);
    }
    pxmc_rocon_pwm2ph_wr(mcs, pwm1, pwm2);
  }else{
    /*pxmc_lpc_wind_current_over_cnt = 0;*/
    pxmc_rocon_pwm2ph_wr(mcs, 0, 0);
  }

  return 0;
}

/**
 * pxmc_rocon_pwm_dc_out - DC motor CW and CCW PWM output
 * @mcs:  Motion controller state information
 */
int
pxmc_rocon_pwm_dc_out(pxmc_state_t *mcs)
{
  volatile uint32_t *pwm_reg_a, *pwm_reg_b;
  int chan = mcs->pxms_out_info;
  int ene = mcs->pxms_ene;

  pwm_reg_a = pxmc_rocon_pwm_chan2reg(chan + 0);
  pwm_reg_b = pxmc_rocon_pwm_chan2reg(chan + 1);

  if (ene < 0) {
    ene = -ene;
    if (ene > 0x7fff)
      ene = 0x7fff;
    ene = (ene * (pxmc_rocon_pwm_magnitude + 5)) >> 15;
    *pwm_reg_a = 0;
    *pwm_reg_b = ene | 0x4000;
  } else {
    if (ene > 0x7fff)
      ene = 0x7fff;
    ene = (ene * (pxmc_rocon_pwm_magnitude + 5)) >> 15;
    *pwm_reg_b = 0;
    *pwm_reg_a = ene | 0x4000;
  }

  return 0;
}

/*******************************************************************/
/* PXMCC - PXMC coprocessor support and communication */

void pxmcc_pxmc_ptofs2mcc(pxmc_state_t *mcs, int enable_update)
{
  volatile pxmcc_axis_data_t *mcc_axis = pxmc_rocon_mcs2pxmcc(mcs);
  int inp_chan=mcs->pxms_inp_info;
  uint32_t ptofs;
  uint32_t irc;

  if (mcc_axis != NULL) {
    if (enable_update >= 0)
      mcc_axis->ptirc = 0xffffffff;
    ptofs = mcs->pxms_ptofs - pxmc_rocon_irc_offset[inp_chan];
    irc = fpga_irc[inp_chan]->count;
    ptofs = (int16_t)(ptofs - irc) + irc;
    mcc_axis->ptofs = ptofs;
    if (enable_update > 0) {
      mcc_axis->ptirc = mcs->pxms_ptirc;
    }
  }
}

static inline
void pxmcc_axis_get_cur_dq_filt_raw(pxmc_state_t *mcs,
                                    int32_t *p_cur_d_raw, int32_t *p_cur_q_raw)
{
  volatile pxmcc_axis_data_t *mcc_axis = pxmc_rocon_mcs2pxmcc(mcs);
  pxmc_rocon_state_t *mcsrc = pxmc_state2rocon_state(mcs);
  uint32_t cur_d_cum = mcc_axis->cur_d_cum;
  uint32_t cur_q_cum = mcc_axis->cur_q_cum;
  int32_t cur_d;
  int32_t cur_q;

  cur_d = cur_d_cum - mcsrc->cur_d_cum_prev;
  mcsrc->cur_d_cum_prev = cur_d_cum;
  cur_q = cur_q_cum - mcsrc->cur_q_cum_prev;
  mcsrc->cur_q_cum_prev = cur_q_cum;

  *p_cur_d_raw = cur_d;
  *p_cur_q_raw = cur_q;
}

static inline
void pxmcc_axis_cur_dq_raw2filt(int *p_cur, int32_t cur_raw)
{
  int cur_div;
  int32_t cur;
  cur_div = cur_raw & 0x1f;
  cur = cur_raw / cur_div;
  cur += (1 << 11) | 0x20;
  *p_cur = cur >> 12;
}


void pxmcc_axis_get_cur_dq_filt(pxmc_state_t *mcs, int *p_cur_d, int *p_cur_q)
{
  int32_t cur_d_raw;
  int32_t cur_q_raw;

  pxmcc_axis_get_cur_dq_filt_raw(mcs, &cur_d_raw, &cur_q_raw);

  pxmcc_axis_cur_dq_raw2filt(p_cur_d, cur_d_raw);
  pxmcc_axis_cur_dq_raw2filt(p_cur_q, cur_q_raw);
}

#ifdef PXMC_WITH_EXTENDED_STATE
int
pxmc_pxmcc_cur_acquire_and_irc_inp(pxmc_state_t *mcs)
{
  int cur_d;
  int cur_q;

  pxmc_inp_rocon_inp(mcs);
  pxmcc_axis_get_cur_dq_filt(mcs, &cur_d, &cur_q);
  mcs->pxms_cur_d_act = cur_d;
  mcs->pxms_cur_q_act = cur_q;

  return 0;
}
#endif /*PXMC_WITH_EXTENDED_STATE*/

static inline
void pxmcc_cur_ctrl_pi(int *p_pwm, int32_t *p_cur_err_sum,
               int cur_err, short cur_ctrl_p, short cur_ctrl_i, int max_pwm)
{
  int pwm;
  int32_t cur_err_sum = *p_cur_err_sum;

  pwm = (cur_err * cur_ctrl_p) >> 8;

  if (cur_ctrl_i)
    cur_err_sum += cur_err * cur_ctrl_i;
  else
    cur_err_sum = 0;

  pwm += cur_err_sum >> 16;

  if (pwm > max_pwm) {
    cur_err_sum -= (pwm - max_pwm) << 16;
    pwm = max_pwm;
  } else if (-pwm > max_pwm) {
    cur_err_sum -= (pwm + max_pwm) << 16;
    pwm = 0 - max_pwm;
  }
  *p_cur_err_sum = cur_err_sum;
  *p_pwm = pwm;
}

static inline void
pxmc_pxmcc_pwm3ph_hal_and_ptofs_start_and_check(pxmc_state_t *mcs)
{
  short ptindx;
  short ptirc = mcs->pxms_ptirc;
  short divisor = mcs->pxms_ptper * 6;
  unsigned char hal_pos;

  hal_pos = pxmc_lpc_bdc_hal_pos_table[pxmc_rocon_bldc_hal_rd(mcs)];

  if (hal_pos == 0xff) {
    if (mcs->pxms_ene)
      pxmc_rocon_process_hal_error(mcs);
  } else {
    if (mcs->pxms_halerc)
      mcs->pxms_halerc--;

    ptindx = (hal_pos * ptirc + divisor / 2) / divisor;

    if (!(mcs->pxms_flg & PXMS_PTI_m) || (mcs->pxms_flg & PXMS_PRA_m)) {
      int set_ptofs_fl = 0;
      int ptofs_enable_update = 0;

      if (((hal_pos != mcs->pxms_hal) && (mcs->pxms_hal != 0x40)) && 1) {
        short ptindx_prev = (mcs->pxms_hal * ptirc + divisor / 2) / divisor;;

        if ((ptindx > ptindx_prev + ptirc / 2) ||
            (ptindx_prev > ptindx + ptirc / 2)) {
          ptindx = (ptindx_prev + ptindx - ptirc) / 2;

          if (ptindx < 0)
            ptindx += ptirc;
        } else {
          ptindx = (ptindx_prev + ptindx) / 2;
        }

        set_ptofs_fl = 1;
        ptofs_enable_update = 1;

        pxmc_set_flag(mcs, PXMS_PTI_b);
        pxmc_clear_flag(mcs, PXMS_PRA_b);
      } else {
        if (!(mcs->pxms_flg & PXMS_PTI_m))
          set_ptofs_fl = 1;
      }
      if (set_ptofs_fl) {
        mcs->pxms_ptindx = ptindx;
        mcs->pxms_ptofs = (mcs->pxms_ap >> PXMC_SUBDIV(mcs)) + mcs->pxms_ptshift - ptindx;

        pxmcc_pxmc_ptofs2mcc(mcs, ptofs_enable_update);
      }
    } else {
      /* if phase table position to mask is know do fine phase table alignment */
      if (mcs->pxms_cfg & PXMS_CFG_I2PT_m) {
        int res;

        res = pxmc_inp_rocon_ptofs_from_index_poll(mcs, 0);
        if (res < 0) {
          pxmc_set_errno(mcs, PXMS_E_I2PT_TOOBIG);
        } else if (res) {
          pxmcc_pxmc_ptofs2mcc(mcs, 1);
          pxmc_set_flag(mcs, PXMS_PTI_b);
          pxmc_set_flag(mcs, PXMS_PHA_b);
        }
      }
    }
    mcs->pxms_hal = hal_pos;
  }
}

int
pxmc_pxmcc_pwm3ph_out(pxmc_state_t *mcs)
{
  int32_t cur_d_raw;
  int32_t cur_q_raw;
  int ene, pwm_d, pwm_q;

  pxmc_rocon_state_t *mcsrc = pxmc_state2rocon_state(mcs);

  if (!(mcs->pxms_flg & PXMS_PTI_m) || !(mcs->pxms_flg & PXMS_PHA_m) ||
      (mcs->pxms_flg & PXMS_PRA_m))
    pxmc_pxmcc_pwm3ph_hal_and_ptofs_start_and_check(mcs);

  pxmcc_axis_get_cur_dq_filt_raw(mcs, &cur_d_raw, &cur_q_raw);

  {
    /*wind_current[0]=(ADC->ADDR0 & 0xFFF0)>>4;*/
    /* FIXME - check winding current against limit */
    /* pxmc_set_errno(mcs, PXMS_E_WINDCURRENT); */
  }

  ene = mcs->pxms_ene;
  pwm_d = 0;
  pwm_q = (pxmc_rocon_pwm_magnitude * ene) >> 15;

  if (mcs->pxms_flg & (PXMS_ENR_m | PXMS_ENO_m)) {
    int cur_d;
    int cur_d_req, cur_d_err;
    int max_pwm = (pxmc_rocon_pwm_magnitude * mcs->pxms_me) >> 15;

    cur_d_req = 0;

    pxmcc_axis_cur_dq_raw2filt(&cur_d, cur_d_raw);

   #ifdef PXMC_WITH_EXTENDED_STATE
    {
      int cur_q;
      pxmcc_axis_cur_dq_raw2filt(&cur_q, cur_q_raw);
      mcs->pxms_cur_d_act = cur_d;
      mcs->pxms_cur_q_act = cur_q;
    }
   #endif

    cur_d_err = cur_d_req - cur_d;

    pxmcc_cur_ctrl_pi(&pwm_d, &mcsrc->cur_d_err_sum, cur_d_err,
                      mcsrc->cur_d_p, mcsrc->cur_d_i, max_pwm);

    if (pxmc_rocon_rx_err_level >= 2)
      pxmc_set_errno(mcs, PXMS_E_WINDCURADC);
    else if (pxmc_rocon_mcc_stuck)
      pxmc_set_errno(mcs, PXMS_E_MCC_FAULT);
  }

  pxmcc_axis_pwm_dq_out(mcs, pwm_d, pwm_q);

  if (mcs->pxms_flg & PXMS_ERR_m)
    pxmc_rocon_pwm3ph_wr(mcs, 0, 0, 0);

  return 0;
}

#ifdef PXMC_WITH_EXTENDED_STATE
int
pxmc_pxmcc_pwm3ph_dq_out(pxmc_state_t *mcs)
{
  int pwm_d = 0;
  int pwm_q = 0;

  if (!(mcs->pxms_flg & PXMS_PTI_m) || !(mcs->pxms_flg & PXMS_PHA_m) ||
      (mcs->pxms_flg & PXMS_PRA_m))
    pxmc_pxmcc_pwm3ph_hal_and_ptofs_start_and_check(mcs);

  if (mcs->pxms_flg & (PXMS_ENR_m | PXMS_ENO_m)) {
    if (pxmc_rocon_rx_err_level >= 2)
      pxmc_set_errno(mcs, PXMS_E_WINDCURADC);
    else if (pxmc_rocon_mcc_stuck)
      pxmc_set_errno(mcs, PXMS_E_MCC_FAULT);
    else {
      pwm_d = mcs->pxms_ene_d;
      pwm_q = mcs->pxms_ene; /* Q component */

      pwm_d = (pxmc_rocon_pwm_magnitude * pwm_d) >> 15;
      pwm_q = (pxmc_rocon_pwm_magnitude * pwm_q) >> 15;
    }
  }

  pxmcc_axis_pwm_dq_out(mcs, pwm_d, pwm_q);

  if (mcs->pxms_flg & PXMS_ERR_m)
    pxmc_rocon_pwm3ph_wr(mcs, 0, 0, 0);

  return 0;
}
#endif /*PXMC_WITH_EXTENDED_STATE*/

int
pxmc_pxmcc_pwm2ph_out(pxmc_state_t *mcs)
{
  pxmc_rocon_state_t *mcsrc = pxmc_state2rocon_state(mcs);
  int32_t cur_d_raw;
  int32_t cur_q_raw;

  pxmcc_axis_get_cur_dq_filt_raw(mcs, &cur_d_raw, &cur_q_raw);

  if(!(mcs->pxms_flg&PXMS_PTI_m) || !(mcs->pxms_flg&PXMS_PHA_m) ||
     (mcs->pxms_flg&PXMS_PRA_m)) {

    {
      /* Wait for index mark to align phases */
      int res;
      res = pxmc_inp_rocon_ptofs_from_index_poll(mcs, 0);
      if (res < 0) {
        pxmc_set_errno(mcs, PXMS_E_I2PT_TOOBIG);
      } else if (res) {
        pxmcc_pxmc_ptofs2mcc(mcs, 1);
        pxmc_set_flag(mcs, PXMS_PTI_b);
        pxmc_set_flag(mcs, PXMS_PHA_b);
      } else {
        pxmcc_pxmc_ptofs2mcc(mcs, 0);
      }
    }
  }

  {
    int ene, pwm_d, pwm_q;

    ene = mcs->pxms_ene;
    pwm_d = 0;

    if (mcs->pxms_flg & PXMS_PHA_m &&
        (mcs->pxms_flg & (PXMS_ENR_m | PXMS_ENO_m))) {
      int cur_d;
      int cur_d_req, cur_d_err;
      int max_pwm = (pxmc_rocon_pwm_magnitude * mcs->pxms_me) >> 15;

      cur_d_req = 0;

      pxmcc_axis_cur_dq_raw2filt(&cur_d, cur_d_raw);

      cur_d_err = cur_d_req - cur_d;

      pxmcc_cur_ctrl_pi(&pwm_d, &mcsrc->cur_d_err_sum, cur_d_err,
                      mcsrc->cur_d_p, mcsrc->cur_d_i, max_pwm);

      if (pxmc_rocon_rx_err_level >= 2)
        pxmc_set_errno(mcs, PXMS_E_WINDCURADC);
      else if (pxmc_rocon_mcc_stuck)
        pxmc_set_errno(mcs, PXMS_E_MCC_FAULT);
    }

    pwm_q = (pxmc_rocon_pwm_magnitude * ene) >> 15;

    pxmcc_axis_pwm_dq_out(mcs, pwm_d, pwm_q);

    if (mcs->pxms_flg & PXMS_ERR_m)
      pxmc_rocon_pwm2ph_wr(mcs, 0, 0);
  }

  return 0;
}

/**
 * pxmc_pxmcc_nofb2ph_inp - Dummy input for direct stepper motor control
 * @mcs:        Motion controller state information
 */
int
pxmc_pxmcc_nofb2ph_inp(pxmc_state_t *mcs)
{
  volatile pxmcc_axis_data_t *mcc_axis = pxmc_rocon_mcs2pxmcc(mcs);
  uint32_t steps_pos_act = mcc_axis->steps_pos;
  mcs->pxms_as = (int32_t)(steps_pos_act - mcs->pxms_ap);
  mcs->pxms_ap += mcs->pxms_as;
  return 0;
}

/**
 * pxmc_pxmcc_nofb_con - Empty controller for direct stepper motor control
 * @mcs:        Motion controller state information
 */
int
pxmc_pxmcc_nofb_con(pxmc_state_t *mcs)
{
  mcs->pxms_ene=mcs->pxms_me;
  return 0;
}

int
pxmc_pxmcc_nofb2ph_out(pxmc_state_t *mcs)
{
  pxmc_rocon_state_t *mcsrc = pxmc_state2rocon_state(mcs);
  volatile pxmcc_axis_data_t *mcc_axis = pxmc_rocon_mcs2pxmcc(mcs);
  int32_t cur_d_raw;
  int32_t cur_q_raw;

  pxmcc_axis_get_cur_dq_filt_raw(mcs, &cur_d_raw, &cur_q_raw);

  if ((mcs->pxms_flg & PXMS_ERR_m) ||
      !(mcs->pxms_flg & (PXMS_ENO_m | PXMS_ENR_m))) {
    pxmc_rocon_pwm2ph_wr(mcs, 0, 0);
    pxmcc_axis_pwm_dq_out(mcs, 0, 0);
    mcc_axis->steps_inc = 0;
    mcc_axis->steps_inc_next = 0;
    mcsrc->cur_d_err_sum = 0;
    mcsrc->cur_q_err_sum = 0;
  } else {
    int pwm_d, pwm_q;
    int cur_d, cur_q;
    int cur_d_req, cur_d_err;
    int cur_q_req, cur_q_err;
    int max_pwm = mcs->pxms_me;
    int32_t stpinc;
    int32_t stpdiv;

    pxmcc_axis_cur_dq_raw2filt(&cur_d, cur_d_raw);
    pxmcc_axis_cur_dq_raw2filt(&cur_q, cur_q_raw);

    if (!mcs->pxms_ene)
      cur_d_req = 0;
    else
      cur_d_req = mcsrc->cur_hold;

    cur_d_err = cur_d_req - cur_d;

    pxmcc_cur_ctrl_pi(&pwm_d, &mcsrc->cur_d_err_sum, cur_d_err,
                      mcsrc->cur_d_p, mcsrc->cur_d_i, max_pwm);

    /* pwm_d = (pxmc_rocon_pwm_magnitude * ene) >> 15; */

    cur_q_req = 0;

    cur_q_err = cur_q_req - cur_q;

    pxmcc_cur_ctrl_pi(&pwm_q, &mcsrc->cur_q_err_sum, cur_q_err,
                      mcsrc->cur_q_p, mcsrc->cur_q_i, max_pwm);

    pxmcc_axis_pwm_dq_out(mcs, pwm_d, pwm_q);

    stpinc = mcs->pxms_rp - mcsrc->steps_pos_prev;

    stpdiv = pxmc_rocon_rx_done_sqn_inc;
    mcc_axis->steps_inc_next = (stpinc + stpdiv / 2) / stpdiv;
    mcc_axis->steps_pos_next = mcsrc->steps_pos_prev;

    mcsrc->steps_pos_prev = mcs->pxms_rp;

    mcc_axis->steps_sqn_next = pxmc_rocon_rx_done_sqn +
                               pxmc_rocon_rx_done_sqn_inc - 1;

    if (pxmc_rocon_rx_err_level >= 2)
      pxmc_set_errno(mcs, PXMS_E_WINDCURADC);
    else if (pxmc_rocon_mcc_stuck)
      pxmc_set_errno(mcs, PXMS_E_MCC_FAULT);
  }

  return 0;
}

int
pxmc_pxmcc_dc_curctrl_out(pxmc_state_t *mcs)
{
  pxmc_rocon_state_t *mcsrc = pxmc_state2rocon_state(mcs);
  volatile uint32_t *pwm_reg_a, *pwm_reg_b;
  int chan = mcs->pxms_out_info;
  int32_t cur_d_raw;
  int32_t cur_q_raw;
  int pwm_d;
  int cur_d;
  int cur_d_req;
  int cur_d_err;

  pxmcc_axis_get_cur_dq_filt_raw(mcs, &cur_d_raw, &cur_q_raw);
  pxmcc_axis_cur_dq_raw2filt(&cur_d, cur_d_raw);
 #ifdef PXMC_WITH_EXTENDED_STATE
  mcs->pxms_cur_d_act = cur_d;
  mcs->pxms_cur_q_act = 0;
 #endif /*PXMC_WITH_EXTENDED_STATE*/

  cur_d_req = mcs->pxms_rp >> PXMC_SUBDIV(mcs);

  if ((mcs->pxms_flg & PXMS_ERR_m) ||
      !(mcs->pxms_flg & (PXMS_ENO_m | PXMS_ENR_m)) ||
      (cur_d_req == 0)) {
    pxmcc_axis_enable(mcs, 0);
    pxmcc_axis_pwm_dq_out(mcs, 0, 0);
    pwm_reg_a = pxmc_rocon_pwm_chan2reg(chan + 0);
    pwm_reg_b = pxmc_rocon_pwm_chan2reg(chan + 1);
    *pwm_reg_a = 0 | PXMC_PT_ZIC_MASK;
    *pwm_reg_b = 0 | PXMC_PT_ZIC_MASK;
    mcsrc->cur_d_err_sum = 0;
  } else {
    int max_pwm = (pxmc_rocon_pwm_magnitude * mcs->pxms_me) >> 15;

    pxmcc_axis_enable(mcs, 1);

    cur_d_err = cur_d_req - cur_d;

    pxmcc_cur_ctrl_pi(&pwm_d, &mcsrc->cur_d_err_sum, cur_d_err,
                      mcsrc->cur_d_p, mcsrc->cur_d_i, max_pwm);

    mcs->pxms_ene = pwm_d;
    pxmcc_axis_pwm_dq_out(mcs, pwm_d, 0);
  }

  return 0;
}


int pxmcc_axis_setup(pxmc_state_t *mcs, int mode)
{
  volatile pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();
  volatile pxmcc_axis_data_t *mcc_axis = pxmc_rocon_mcs2pxmcc(mcs);
  pxmc_rocon_state_t *mcsrc = pxmc_state2rocon_state(mcs);
  uint32_t ptirc;
  uint32_t ptreci;
  uint32_t inp_info;
  uint32_t pwmtx_info;
  uint32_t pwmtx_info_dummy = 27;
  uint64_t ull;
  int      i;
  int      phcnt = 0;
  int      pwm_chan;

  if (mcc_axis == NULL)
    return -1;

  if (mcc_data->common.fwversion != PXMCC_FWVERSION)
    return -1;

  mcc_axis->ccflg = 0;
  mcc_axis->mode = PXMCC_MODE_IDLE;

  mcc_data->common.pwm_cycle = PXMC_LXPWR_PWM_CYCLE;

  ptirc = mcs->pxms_ptirc;
  if (mode == PXMCC_MODE_STEPPER)
    ptirc <<= PXMC_SUBDIV(mcs);

  ull = (1ULL << 32) * mcs->pxms_ptper;
  ptreci = (ull + ptirc / 2) / ptirc;

  mcc_axis->ptreci = ptreci;

  pxmcc_pxmc_ptofs2mcc(mcs, 0);

  inp_info = (char*)&fpga_irc[mcs->pxms_inp_info]->count - (char*)fpga_irc[0];
  inp_info += mcc_data->common.irc_base;

  switch (mode) {
    case PXMCC_MODE_IDLE:
      phcnt = 0;
      break;
    case PXMCC_MODE_BLDC:
      phcnt = 3;
      break;
    case PXMCC_MODE_STEPPER_WITH_IRC:
      phcnt = 4;
      break;
    case PXMCC_MODE_STEPPER:
      phcnt = 4;
      inp_info = (char*)&mcc_axis->steps_pos - (char*)mcc_data;
      break;
    case PXMCC_MODE_DC_CUR:
      phcnt = 2;
      inp_info = (char*)&mcc_axis->steps_pos - (char*)mcc_data;
      mcc_axis->ptreci = 0x1 << 24;
      break;
  }

  mcc_axis->inp_info = inp_info;
  mcc_axis->out_info = mcs->pxms_out_info;

  pwm_chan = mcs->pxms_out_info;

  pwmtx_info = (pwmtx_info_dummy << 0) | (pwmtx_info_dummy << 8) |
               (pwmtx_info_dummy << 16) | (pwmtx_info_dummy << 24);

  for (i = phcnt; --i >= 0; ) {
    volatile uint32_t *pwm_reg;
    volatile uint32_t *pwm_reg_base = fpga_lx_master_transmitter_base;

    pwmtx_info <<= 8;

    pwm_reg = pxmc_rocon_pwm_chan2reg(pwm_chan + i);
    if (pwm_reg == &pxmc_rocon_pwm_dummy_reg) {
      pwmtx_info |= pwmtx_info_dummy;
    } else {
      pwmtx_info |= pwm_reg - pwm_reg_base;
    }
  }

  mcc_axis->pwmtx_info = pwmtx_info;

  mcc_axis->mode = mode;

  mcc_axis->ccflg = 0;
  mcc_axis->pwm_dq = 0;

  if (mode == PXMCC_MODE_STEPPER) {
    mcsrc->steps_pos_prev = mcs->pxms_rp = mcs->pxms_ap;
    mcs->pxms_rs = mcs->pxms_as = 0;
    mcc_axis->steps_inc_next = 0;
    mcc_axis->steps_pos_next = mcsrc->steps_pos_prev;
    mcc_axis->steps_inc = 0;
    mcc_axis->steps_pos = mcsrc->steps_pos_prev;
    mcc_axis->ptirc = mcs->pxms_ptirc << PXMC_SUBDIV(mcs);
  } else if (mode == PXMCC_MODE_DC_CUR) {
    mcsrc->steps_pos_prev = mcs->pxms_rp = 0;
    mcs->pxms_rs = mcs->pxms_as = 0;
    mcc_axis->steps_inc_next = 0;
    mcc_axis->steps_pos_next = 0;
    mcc_axis->steps_inc = 0;
    mcc_axis->steps_pos = 0;
    mcc_axis->ptirc = 0xffffffff;
  } else {
    pxmcc_pxmc_ptofs2mcc(mcs, 1);
  }
  return 0;
}

int pxmcc_curadc_zero(int wait)
{
  int chan;
  unsigned try = wait? 200: 0;
  volatile pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();
  volatile pxmcc_curadc_data_t *curadc;

  for (chan = 0; chan < PXMCC_CURADC_CHANNELS; chan++)
    pxmc_rocon_pwm_direct_wr(chan, 0, 0);

  do {
    if (mcc_data->common.fwversion == PXMCC_FWVERSION)
      break;
    if (!try--)
      return -1;
  } while(1);

  if (wait) {
    if (pxmc_rocon_wait_rx_done() < 0)
      return -1;

    if (pxmc_rocon_wait_rx_done() < 0)
      return -1;
  }

  for (chan = 0; chan < PXMCC_CURADC_CHANNELS; chan++) {
    curadc = mcc_data->curadc + chan;
    curadc->siroladc_offs += curadc->cur_val;
  }

  return 0;
}

/*******************************************************************/

volatile void *pxmc_rocon_rx_data_hist_buff;
volatile void *pxmc_rocon_rx_data_hist_buff_end;
int pxmc_rocon_rx_data_hist_mode;

uint32_t pxmc_rocon_rx_last_irq;
uint32_t pxmc_rocon_rx_cycle_time;
uint32_t pxmc_rocon_rx_irq_latency;
uint32_t pxmc_rocon_rx_irq_latency_max;

IRQ_HANDLER_FNC(pxmc_rocon_rx_done_isr)
{
  uint32_t ir;

  ir = ROCON_RX_TIM->IR & LPC_TIM_IR_ALL_m;
  ROCON_RX_TIM->IR = ir;
  if (ir & LPC_TIM_IR_CR1INT_m) {
    uint32_t cr0, cr1;
    cr0 = ROCON_RX_TIM->CR0;
    cr1 = ROCON_RX_TIM->CR1;

    pxmc_rocon_rx_cycle_time = cr1 - pxmc_rocon_rx_last_irq;
    pxmc_rocon_rx_last_irq = cr1;

    hal_gpio_set_value(T2MAT0_PIN, 1);
    hal_gpio_set_value(T2MAT1_PIN, 0);
    hal_gpio_set_value(T2MAT0_PIN, 0);

    pxmc_rocon_rx_done_sqn_compute();
    pxmc_rocon_vin_compute();
    pxmc_rocon_rx_error_check();

    if (pxmc_rocon_rx_data_hist_buff >= pxmc_rocon_rx_data_hist_buff_end)
      pxmc_rocon_rx_data_hist_buff = NULL;

    if (pxmc_rocon_rx_data_hist_buff != NULL) {
      if (pxmc_rocon_rx_data_hist_mode == 0) {
        int i;
        volatile uint32_t *pwm_reg = fpga_lx_master_transmitter_base + 8;
        volatile uint32_t *rec_reg = fpga_lx_master_receiver_base + 8;
        uint16_t *pbuf = (uint16_t *)pxmc_rocon_rx_data_hist_buff;
        for (i = 0; i < 8; i++) {
          *(pbuf++) = *(rec_reg++);
        }
        for (i = 0; i < 8; i++) {
          *(pbuf++) = *(pwm_reg++);
        }
        pxmc_rocon_rx_data_hist_buff = pbuf;
      } else if (pxmc_rocon_rx_data_hist_mode == 2) {
        uint32_t *pbuf = (uint32_t *)pxmc_rocon_rx_data_hist_buff;
        unsigned int as5pos;
        as5pos = as5_spi_get_pos(0);
        as5_spi_pos_update();
        if (pxmc_main_list.pxml_cnt > 0) {
          *(pbuf++) = pxmc_main_list.pxml_arr[0]->pxms_ap;
          *(pbuf++) = as5pos;
        }
        pxmc_rocon_rx_data_hist_buff = pbuf;
      } else if (!((pxmc_rocon_rx_data_hist_mode & 0xf8) ^ 0x10)) {
        int i;
        int chan = pxmc_rocon_rx_data_hist_mode & 7;
        uint32_t *pbuf = (uint32_t *)pxmc_rocon_rx_data_hist_buff;
        pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();
        pxmcc_axis_data_t *mcc_axis = mcc_data->axis + chan;
        uint32_t *ptumbl = (uint32_t *)mcc_axis;

        if (chan < PXMCC_AXIS_COUNT) {
          for (i = 0; i < 16; i++)
            *(pbuf++) = *(ptumbl++);
        }

        pxmc_rocon_rx_data_hist_buff = pbuf;
      } else if (!((pxmc_rocon_rx_data_hist_mode & 0xf8) ^ 0x20)) {
        uint32_t *pbuf = (uint32_t *)pxmc_rocon_rx_data_hist_buff;
        volatile pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();
        volatile pxmcc_curadc_data_t *curadc;
        pxmc_rocon_state_t *mcsrc = NULL;
        int chan = pxmc_rocon_rx_data_hist_mode & 7;
        if (chan < pxmc_main_list.pxml_cnt)
          mcsrc = pxmc_state2rocon_state(pxmc_main_list.pxml_arr[chan]);
        if (mcsrc) {
          *(pbuf++) = pxmc_rocon_vin_act;
          chan = mcsrc->base.pxms_inp_info;
          *(pbuf++) = fpga_irc[chan]->count + pxmc_rocon_irc_offset[chan];;
          *(pbuf++) = mcsrc->base.pxms_rp >> PXMC_SUBDIV(&mcsrc->base);
          *(pbuf++) = mcsrc->base.pxms_ene;
          *(pbuf++) = mcsrc->base.pxms_foi;
          chan = mcsrc->base.pxms_out_info;
          curadc = mcc_data->curadc + chan;
          *(pbuf++) = *pxmc_rocon_pwm_chan2reg(chan++);
          *(pbuf++) = (curadc++)->cur_val;
          *(pbuf++) = *pxmc_rocon_pwm_chan2reg(chan++);
          *(pbuf++) = (curadc++)->cur_val;
          *(pbuf++) = *pxmc_rocon_pwm_chan2reg(chan++);
          *(pbuf++) = (curadc++)->cur_val;
          *(pbuf++) = *pxmc_rocon_pwm_chan2reg(chan++);
          *(pbuf++) = (curadc++)->cur_val;
          pxmc_rocon_rx_data_hist_buff = pbuf;
        }
      }
    }

    pxmc_rocon_rx_irq_latency = ROCON_RX_TIM->TC - cr1;
    if (pxmc_rocon_rx_irq_latency > pxmc_rocon_rx_irq_latency_max)
      pxmc_rocon_rx_irq_latency_max = pxmc_rocon_rx_irq_latency;

   #ifdef PXMC_ROCON_TIMED_BY_RX_DONE
    pxmc_sfi_isr();
    do_pxmc_coordmv();
   #ifdef APPL_WITH_DIO
    do_dio_proc();
   #endif /*APPL_WITH_DIO*/
   #ifdef APPL_WITH_CMPQUE
    do_pxmc_cmpque();
   #endif /*APPL_WITH_CMPQUE*/
   #endif /*PXMC_ROCON_TIMED_BY_RX_DONE*/
  }

  return IRQ_HANDLED;
}

int
pxmc_rocon_rx_done_isr_setup(irq_handler_t rx_done_isr_handler)
{

  disable_irq(ROCON_RX_IRQn);

  hal_pin_conf_set(T2MAT0_PIN, PORT_CONF_GPIO_OUT_LO);
  hal_pin_conf_set(T2MAT1_PIN, PORT_CONF_GPIO_OUT_LO);
  hal_pin_conf(T2CAP0_PIN);
  hal_pin_conf(T2CAP1_PIN);

  hal_gpio_direction_output(T2MAT0_PIN, 1);
  hal_gpio_direction_output(T2MAT1_PIN, 0);
  hal_gpio_set_value(T2MAT0_PIN, 0);

  /* Enable CLKOUT pin function, source CCLK, divide by 1 */
  LPC_SC->CLKOUTCFG = 0x0100;

  request_irq(ROCON_RX_IRQn, rx_done_isr_handler, 0, NULL,NULL);

  ROCON_RX_TIM->TCR = 0;
  ROCON_RX_TIM->CTCR = 0;
  ROCON_RX_TIM->PR = 0;	/* Divide by 1 */

  ROCON_RX_TIM->CCR = LPC_TIM_CCR_CAP0RE_m | LPC_TIM_CCR_CAP1FE_m |
                   LPC_TIM_CCR_CAP1I_m;

  ROCON_RX_TIM->EMR = __val2mfld(LPC_TIM_EMR_EMC0_m, LPC_TIM_EMR_NOP) |
                   __val2mfld(LPC_TIM_EMR_EMC1_m, LPC_TIM_EMR_NOP);

  ROCON_RX_TIM->MCR = 0;			/* No IRQ on MRx */
  ROCON_RX_TIM->TCR = LPC_TIM_TCR_CEN_m;	/* Enable timer counting */
  enable_irq(ROCON_RX_IRQn);		/* Enable interrupt */

  return 0;

}

int
pxmc_rocon_pwm_master_setup(unsigned lxpwr_chips)
{
  int i;
  int grp_in = 0;
  int grp_out = 0;
  unsigned word_slot;
  unsigned receiver_done_div = 1;
  unsigned lxpwr_chips_max = 2;
 #ifdef LXPWR_WITH_SIROLADC
  unsigned lxpwr_header = 1;
  unsigned lxpwr_words = 1 + 8 * 2 + 2;
  unsigned lxpwr_chip_pwm_cnt = 8;
 #else /*LXPWR_WITH_SIROLADC*/
  unsigned lxpwr_header = 0;
  unsigned lxpwr_words = 8;
  unsigned lxpwr_chip_pwm_cnt = 8;
 #endif /*LXPWR_WITH_SIROLADC*/

 #ifdef PXMC_ROCON_TIMED_BY_RX_DONE
  receiver_done_div = 5;
 #endif /*PXMC_ROCON_TIMED_BY_RX_DONE*/

  *fpga_lx_master_reset = 1;
  *fpga_lx_master_transmitter_reg = 0;
  *fpga_lx_master_transmitter_cycle = PXMC_LXPWR_PWM_CYCLE; /* 50 MHz -> 20 kHz */
  *fpga_lx_master_receiver_done_div = receiver_done_div << 8;
  pxmc_rocon_rx_done_sqn_inc = receiver_done_div;

  if (lxpwr_chips > lxpwr_chips_max)
    return -1;

  for (i = 0; i < LX_MASTER_DATA_OFFS + lxpwr_words * lxpwr_chips_max; i++)
    fpga_lx_master_receiver_base[i] = 0;

  if (lxpwr_chips >= 2) {
    word_slot = LX_MASTER_DATA_OFFS + lxpwr_words;
    fpga_lx_master_receiver_base[grp_in++] = (word_slot << 8) | lxpwr_words;
  }

  word_slot = LX_MASTER_DATA_OFFS;
  fpga_lx_master_receiver_base[grp_in++] = (word_slot << 8) | lxpwr_words;

  fpga_lx_master_receiver_base[grp_in++] = 0x0000;

  for (i = 0; i < LX_MASTER_DATA_OFFS + lxpwr_words * lxpwr_chips_max; i++)
    fpga_lx_master_transmitter_base[i] = 0;

  word_slot = LX_MASTER_DATA_OFFS + lxpwr_header + lxpwr_chip_pwm_cnt;
  fpga_lx_master_transmitter_base[grp_out++] = (word_slot << 8) | lxpwr_words;
 #ifdef LXPWR_WITH_SIROLADC
  fpga_lx_master_transmitter_base[word_slot] = 0xc100 | (lxpwr_words - 1);
 #endif /*LXPWR_WITH_SIROLADC*/

  word_slot = LX_MASTER_DATA_OFFS + 0;
  fpga_lx_master_transmitter_base[grp_out++] = (word_slot << 8) | lxpwr_words;
 #ifdef LXPWR_WITH_SIROLADC
  fpga_lx_master_transmitter_base[word_slot] = 0xc100 | (lxpwr_words - 1);
 #endif /*LXPWR_WITH_SIROLADC*/

  fpga_lx_master_transmitter_base[grp_out++] = 0x0000;

  *fpga_lx_master_reset = 0;
  *fpga_lx_master_transmitter_cycle = PXMC_LXPWR_PWM_CYCLE; /* 50 MHz -> 20 kHz */
  *fpga_lx_master_receiver_done_div = receiver_done_div << 8;

  return 0;
}

int
pxmc_rocon_wait_rx_done(void)
{
  uint32_t sqn_last;
  uint32_t sqn_act;
  uint32_t timeout = 10000;

  sqn_last = *fpga_lx_master_receiver_done_div;
  sqn_last = sqn_last & 0x1f;

  do {
    sqn_act = *fpga_lx_master_receiver_done_div;
    sqn_act = sqn_act & 0x1f;
    if (sqn_act != sqn_last)
      return 0;
  } while(timeout--);

  return -1;
}

int
pxmc_rocon_pwm_master_init(void)
{
  int res;
  volatile uint32_t *lxpwr_header_ptr;
  unsigned lxpwr_words = 1 + 8 * 2 + 2;

  pxmc_rocon_lxpwr_chips = 0;

  res = pxmc_rocon_pwm_master_setup(2);
  if (res < 0)
    return 0;

  if (pxmc_rocon_wait_rx_done() < 0)
    return -1;
  if (pxmc_rocon_wait_rx_done() < 0)
    return -1;

  lxpwr_header_ptr = fpga_lx_master_receiver_base;
  lxpwr_header_ptr += LX_MASTER_DATA_OFFS;

  if (lxpwr_header_ptr[0] == 0xb100 + lxpwr_words - 1) {
    if (lxpwr_header_ptr[lxpwr_words] == 0xb100 + lxpwr_words - 1) {
      pxmc_rocon_lxpwr_chips = 2;
      return 2;
    }
    return -1;
  }

  if (lxpwr_header_ptr[lxpwr_words] != 0xb100 + lxpwr_words - 1) {
    return -1;
  }

  res = pxmc_rocon_pwm_master_setup(1);
  if (res < 0)
    return 0;

  if (pxmc_rocon_wait_rx_done() < 0)
    return -1;
  if (pxmc_rocon_wait_rx_done() < 0)
    return -1;

  if (lxpwr_header_ptr[0] != 0xb100 + lxpwr_words - 1)
    return -1;

  pxmc_rocon_lxpwr_chips = 1;

  return 1;
}

int pxmc_ptofs_from_index(pxmc_state_t *mcs, unsigned long irc,
                           unsigned long index_irc, int diff2err)
{
  long ofsl;
  short ofs;

  ofs = ofsl = index_irc - mcs->pxms_ptmark;

  if (diff2err) {
    short diff;
    diff = (unsigned short)ofs - (unsigned short)mcs->pxms_ptofs;
    if (diff >= mcs->pxms_ptirc / 2)
      diff -= mcs->pxms_ptirc;
    if (diff <= -mcs->pxms_ptirc / 2)
      diff += mcs->pxms_ptirc;
    if (diff < 0)
      diff = -diff;
    if(diff >= mcs->pxms_ptirc / 6) {
      return -1;
    }
  } else {
    long diff;
    diff = (unsigned long)ofsl - irc;
    ofs = ofsl - (diff / mcs->pxms_ptirc) * mcs->pxms_ptirc;
  }

  mcs->pxms_ptofs = ofs;

  return 1;
}

/**
 * pxmc_dummy_con - Dummy controller for synchronous BLDC/PMSM/steper drive
 * @mcs:        Motion controller state information
 */
int
pxmc_dummy_con(pxmc_state_t *mcs)
{
  return 0;
}

int pxmc_rocon_hh_gd10(pxmc_state_t *mcs);
int pxmc_rocon_hh_gd20(pxmc_state_t *mcs);
int pxmc_rocon_hh_gd30(pxmc_state_t *mcs);
int pxmc_rocon_hh_gd40(pxmc_state_t *mcs);
int pxmc_rocon_hh_gd50(pxmc_state_t *mcs);
int pxmc_rocon_hh_gd90(pxmc_state_t *mcs);

/* initialize for hard home */
int
pxmc_rocon_hh_gi(pxmc_state_t *mcs)
{
  pxmc_set_flag(mcs,PXMS_BSY_b);
 #if 0 /* config and speed already set in pxmc_hh */
  spd = mcs->pxms_ms;
  spd >>= mcs->pxms_cfg & PXMS_CFG_HSPD_m;
  if(!spd) spd = 1;
  if(mcs->pxms_cfg & PXMS_CFG_HDIR_m)
    spd = -spd;
  mcs->pxms_gen_hsp = spd;
 #endif
  mcs->pxms_rsfg = 0;
  mcs->pxms_gen_htim = 16;
  mcs->pxms_do_gen = pxmc_rocon_hh_gd10;
  return pxmc_rocon_hh_gd10(mcs);
}

/* fill initial mark filter and then decide */
int
pxmc_rocon_hh_gd10(pxmc_state_t *mcs)
{
  int mark;

  if(mcs->pxms_flg & PXMS_ERR_m)
    return pxmc_spdnext_gend(mcs);

  pxmc_spd_gacc(&(mcs->pxms_rs), 0, mcs->pxms_ma);
  mcs->pxms_rp += mcs->pxms_rs;

  mark = pxmc_inp_rocon_is_mark(mcs);

  if (mcs->pxms_gen_htim) {
    mcs->pxms_gen_htim--;
    return 0;
  }

  if (!(mcs->pxms_cfg & PXMS_CFG_HLS_m)) {
    /* limit switch is not used */
    pxmc_inp_rocon_is_index_edge(mcs, 1);
    mcs->pxms_do_gen = pxmc_rocon_hh_gd50;
    return 0;
  }

  if (mark >= 6) {
    /* go out from switch */
    mcs->pxms_do_gen = pxmc_rocon_hh_gd20;
  } else {
    /* switch is off -> look for it */
    mcs->pxms_do_gen = pxmc_rocon_hh_gd30;
  }
  return 0;
}

/* go out from switch */
int
pxmc_rocon_hh_gd20(pxmc_state_t *mcs)
{
  int mark;

  if(mcs->pxms_flg & PXMS_ERR_m)
    return pxmc_spdnext_gend(mcs);

  mark = pxmc_inp_rocon_is_mark(mcs);

  if (mark <= 1) {
    /* switch is off -> look for it again */
    mcs->pxms_do_gen = pxmc_rocon_hh_gd30;
  }

  pxmc_spd_gacc(&(mcs->pxms_rs), -mcs->pxms_gen_hsp, mcs->pxms_ma);
  mcs->pxms_rp += mcs->pxms_rs;

  return 0;
}

/* switch is off -> look for it */
int
pxmc_rocon_hh_gd30(pxmc_state_t *mcs)
{
  long spd;
  int mark;

  if (mcs->pxms_flg & PXMS_ERR_m)
    return pxmc_spdnext_gend(mcs);

  pxmc_spd_gacc(&(mcs->pxms_rs), mcs->pxms_gen_hsp, mcs->pxms_ma);
  mcs->pxms_rp += mcs->pxms_rs;

  mark = pxmc_inp_rocon_is_mark(mcs);

  if (mark >= 8){
    spd = mcs->pxms_gen_hsp >> 2; /* slow down */
    if (!spd)
      spd = 1;
    mcs->pxms_gen_hsp = spd;
    mcs->pxms_do_gen = pxmc_rocon_hh_gd40;
  }

  return 0;
}

/* switch is on -> find edge */
int
pxmc_rocon_hh_gd40(pxmc_state_t *mcs)
{
  int mark;

  if (mcs->pxms_flg & PXMS_ERR_m)
    return pxmc_spdnext_gend(mcs);

  mark = pxmc_inp_rocon_is_mark(mcs);

  if (mark <= 1) {
    if (mcs->pxms_cfg & PXMS_CFG_HRI_m) {
      pxmc_inp_rocon_is_index_edge(mcs, 1);
      mcs->pxms_do_gen = pxmc_rocon_hh_gd50;
    } else {
      pxmc_inp_rocon_ap_zero(mcs);
      if (mcs->pxms_do_inp == pxmc_pxmcc_nofb2ph_inp) {
        long irc_diff = -mcs->pxms_ap >> PXMC_SUBDIV(mcs);
        pxmc_inp_rocon_adjust_to_irc_change(mcs, irc_diff);
      }
      mcs->pxms_do_gen = pxmc_stop_gi;
    }
  }

  pxmc_spd_gacc(&(mcs->pxms_rs), -mcs->pxms_gen_hsp, mcs->pxms_ma);
  mcs->pxms_rp += mcs->pxms_rs;

  return 0;
}

/* calibrate on revolution index */
int
pxmc_rocon_hh_gd50(pxmc_state_t *mcs)
{
  if (mcs->pxms_flg & PXMS_ERR_m)
    return pxmc_spdnext_gend(mcs);

  pxmc_spd_gacc(&(mcs->pxms_rs), mcs->pxms_gen_hsp, mcs->pxms_ma);
  mcs->pxms_rp += mcs->pxms_rs;

  if (pxmc_inp_rocon_is_index_edge(mcs, 1)){
    pxmc_inp_rocon_irc_offset_from_index(mcs);
    if (mcs->pxms_do_inp == pxmc_pxmcc_nofb2ph_inp) {
      long irc_diff = -mcs->pxms_ap >> PXMC_SUBDIV(mcs);
      pxmc_inp_rocon_adjust_to_irc_change(mcs, irc_diff);
    }
    mcs->pxms_do_gen = pxmc_stop_gi;
  }

  return 0;
}

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_rocon_hh_gi;
}

/*******************************************************************/

pxmc_rocon_state_t mcs0 =
{
.base = {
pxms_flg:
  PXMS_ENI_m,
pxms_do_inp:
  pxmc_inp_rocon_inp,
pxms_do_con:
  pxmc_pid_con /*pxmc_dummy_con*/,
pxms_do_out:
  pxmc_rocon_pwm_dc_out /*pxmc_rocon_pwm3ph_out*/,
  pxms_do_deb: 0,
  pxms_do_gen: 0,
pxms_do_ap2hw:
  pxmc_inp_rocon_ap2hw,
  pxms_ap: 0, pxms_as: 0,
  pxms_rp: 55 * 256, pxms_rs: 0, pxms_subdiv: 8,
  pxms_md: 800 << 8, pxms_ms: 500, pxms_ma: 10,
  pxms_inp_info: 0,
  pxms_out_info: 0,
  pxms_ene: 0, pxms_erc: 0,
  pxms_p: 80, pxms_i: 10, pxms_d: 200, pxms_s1: 200, pxms_s2: 0,
  pxms_me: 0x7e00/*0x7fff*/,
pxms_cfg:
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
  PXMS_CFG_HRI_m * 0 | PXMS_CFG_HDIR_m * 0 |
  PXMS_CFG_I2PT_m * 0 | 0x2,

  pxms_ptper: 1,
  pxms_ptirc: 1000,
  pxms_ptmark: 1180,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
},
  .cur_d_p = 150,
  .cur_d_i = 6000,
  .cur_q_p = 150,
  .cur_q_i = 6000,
  .cur_hold = 200,
};

pxmc_rocon_state_t mcs1 =
{
.base = {
pxms_flg:
  PXMS_ENI_m,
pxms_do_inp:
  pxmc_inp_rocon_inp,
pxms_do_con:
  pxmc_pid_con,
pxms_do_out:
  pxmc_rocon_pwm_dc_out,
  pxms_do_deb: 0,
  pxms_do_gen: 0,
pxms_do_ap2hw:
  pxmc_inp_rocon_ap2hw,
  pxms_ap: 0, pxms_as: 0,
  pxms_rp: 55 * 256, pxms_rs: 0, pxms_subdiv: 8,
  pxms_md: 800 << 8, pxms_ms: 500, pxms_ma: 10,
  pxms_inp_info: 1,
  pxms_out_info: 2,
  pxms_ene: 0, pxms_erc: 0,
  pxms_p: 80, pxms_i: 10, pxms_d: 200, pxms_s1: 200, pxms_s2: 0,
  pxms_me: 0x7e00/*0x7fff*/,
pxms_cfg:
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
  PXMS_CFG_HRI_m * 0 | PXMS_CFG_HDIR_m * 0 |
  PXMS_CFG_I2PT_m * 0 | 0x2,

  pxms_ptper: 1,
  pxms_ptirc: 1000,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
}};

pxmc_rocon_state_t mcs2 =
{
.base = {
pxms_flg:
  PXMS_ENI_m,
pxms_do_inp:
  pxmc_inp_rocon_inp,
pxms_do_con:
  pxmc_pid_con,
pxms_do_out:
  pxmc_rocon_pwm_dc_out,
  pxms_do_deb: 0,
  pxms_do_gen: 0,
pxms_do_ap2hw:
  pxmc_inp_rocon_ap2hw,
  pxms_ap: 0, pxms_as: 0,
  pxms_rp: 55 * 256, pxms_rs: 0, pxms_subdiv: 8,
  pxms_md: 800 << 8, pxms_ms: 500, pxms_ma: 10,
  pxms_inp_info: 2,
  pxms_out_info: 4,
  pxms_ene: 0, pxms_erc: 0,
  pxms_p: 80, pxms_i: 10, pxms_d: 200, pxms_s1: 200, pxms_s2: 0,
  pxms_me: 0x7e00/*0x7fff*/,
pxms_cfg:
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
  PXMS_CFG_I2PT_m * 0 | PXMS_CFG_HRI_m |
  PXMS_CFG_HDIR_m | 0x2,

  pxms_ptper: 1,
  pxms_ptirc: 1000,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
}};

pxmc_rocon_state_t mcs3 =
{
.base = {
pxms_flg:
  PXMS_ENI_m,
pxms_do_inp:
  pxmc_inp_rocon_inp,
pxms_do_con:
  pxmc_pid_con,
pxms_do_out:
  pxmc_rocon_pwm_dc_out,
  pxms_do_deb: 0,
  pxms_do_gen: 0,
pxms_do_ap2hw:
  pxmc_inp_rocon_ap2hw,
  pxms_ap: 0, pxms_as: 0,
  pxms_rp: 55 * 256, pxms_rs: 0, pxms_subdiv: 8,
  pxms_md: 800 << 8, pxms_ms: 500, pxms_ma: 10,
  pxms_inp_info: 3,
  pxms_out_info: 6,
  pxms_ene: 0, pxms_erc: 0,
  pxms_p: 80, pxms_i: 10, pxms_d: 200, pxms_s1: 200, pxms_s2: 0,
  pxms_me: 0x7e00/*0x7fff*/,
pxms_cfg:
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
  PXMS_CFG_I2PT_m * 0 | PXMS_CFG_HRI_m |
  PXMS_CFG_HDIR_m * 0 | 0x2,

  pxms_ptper: 1,
  pxms_ptirc: 1000,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
}};

pxmc_rocon_state_t mcs4 =
{
.base = {
pxms_flg:
  PXMS_ENI_m,
pxms_do_inp:
  pxmc_inp_rocon_inp,
pxms_do_con:
  pxmc_pid_con,
pxms_do_out:
  pxmc_rocon_pwm_dc_out,
  pxms_do_deb: 0,
  pxms_do_gen: 0,
pxms_do_ap2hw:
  pxmc_inp_rocon_ap2hw,
  pxms_ap: 0, pxms_as: 0,
  pxms_rp: 55 * 256, pxms_rs: 0, pxms_subdiv: 8,
  pxms_md: 800 << 8, pxms_ms: 500, pxms_ma: 10,
  pxms_inp_info: 4,
  pxms_out_info: 8,
  pxms_ene: 0, pxms_erc: 0,
  pxms_p: 80, pxms_i: 10, pxms_d: 200, pxms_s1: 200, pxms_s2: 0,
  pxms_me: 0x7e00/*0x7fff*/,
pxms_cfg:
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
  PXMS_CFG_HRI_m | PXMS_CFG_I2PT_m * 0 |
  PXMS_CFG_HDIR_m | 0x2,

  pxms_ptper: 1,
  pxms_ptirc: 1000,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
}};

pxmc_rocon_state_t mcs5 =
{
.base = {
pxms_flg:
  PXMS_ENI_m,
pxms_do_inp:
  pxmc_inp_rocon_inp,
pxms_do_con:
  pxmc_pid_con,
pxms_do_out:
  pxmc_rocon_pwm_dc_out,
  pxms_do_deb: 0,
  pxms_do_gen: 0,
pxms_do_ap2hw:
  pxmc_inp_rocon_ap2hw,
  pxms_ap: 0, pxms_as: 0,
  pxms_rp: 55 * 256, pxms_rs: 0, pxms_subdiv: 8,
  pxms_md: 800 << 8, pxms_ms: 500, pxms_ma: 10,
  pxms_inp_info: 5,
  pxms_out_info: 10,
  pxms_ene: 0, pxms_erc: 0,
  pxms_p: 80, pxms_i: 10, pxms_d: 200, pxms_s1: 200, pxms_s2: 0,
  pxms_me: 0x7e00/*0x7fff*/,
pxms_cfg:
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m |
  PXMS_CFG_HRI_m | PXMS_CFG_I2PT_m * 0 |
  PXMS_CFG_HDIR_m | 0x2,

  pxms_ptper: 1,
  pxms_ptirc: 1000,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
}};

pxmc_rocon_state_t mcs6 =
{
.base = {
pxms_flg:
  PXMS_ENI_m,
pxms_do_inp:
  pxmc_inp_rocon_inp,
pxms_do_con:
  pxmc_pid_con,
pxms_do_out:
  pxmc_rocon_pwm_dc_out,
  pxms_do_deb: 0,
  pxms_do_gen: 0,
pxms_do_ap2hw:
  pxmc_inp_rocon_ap2hw,
  pxms_ap: 0, pxms_as: 0,
  pxms_rp: 55 * 256, pxms_rs: 0, pxms_subdiv: 8,
  pxms_md: 800 << 8, pxms_ms: 500, pxms_ma: 10,
  pxms_inp_info: 6,
  pxms_out_info: 12,
  pxms_ene: 0, pxms_erc: 0,
  pxms_p: 80, pxms_i: 10, pxms_d: 200, pxms_s1: 200, pxms_s2: 0,
  pxms_me: 0x7e00/*0x7fff*/,
pxms_cfg:
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_I2PT_m * 0 |
  0x1,

  pxms_ptper: 1,
  pxms_ptirc: 1000,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
}};

pxmc_rocon_state_t mcs7 =
{
.base = {
pxms_flg:
  PXMS_ENI_m,
pxms_do_inp:
  pxmc_inp_rocon_inp,
pxms_do_con:
  pxmc_pid_con,
pxms_do_out:
  pxmc_rocon_pwm_dc_out,
  pxms_do_deb: 0,
  pxms_do_gen: 0,
pxms_do_ap2hw:
  pxmc_inp_rocon_ap2hw,
  pxms_ap: 0, pxms_as: 0,
  pxms_rp: 55 * 256, pxms_rs: 0, pxms_subdiv: 8,
  pxms_md: 800 << 8, pxms_ms: 500, pxms_ma: 10,
  pxms_inp_info: 7,
  pxms_out_info: 14,
  pxms_ene: 0, pxms_erc: 0,
  pxms_p: 80, pxms_i: 10, pxms_d: 200, pxms_s1: 200, pxms_s2: 0,
  pxms_me: 0x7e00/*0x7fff*/,
pxms_cfg:
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_I2PT_m * 0 |
  0x1,

  pxms_ptper: 1,
  pxms_ptirc: 1000,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
}};

pxmc_state_t *pxmc_main_arr[PXML_MAIN_CNT] =
{&mcs0.base, &mcs1.base, &mcs2.base, &mcs3.base,
 &mcs4.base, &mcs5.base, &mcs6.base, &mcs7.base};

/*******************************************************************/
#ifdef APPL_CONFIG_BOX4PMSM8DC
#define APPL_PXMC_FILL_BY_DEFAULT
#define APPL_PXMC_FILL_OUT_INFO_SPAN     4
#define APPL_PXMC_FILL_OUT_INFO_ROW_GAP  2

const pxmc_rocon_state_t pxmc_mcs_default_parameters =
{
.base = {
pxms_flg:
  PXMS_ENI_m,
pxms_do_inp:
  pxmc_inp_rocon_inp,
pxms_do_con:
  pxmc_pid_con /*pxmc_dummy_con*/,
pxms_do_out:
  pxmc_rocon_pwm_dc_out /*pxmc_rocon_pwm3ph_out*/,
  pxms_do_deb: 0,
  pxms_do_gen: 0,
pxms_do_ap2hw:
  pxmc_inp_rocon_ap2hw,
  pxms_ap: 0, pxms_as: 0,
  pxms_rp: 55 * 256, pxms_rs: 0, pxms_subdiv: 8,
  pxms_md: 800 << 8, pxms_ms: 500, pxms_ma: 10,
  pxms_inp_info: 0,
  pxms_out_info: 0,
  pxms_ene: 0, pxms_erc: 0,
  pxms_p: 80, pxms_i: 10, pxms_d: 200, pxms_s1: 200, pxms_s2: 0,
  pxms_me: 0x7e00/*0x7fff*/,
pxms_cfg:
  PXMS_CFG_SMTH_m | PXMS_CFG_MD2E_m | PXMS_CFG_HLS_m | PXMS_CFG_HPS_m * 0 |
  PXMS_CFG_HRI_m * 0 | PXMS_CFG_HDIR_m * 0 |
  PXMS_CFG_I2PT_m * 0 | 0x2,

  pxms_ptper: 1,
  pxms_ptirc: 1000,
  pxms_ptmark: 1180,
  /*pxms_ptamp: 0x7fff,*/

  pxms_hal: 0x40,
},
  .cur_d_p = 150,
  .cur_d_i = 6000,
  .cur_q_p = 150,
  .cur_q_i = 6000,
  .cur_hold = 200,
};

#endif /*APPL_CONFIG_BOX4PMSM8DC*/
/*******************************************************************/

pxmc_state_list_t  pxmc_main_list =
{
pxml_arr:
  pxmc_main_arr,
  pxml_cnt: 0
};

void pxmc_lpc_qei_callback_index(struct lpc_qei_state_t *qst, void *context)
{
  pxmc_state_t *mcs = (pxmc_state_t *)context;
  short ofs;
  short irc;
  irc = qst->index_pos;

  if ((mcs->pxms_cfg & PXMS_CFG_I2PT_m) && (mcs->pxms_flg & PXMS_PTI_m))
  {
    short diff;
    ofs = irc - mcs->pxms_ptmark;
    diff = ofs - mcs->pxms_ptofs;

    if (diff >= mcs->pxms_ptirc / 2)
      diff -= mcs->pxms_ptirc;

    if (diff <= -mcs->pxms_ptirc / 2)
      diff += mcs->pxms_ptirc;

    if (diff < 0)
      diff = -diff;

    if (diff >= mcs->pxms_ptirc / 6)
    {
      pxmc_set_errno(mcs, PXMS_E_I2PT_TOOBIG);
    }
    else
    {
      mcs->pxms_ptofs = ofs;
      pxmc_set_flag(mcs, PXMS_PHA_b);
    }
  } /*else {

    ofs=irc-mcs->pxms_ptofs;
    if((unsigned short)ofs>=(unsigned short)mcs->pxms_ptirc) {
      if(ofs>0) {
        ofs-=mcs->pxms_ptirc;
      } else {
        ofs+=mcs->pxms_ptirc;
      }
    }
    mcs->pxms_ptmark=ofs;
  }*/

  /*lpc_qei_set_cmpos(qst, 0, qst->index_pos-4000);*/
}

int pxmc_lpc_pthalalign_callback(pxmc_state_t *mcs)
{
  short ptofs;
  short ptmark;
  lpc_qei_state_t *qst = &lpc_qei_state;
  int irc = lpc_qei_get_pos(qst);
  int idx_rel;

  if (!qst->index_occ)
    return 0;

  idx_rel = qst->index_pos - irc;
  idx_rel %= mcs->pxms_ptirc;

  if (idx_rel < 0)
    idx_rel += mcs->pxms_ptirc;

  ptofs = irc - mcs->pxms_ptofs;
  ptmark = ptofs + idx_rel;

  if ((unsigned short)ptmark >= mcs->pxms_ptirc)
  {
    if (ptmark > 0)
      ptmark -= mcs->pxms_ptirc;
    else
      ptmark += mcs->pxms_ptirc;
  }

  if ((unsigned short)ptmark < mcs->pxms_ptirc)
  {
    mcs->pxms_ptmark = ptmark;
  }

  return 0;
}

int pxmc_lpc_pthalalign(pxmc_state_t *mcs, int periods)
{
  int res;
  long r2acq;
  long spd;
  pxmc_call_t *fin_fnc = pxmc_lpc_pthalalign_callback;
  lpc_qei_state_t *qst = &lpc_qei_state;

  mcs->pxms_cfg &= ~PXMS_CFG_I2PT_m;
  lpc_qei_setup_index_catch(qst);

  r2acq = ((long)mcs->pxms_ptirc << PXMC_SUBDIV(mcs)) * periods;
  spd = 1 << PXMC_SUBDIV(mcs);

  res = pxmc_pthalalign(mcs, r2acq, spd, fin_fnc);

  return res;
}

int pxmc_lpc_pthalalign_run(void)
{
  return pxmc_lpc_pthalalign(pxmc_main_list.pxml_arr[0], 20);
}

static inline void pxmc_sfi_input(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENI_m - check if input (IRC) update is enabled */
    if (mcs->pxms_flg & PXMS_ENI_m)
    {
      pxmc_call(mcs, mcs->pxms_do_inp);
    }
  }
}

static inline void pxmc_sfi_controller_and_output(void)
{
  int var;
  pxmc_state_t *mcs;
  int uv_prot_err = pxmc_rocon_vin_uv_prot_cnt > 4;

  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENR_m - check if controller is enabled */
    if (mcs->pxms_flg & PXMS_ENR_m || mcs->pxms_flg & PXMS_ENO_m)
    {

      /* If output only is enabled, we skip the controller */
      if (mcs->pxms_flg & PXMS_ENR_m)
      {

        pxmc_call(mcs, mcs->pxms_do_con);

        /* PXMS_ERR_m - if axis in error state */
        if (mcs->pxms_flg & PXMS_ERR_m)
          mcs->pxms_ene = 0;
      }

      if (uv_prot_err) {
        /* emergency stop at undervoltage condition */
        pxmc_set_errno(mcs, PXMS_E_UV_PROT);
        mcs->pxms_ene = 0;
      }

      /* for bushless motors, it is necessary to call do_out
        even if the controller is not enabled and PWM should be provided. */
      pxmc_call(mcs, mcs->pxms_do_out);
    }
  }
}

static inline void pxmc_sfi_generator(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    /* PXMS_ENG_m - check if requested value (position) generator is enabled */
    if (mcs->pxms_flg & PXMS_ENG_m)
    {
      pxmc_call(mcs, mcs->pxms_do_gen);
    }
  }
}

static inline void pxmc_sfi_dbg(void)
{
  int var;
  pxmc_state_t *mcs;
  pxmc_for_each_mcs(var, mcs)
  {
    if (mcs->pxms_flg & PXMS_DBG_m)
    {
      pxmc_call(mcs, mcs->pxms_do_deb);
    }
  }
}

int pxmc_rocon_pthalalign_callback(pxmc_state_t *mcs)
{
  short ptofs;
  short ptmark;
  int inp_chan = mcs->pxms_inp_info;
  int idx_rel;
  long irc = fpga_irc[inp_chan]->count;

  if (!pxmc_inp_rocon_is_index_edge(mcs, 1))
    return 0;

  idx_rel = fpga_irc[inp_chan]->count_index - irc;
  idx_rel %= mcs->pxms_ptirc;
  if(idx_rel < 0)
    idx_rel += mcs->pxms_ptirc;

  ptofs = irc-mcs->pxms_ptofs;
  ptmark = ptofs+idx_rel;

  if((unsigned short)ptmark >= mcs->pxms_ptirc) {
    if(ptmark>0)
      ptmark -= mcs->pxms_ptirc;
    else
      ptmark += mcs->pxms_ptirc;
  }

  if((unsigned short)ptmark < mcs->pxms_ptirc) {
    mcs->pxms_ptmark = ptmark;
  }
  return 0;
}

int pxmc_rocon_pthalalign(pxmc_state_t *mcs, int periods)
{
  int res;
  long r2acq;
  long spd;
  pxmc_call_t *fin_fnc = pxmc_rocon_pthalalign_callback;

  mcs->pxms_cfg&=~PXMS_CFG_I2PT_m;

  r2acq=((long)mcs->pxms_ptirc<<PXMC_SUBDIV(mcs))*periods;
  spd=1<<PXMC_SUBDIV(mcs);

  /* clear bit indicating previous index */
  pxmc_inp_rocon_is_index_edge(mcs, 1);

  res=pxmc_pthalalign(mcs, r2acq, spd, fin_fnc);

  return res;
}

int pxmc_axis_out_chans4mode(int mode)
{
  switch (mode) {
    case PXMC_AXIS_MODE_DC:
    case PXMC_AXIS_MODE_DC_CURCTRL:
      return 2;
    case PXMC_AXIS_MODE_BLDC:
    case PXMC_AXIS_MODE_BLDC_PXMCC:
    case PXMC_AXIS_MODE_BLDC_DQ:
      return 3;
    case PXMC_AXIS_MODE_STEPPER_WITH_IRC:
    case PXMC_AXIS_MODE_STEPPER_WITH_IRC_PXMCC:
    case PXMC_AXIS_MODE_STEPPER_PXMCC:
      return 4;
  }
  return -1;
}

int pxmc_axis_rdmode(pxmc_state_t *mcs)
{
  if (mcs->pxms_do_out == pxmc_rocon_pwm2ph_out)
    return PXMC_AXIS_MODE_STEPPER_WITH_IRC;
  if (mcs->pxms_do_out == pxmc_rocon_pwm_dc_out)
    return PXMC_AXIS_MODE_DC;
  if (mcs->pxms_do_out == pxmc_rocon_pwm3ph_out)
    return PXMC_AXIS_MODE_BLDC;
  if (mcs->pxms_do_out == pxmc_pxmcc_pwm3ph_out)
    return PXMC_AXIS_MODE_BLDC_PXMCC;
  if (mcs->pxms_do_out == pxmc_pxmcc_pwm2ph_out)
    return PXMC_AXIS_MODE_STEPPER_WITH_IRC_PXMCC;
  if (mcs->pxms_do_out == pxmc_pxmcc_nofb2ph_out)
    return PXMC_AXIS_MODE_STEPPER_PXMCC;
 #ifdef PXMC_WITH_EXTENDED_STATE
  if (mcs->pxms_do_out == pxmc_pxmcc_pwm3ph_dq_out)
    return PXMC_AXIS_MODE_BLDC_DQ;
 #endif /*PXMC_WITH_EXTENDED_STATE*/
  if (mcs->pxms_do_out == pxmc_pxmcc_dc_curctrl_out)
    return PXMC_AXIS_MODE_DC_CURCTRL;
  return -1;
}

int
pxmc_axis_pt4mode(pxmc_state_t *mcs, int mode)
{
  static const typeof(*mcs->pxms_ptptr1) dummy0 = 0;
  int res = 0;

  if (mode == PXMC_AXIS_MODE_NOCHANGE)
    mode = pxmc_axis_rdmode(mcs);
  if (mode < 0)
    return -1;

  switch (mode) {
    /* case PXMC_AXIS_MODE_STEPPER: */
    case PXMC_AXIS_MODE_STEPPER_PXMCC:
    case PXMC_AXIS_MODE_STEPPER_WITH_IRC:
    case PXMC_AXIS_MODE_STEPPER_WITH_IRC_PXMCC:
      res = pxmc_ptable_set_profile(mcs, &pxmc_ptprofile_sin, 0, 0);
      break;
    /*case PXMC_AXIS_MODE_STEPPER_WITH_PWM:*/
    case PXMC_AXIS_MODE_DC:
    case PXMC_AXIS_MODE_DC_CURCTRL:
      /*profive some sane dummy values*/
      mcs->pxms_ptptr1 = (typeof(mcs->pxms_ptptr1))&dummy0;
      mcs->pxms_ptptr2 = (typeof(mcs->pxms_ptptr1))&dummy0;
      mcs->pxms_ptptr3 = (typeof(mcs->pxms_ptptr1))&dummy0;

      mcs->pxms_ptscale_mult=1;
      mcs->pxms_ptscale_shift=15;
      break;
    case PXMC_AXIS_MODE_BLDC:
    case PXMC_AXIS_MODE_BLDC_PXMCC:
    case PXMC_AXIS_MODE_BLDC_DQ:
      /* res = pxmc_init_ptable(mcs, PXMC_PTPROF_SIN3FUP); */
     #ifndef PXMC_WITH_PT_ZIC
      res = pxmc_ptable_set_profile(mcs, &pxmc_ptprofile_sin3phup, 0, 0);
     #else /*PXMC_WITH_PT_ZIC*/
      res = pxmc_ptable_set_profile(mcs, &pxmc_ptprofile_sin3phup_zic, 0, 0);
     #endif /*PXMC_WITH_PT_ZIC*/
      break;
    default:
      return -1;
  }

  mcs->pxms_ptvang = pxmc_ptvang_deg2irc(mcs, 90);

  return res;
}

/**
 * pxmc_axis_mode - Sets axis mode.[extern API]
 * @mcs:        Motion controller state information
 * @mode:       0 .. previous mode, 1 .. stepper motor mode,
 *              2 .. stepper motor with IRC feedback and PWM ,
 *              3 .. stepper motor with PWM control
 *              4 .. DC motor with IRC feedback and PWM
 *
 */
int
pxmc_axis_mode(pxmc_state_t *mcs, int mode)
{
  int res;
  int prev_mode;

  pxmc_axis_release(mcs);
  pxmc_clear_flag(mcs, PXMS_ENI_b);
  pxmc_clear_flags(mcs,PXMS_ENO_m);
  /* Clear possible stall index flags from hardware */
  pxmc_inp_rocon_is_index_edge(mcs, 1);
  pxmc_clear_flag(mcs, PXMS_PHA_b);
  pxmc_clear_flag(mcs, PXMS_PTI_b);


  prev_mode = pxmc_axis_rdmode(mcs);

  if (mode == PXMC_AXIS_MODE_NOCHANGE)
    mode = prev_mode;
  if (mode < 0)
    return -1;
  if (!mode)
    mode = PXMC_AXIS_MODE_DC;

  if ((prev_mode == PXMC_AXIS_MODE_BLDC_PXMCC) ||
      (prev_mode == PXMC_AXIS_MODE_STEPPER_WITH_IRC_PXMCC) ||
      (prev_mode == PXMC_AXIS_MODE_STEPPER_PXMCC) ||
      (prev_mode == PXMC_AXIS_MODE_BLDC_DQ))
    pxmcc_axis_setup(mcs, PXMCC_MODE_IDLE);

  res = pxmc_axis_pt4mode(mcs, mode);
  if (res < 0)
    return -1;

  if ((mcs->pxms_do_inp == pxmc_pxmcc_nofb2ph_inp)
     #ifdef PXMC_WITH_EXTENDED_STATE
      ||(mcs->pxms_do_inp == pxmc_pxmcc_cur_acquire_and_irc_inp)
     #endif /*PXMC_WITH_EXTENDED_STATE*/
     )
    mcs->pxms_do_inp = pxmc_inp_rocon_inp;
  if (mcs->pxms_do_con == pxmc_pxmcc_nofb_con)
    mcs->pxms_do_con = pxmc_pid_con;

  switch (mode) {
    /*case PXMC_AXIS_MODE_STEPPER:*/
    case PXMC_AXIS_MODE_STEPPER_WITH_IRC:
      mcs->pxms_do_out = pxmc_rocon_pwm2ph_out;
      break;
    /*case PXMC_AXIS_MODE_STEPPER_WITH_PWM:*/
    case PXMC_AXIS_MODE_DC:
      mcs->pxms_do_out = pxmc_rocon_pwm_dc_out;
      break;
    case PXMC_AXIS_MODE_BLDC:
      mcs->pxms_do_out = pxmc_rocon_pwm3ph_out;
      break;
    case PXMC_AXIS_MODE_BLDC_PXMCC:
   #ifdef PXMC_WITH_EXTENDED_STATE
    case PXMC_AXIS_MODE_BLDC_DQ:
   #endif /*PXMC_WITH_EXTENDED_STATE*/
      if (pxmcc_axis_setup(mcs, PXMCC_MODE_BLDC) < 0)
        return -1;
      pxmcc_axis_enable(mcs, 1);
     #ifdef PXMC_WITH_EXTENDED_STATE
      if (mode == PXMC_AXIS_MODE_BLDC_DQ) {
        mcs->pxms_do_out = pxmc_pxmcc_pwm3ph_dq_out;
        mcs->pxms_do_inp = pxmc_pxmcc_cur_acquire_and_irc_inp;
      } else
     #endif /*PXMC_WITH_EXTENDED_STATE*/
      {
        mcs->pxms_do_out = pxmc_pxmcc_pwm3ph_out;
      }
      break;
    case PXMC_AXIS_MODE_STEPPER_WITH_IRC_PXMCC:
      if (pxmcc_axis_setup(mcs, PXMCC_MODE_STEPPER_WITH_IRC) < 0)
        return -1;
      pxmcc_axis_enable(mcs, 1);
      mcs->pxms_do_out = pxmc_pxmcc_pwm2ph_out;
      break;
    case PXMC_AXIS_MODE_STEPPER_PXMCC:
      if (pxmcc_axis_setup(mcs, PXMCC_MODE_STEPPER) < 0)
        return -1;
      pxmcc_axis_enable(mcs, 1);
      mcs->pxms_do_inp = pxmc_pxmcc_nofb2ph_inp;
      mcs->pxms_do_con = pxmc_pxmcc_nofb_con;
      mcs->pxms_do_out = pxmc_pxmcc_nofb2ph_out;
      break;
   case PXMC_AXIS_MODE_DC_CURCTRL:
      if (pxmcc_axis_setup(mcs, PXMCC_MODE_DC_CUR) < 0)
        return -1;
      pxmcc_axis_enable(mcs, 1);
      mcs->pxms_do_con = pxmc_pxmcc_nofb_con;
      mcs->pxms_do_out = pxmc_pxmcc_dc_curctrl_out;
      break;
   default:
      return -1;
  }

  /* Clear possible stall index flags from hardware */
  pxmc_inp_rocon_is_index_edge(mcs, 1);
  /* Request new phases alignment for changed parameters */
  pxmc_clear_flag(mcs, PXMS_PHA_b);
  pxmc_clear_flag(mcs, PXMS_PTI_b);
  pxmc_set_flag(mcs, PXMS_ENI_b);
  return res;
}

void pxmc_sfi_isr(void)
{
  unsigned long spent = pxmc_fast_tick_time();

  pxmc_sfi_input();
  pxmc_sfi_controller_and_output();
  pxmc_sfi_generator();
  pxmc_sfi_dbg();
  /* Kick LX Master watchdog */
  if (pxmc_main_list.pxml_cnt != 0)
    *fpga_lx_master_transmitter_wdog = 1;

  spent = pxmc_fast_tick_time() - spent;

  if(spent > pxmc_sfi_spent_time_max)
    pxmc_sfi_spent_time_max = spent;

}

pxmc_call_t *const pxmc_reg_type_table[] = {
  pxmc_pid_con,
  pxmc_pid_con,
  pxmc_pidnl_con
};


int pxmc_get_reg_type(pxmc_state_t *mcs)
{
  int reg_type;
  int max_type = sizeof(pxmc_reg_type_table) / sizeof(*pxmc_reg_type_table);

  for (reg_type = 1; reg_type < max_type; reg_type++)
    if (mcs->pxms_do_con == pxmc_reg_type_table[reg_type])
      return reg_type;
  return 0;
}

int pxmc_set_reg_type(pxmc_state_t *mcs, int reg_type)
{
  int max_type = sizeof(pxmc_reg_type_table) / sizeof(*pxmc_reg_type_table);

  if ((reg_type < 0) || (reg_type >= max_type))
    return -1;
  if (mcs->pxms_flg & PXMS_ENR_m)
    return -1;

  mcs->pxms_do_con = pxmc_reg_type_table[reg_type];
  return 0;
}

int pxmc_clear_power_stop(void)
{
  return 0;
}

int pxmc_regpwron_set(int enable)
{
  if ((enable < 0) || (enable > 2))
    return -1;
  pxmc_rocon_regpwron_state = enable != 0;
 #ifdef APPL_WITH_PSINDCTL
 #ifdef APPL_WITH_PSINDCTL_PAST
  if (!pxmc_rocon_regpwron_state) {
    psindctl_past_pwr_stop();
  } else if (enable == 2) {
    psindctl_past_pwr_enable();
    psindctl_initiate_pwr_on_pulse();
  }
 #endif /*APPL_WITH_PSINDCTL_PAST*/
 #endif /*APPL_WITH_PSINDCTL*/
  return 0;
}

int pxmc_regpwron_get(void)
{
  return pxmc_rocon_regpwron_state;
}

int pxmc_process_state_check(unsigned long *pbusy_bits,
                             unsigned long *perror_bits)
{
  unsigned short flg;
  unsigned short chan;
  unsigned long busy_bits = 0;
  unsigned long error_bits = 0;
  pxmc_state_t *mcs;
  flg=0;
  pxmc_for_each_mcs(chan, mcs) {
    if(mcs) {
      flg |= mcs->pxms_flg;
      if (mcs->pxms_flg & PXMS_BSY_m)
        busy_bits |= 1 << chan;
      if (mcs->pxms_flg & PXMS_ERR_m)
        error_bits |= 1 << chan;
    }
  }
  if (appl_errstop_mode) {
    if((flg & PXMS_ENG_m) && (flg & PXMS_ERR_m)) {
      pxmc_for_each_mcs(chan, mcs) {
        if(mcs&&(mcs->pxms_flg & PXMS_ENG_m)) {
          pxmc_stop(mcs, 0);
        }
      }
    }
  }

  if (pbusy_bits != NULL)
    *pbusy_bits = busy_bits;
  if (perror_bits != NULL)
    *perror_bits = error_bits;

  return flg;
}

int pxmc_done(void)
{
  int var;
  pxmc_state_t *mcs;

  if (!pxmc_main_list.pxml_cnt)
    return 0;

  pxmc_for_each_mcs(var, mcs)
  {
    pxmc_axis_release(mcs);
  }

  pxmc_main_list.pxml_cnt = 0;
  __memory_barrier();

  return 0;
}

int pxmc_initialize(void)
{
  int res;
  int i;

  pxmc_main_list.pxml_cnt = 0;
  pxmc_dbg_hist = NULL;
 #ifdef PXMC_ROCON_TIMED_BY_RX_DONE
  disable_irq(ROCON_RX_IRQn);
 #endif /*PXMC_ROCON_TIMED_BY_RX_DONE*/
  __memory_barrier();

  pxmc_state_t *mcs = &mcs0.base;
  lpc_qei_state_t *qst = &lpc_qei_state;

  *fpga_irc_reset = 1;

  for (i = 0; i < 8; i++) {
    fpga_irc[i]->count = 0;
    fpga_irc[i]->count_index = 0;
    *fpga_irc_state[i] = FPGA_IRC_STATE_INDEX_EVENT_MASK;
  }

  /* Initialize QEI module for IRC counting */
  pxmc_inp_lpc_qei_init(mcs->pxms_inp_info);

  /* Initialize QEI events processing */
  if (lpc_qei_setup_irq(qst) < 0)
    return -1;

  *fpga_irc_reset = 0;

  //res = pxmc_axis_pt4mode(mcs, PXMC_AXIS_MODE_BLDC);

  /*pxmc_ctm4pwm3f_wr(mcs, 0, 0, 0);*/
  //pxmc_rocon_pwm3ph_wr(mcs, 0, 0, 0);

  res = pxmc_rocon_pwm_master_init();
  if (res < 0)
    return -1;

 #ifdef PXMC_ROCON_TIMED_BY_RX_DONE
  pxmc_rocon_rx_done_isr_setup(pxmc_rocon_rx_done_isr);
 #endif /*PXMC_ROCON_TIMED_BY_RX_DONE*/

  __memory_barrier();

 #ifdef APPL_PXMC_FILL_BY_DEFAULT
  for (i = 0; i < PXML_MAIN_CNT; i ++) {
    pxmc_rocon_state_t *mcsrc;
    pxmc_state_t *mcs = pxmc_main_list.pxml_arr[i];
    if (mcs == NULL)
      continue;
    mcsrc = pxmc_state2rocon_state(mcs);
    *mcsrc = pxmc_mcs_default_parameters;
    mcs->pxms_inp_info = i;
    mcs->pxms_out_info = i * APPL_PXMC_FILL_OUT_INFO_SPAN;
    if ((mcs->pxms_out_info >= 16) && APPL_PXMC_FILL_OUT_INFO_ROW_GAP)
      mcs->pxms_out_info -= 16 - APPL_PXMC_FILL_OUT_INFO_ROW_GAP;
  }
 #endif /*APPL_PXMC_FILL_BY_DEFAULT*/

  __memory_barrier();
  pxmc_main_list.pxml_cnt = PXML_MAIN_CNT;

  return 0;
}
