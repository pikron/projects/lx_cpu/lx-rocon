/*******************************************************************
  Motion and Robotic System (MARS) aplication components

  appl_cmds.c - application specific commands - mainly global
                PXMC state manipulation for RoCoN

  Copyright (C) 2001-2014 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2014 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <stdlib.h>
#include <string.h>
#include <pxmc.h>
#include <pxmc_coordmv.h>
#include <cmd_proc.h>
#include <pxmc_cmds.h>
#include <utils.h>
#include <hal_machperiph.h>

#include "appl_defs.h"
#include "appl_version.h"

#ifdef APPL_WITH_USB
int usb_app_stop(void);
#endif

#ifndef __STRINGIFY
#define __STRINGIFY(x)     #x              /* stringify without expanding x */
#endif
#ifndef STRINGIFY
#define STRINGIFY(x)    __STRINGIFY(x)        /* expand x, then stringify */
#endif

int sqrtll_main(int argc, char *argv[]);
int cmd_do_stop_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_release_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_status_bsybits(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_axst_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_cer_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_clr_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_reboot(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
int cmd_do_rungzipapp(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);

unsigned short statchk_power_stop = 0;
unsigned short statchk_power_off = 0;
int appl_errstop_mode = 0;
int appl_idlerel_time = 0;

int cmd_do_status_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  int val;
  int opchar;
  pxmc_state_t *mcs;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar=='?'){
    val=0;
    pxmc_for_each_mcs(i, mcs) {
      if(mcs)
        val|=mcs->pxms_flg;
    }
    if(pxmc_coordmv_checkst(&pxmc_coordmv_state)>=2)
      val|=PXMS_CQF_m;	/* coordinator coomand queue full */
    /*if(trap_pc_addr)
      val|=0x1000000;*/
    if(statchk_power_stop)
      val|=0x20000;
    if(statchk_power_off)
      val|=0x10000;
    return cmd_opchar_replong(cmd_io, param, val, 0, 0);
  }
  return 0; 
}

int cmd_do_pthalign(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
    pxmc_state_t *mcs;
    int res;

    if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;

    if(*param[2]=='?') {
      return cmd_opchar_replong(cmd_io, param, (long)mcs->pxms_ptmark, 0, 0);
    }

    if(*param[2]!=':') return -CMDERR_OPCHAR;

    res = pxmc_rocon_pthalalign(mcs, 20);

    if(res < 0)
       return -CMDERR_BADDIO;

    return 0;
}

int cmd_do_reg_type(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  pxmc_state_t *mcs;

  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;

  if(*param[2]=='?') {
    return cmd_opchar_replong(cmd_io, param, pxmc_get_reg_type(mcs), 0, 0);
  }

  if(*param[2]!=':') return -CMDERR_OPCHAR;

  if(mcs->pxms_flg&PXMS_BSY_m) return -CMDERR_BSYREG;

  val=atol(param[3]);
  val=pxmc_set_reg_type(mcs, val);

  if(val<0)
    return val;

  return 0;
}

/**
 * cmd_do_axis_mode - checks the command format and busy flag validity, calls pxmc_axis_mode
 *
 * if pxmc_axis_mode returns -1, cmd_do_axis_mode returns -1.
 */
int cmd_do_axis_mode(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  pxmc_state_t *mcs;

  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;

  if(*param[2]=='?') {
    return cmd_opchar_replong(cmd_io, param, pxmc_axis_rdmode(mcs), 0, 0);
  }

  if(*param[2]!=':') return -CMDERR_OPCHAR;

  if(mcs->pxms_flg&PXMS_BSY_m) return -CMDERR_BSYREG;

  val=atol(param[3]);
  val=pxmc_axis_mode(mcs,val);
  if(val<0)
    return val;

  return 0;
}

int cmd_do_axes_outmap(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  char *ps;
  ps = param[3];
  int mcs_idx;
  int out_chan = 0;
  pxmc_state_t *mcs;
  int exes_cnt = pxmc_main_list.pxml_cnt > 0? pxmc_main_list.pxml_cnt : 0;
  pxmc_info_t outmap[exes_cnt];

  if (exes_cnt == 0)
    return 0;

  if(*param[2]=='?') {
    char str[20];
    cmd_io_write(cmd_io,param[0],param[2]-param[0]);

    pxmc_for_each_mcs(mcs_idx, mcs)
    {
      cmd_io_putc(cmd_io, mcs_idx? ',' : '=');
      if (mcs != NULL) {
        val = mcs->pxms_out_info;
        i2str(str, val, 0, 0);
        cmd_io_write(cmd_io,str,strlen(str));
      } else {
        cmd_io_putc(cmd_io, 'X');
      }
    }

    return 0;
  }

  if(*param[2]!=':') return -CMDERR_OPCHAR;

  mcs_idx = -1;
  si_skspace(&ps);
  do {
    if (!*ps)
      break;
    if (mcs_idx >= exes_cnt - 1)
      return -CMDERR_BADREG;

    if (si_long(&ps, &val, 0) < 0)
      return -CMDERR_BADPAR;

    if (val < 0)
      return -CMDERR_BADPAR;

    outmap[++mcs_idx] = val;

    si_skspace(&ps);

    if (*ps)
      if(*(ps++) != ',')
        return -CMDERR_BADSEP;
  } while(1);

  while (mcs_idx < exes_cnt - 1) {
    if (mcs_idx >= 0) {
      mcs = pxmc_main_list.pxml_arr[mcs_idx];
      if (mcs != NULL) {
        val = pxmc_axis_rdmode(mcs);
        val = pxmc_axis_out_chans4mode(val);
        if (val > 0)
          out_chan = outmap[mcs_idx] + val;
      }
    }
    outmap[++mcs_idx] = out_chan;
  }

  pxmc_for_each_mcs(mcs_idx, mcs)
  {
    if (mcs == NULL)
      continue;
    if(mcs->pxms_flg & PXMS_BSY_m)
      return -CMDERR_BSYREG;
    pxmc_axis_release(mcs);
    val = pxmc_axis_mode(mcs, PXMC_AXIS_MODE_NOCHANGE);
    if (val < 0)
      return -CMDERR_EIO;
  }

  pxmc_for_each_mcs(mcs_idx, mcs)
  {
    if (mcs == NULL)
      continue;
    mcs->pxms_out_info = outmap[mcs_idx];
    val = pxmc_axis_mode(mcs, PXMC_AXIS_MODE_NOCHANGE);
    if (val < 0)
      return -CMDERR_EIO;
  }

  return 0;
}

int cmd_do_regpwron(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  int opchar;
  char *ps;
  ps = param[3];

  if ((opchar = cmd_opchar_check(cmd_io,des,param)) < 0)
    return opchar;
  if (opchar == ':') {
    if (si_long(&ps, &val, 0) < 0)
      return -CMDERR_BADPAR;
    si_skspace(&ps);
    if (*ps)
      return -CMDERR_GARBAG;
    if (pxmc_regpwron_set(val) < 0)
      return -CMDERR_EIO;
  } else {
    val = pxmc_regpwron_get();
    return cmd_opchar_replong(cmd_io, param, val, 0, 0);
  }
  return 0;
}

int cmd_do_ioecho(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io, des, param))<0) return opchar;
  if(opchar == ':') {
    val = *param[3];
    if (val == '0')
      cmd_io->priv.ed_line.in->flg &= ~FL_ELB_ECHO;
    else if(val == '1')
      cmd_io->priv.ed_line.in->flg |= FL_ELB_ECHO;
    else return -CMDERR_BADPAR;
  }else{
    cmd_io_write(cmd_io, param[0], param[2]-param[0]);
    cmd_io_putc(cmd_io,'=');
    cmd_io_putc(cmd_io,cmd_io->priv.ed_line.in->flg & FL_ELB_ECHO?'1':'0');
  }
  return 0;
}

const char software_ver[]=APP_VER_ID" v"STRINGIFY(APP_VER_MAJOR)"."STRINGIFY(APP_VER_MINOR)
           " pl"STRINGIFY(APP_VER_PATCH)" build Pi "__DATE__" "__TIME__;

int cmd_do_ver(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  if(*param[2]!='?') return -CMDERR_OPCHAR;
  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  cmd_io_write(cmd_io,software_ver,strlen(software_ver));
  return 0;
}

int cmd_do_reboot(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  if(*param[2]!=':') return -CMDERR_OPCHAR;

  cmd_do_release_all(cmd_io, des, param);
  pxmc_regpwron_set(0);

  /* after 10ms invoke hardware reset by watchdog */
  lpc_watchdog_init(1, 10);
  watchdog_feed();

 #ifdef APPL_WITH_USB
  usb_app_stop();
 #endif

  return 0;
}


int cmd_do_spmax(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  if(*param[2]!='?') return -CMDERR_OPCHAR;
  cmd_opchar_replong(cmd_io, param, pxmc_sfi_spent_time_max, 0, 0);
  pxmc_sfi_spent_time_max = 0;
  return 0;
}

int cmd_do_sqrtll(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *ps;
  ps = param[1];
  sqrtll_main(!ps?1: !*ps? 1: 2, param);
  return 0;
}

cmd_des_t const cmd_des_cer_all={0, CDESM_OPCHR,"PURGE","clear error flags and release failded axes",
			cmd_do_cer_all,{}};
cmd_des_t const cmd_des_clr_all={0, CDESM_OPCHR,
			"CLEAR",0,cmd_do_clr_all,{}};
cmd_des_t const cmd_des_stop_all={0, CDESM_OPCHR,
			"STOP","stop all motions",cmd_do_stop_all,{}};
cmd_des_t const cmd_des_release_all={0, CDESM_OPCHR,
			"RELEASE","releases all axes closed loop control",cmd_do_release_all,{}};
cmd_des_t const cmd_des_status_all={0, CDESM_OPCHR|CDESM_RD,
			"ST","system status bits encoded in number",
			cmd_do_status_all,{}};
cmd_des_t const cmd_des_status_bsybits={0, CDESM_OPCHR|CDESM_RD,
			"STBSYBITS","busy bits of all axes in one number",
			cmd_do_status_bsybits,{}};
cmd_des_t const cmd_des_stamp={0, CDESM_OPCHR,
			"STAMP","host communication stamp",
			cmd_do_stamp,{}};
cmd_des_t const cmd_des_pthalign={0, CDESM_OPCHR|CDESM_RW,"PTHALIGN?","run alignement of phase according to HAL",
			cmd_do_pthalign,
			{0,0}};
cmd_des_t const cmd_des_reg_type={0, CDESM_OPCHR|CDESM_RW,
			"REGTYPE?","select controller structure and type",cmd_do_reg_type,
			{}};
cmd_des_t const cmd_des_axis_mode={0, CDESM_OPCHR|CDESM_WR,
			"REGMODE?","axis working mode",cmd_do_axis_mode,
			{}};
cmd_des_t const cmd_des_axes_outmap={0, CDESM_OPCHR|CDESM_WR,
			"REGOUTMAP","map outputs to axes",cmd_do_axes_outmap,
			{}};
cmd_des_t const cmd_des_errstop={0, CDESM_OPCHR|CDESM_RW,
			"ERRSTOP","stop all axes at error",cmd_do_rw_int,
			{(char*)&appl_errstop_mode,
			 0}};
cmd_des_t const cmd_des_idlerel={0, CDESM_OPCHR|CDESM_RW,
			"IDLEREL","release controllers after ? seconds",cmd_do_rw_int,
			{(char*)&appl_idlerel_time,
			 0}};
cmd_des_t const cmd_des_regpwron={0, CDESM_OPCHR|CDESM_RW,
			"REGPWRON","power control flags",cmd_do_regpwron,
			{}};
cmd_des_t const cmd_des_echo={0, CDESM_OPCHR|CDESM_RW,
			"ECHO","enable echoing of received character",cmd_do_ioecho,{}};
cmd_des_t const cmd_des_ver={0, CDESM_OPCHR|CDESM_RD,
			"VER","software version",cmd_do_ver,{}};
cmd_des_t const cmd_des_reboot={0, CDESM_OPCHR|CDESM_WR,
			"REBOOT","reset and reboot",cmd_do_reboot,{}};
cmd_des_t const cmd_des_spmax={0, CDESM_OPCHR|CDESM_RD,
			"SPMAX","read and reset maximal time spent in isr",
			cmd_do_spmax,{}};
cmd_des_t const cmd_des_sqrtll={0, 0,"sqrtll","test 64-bit square root computation",
			cmd_do_sqrtll,
			{0,0}};
cmd_des_t const cmd_des_rungzipapp={0, 0,"rungzipapp","load gzipped application and run it",
			cmd_do_rungzipapp,{}};

cmd_des_t const *const cmd_appl_specific[]={
  &cmd_des_cer_all,
  &cmd_des_clr_all,
  &cmd_des_stop_all,
  &cmd_des_release_all,
  &cmd_des_status_all,
  &cmd_des_status_bsybits,
  &cmd_des_stamp,
  &cmd_des_pthalign,
  &cmd_des_reg_type,
  &cmd_des_axis_mode,
  &cmd_des_axes_outmap,
  &cmd_des_errstop,
  &cmd_des_idlerel,
  &cmd_des_regpwron,
  &cmd_des_echo,
  &cmd_des_ver,
  &cmd_des_reboot,
  &cmd_des_spmax,
  &cmd_des_sqrtll,
  &cmd_des_rungzipapp,
  NULL
};
