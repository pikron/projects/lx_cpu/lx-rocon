#include <cpu_def.h>
#include <LPC17xx.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>
#include <cmd_proc.h>
#include <move_and_exec.h>
#include <load_zlib_binary.h>
#include <lt_timer.h>
#include <utils.h>
#include <pxmc.h>

#include "appl_defs.h"

static void keep_system_alive(void)
{
#ifdef APPL_WITH_LWIP
  lwip_app_poll();
#endif
#ifdef APPL_WITH_USB
  if (usb_enable_flag) {
    usb_app_poll();
  }
#endif /*APPL_WITH_USB*/
}

int cmd_do_rungzipapp_progress_fnc(void *context, size_t bytes, char *msg)
{

  if (msg != NULL)
    printf("zlib message \"%s\" at pos %ld\n\r", msg, (long)bytes);

  keep_system_alive();
  return 0;
}

int cmd_do_rungzipapp_read_char(void *read_char_arg)
{
  int ch;
  cmd_io_t* cmd_io = (cmd_io_t*)read_char_arg;

  ch = cmd_io_getc(cmd_io);
  if (0) {
    if (ch >= 0) {
      printf("%02x\n", ch);
    } else {
      printf("%i\n", ch);
    }
  }
  if (ch < 0)
    keep_system_alive();

  return ch;
}

int cmd_do_rungzipapp(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res;
  char *ps = param[1];
  long max_size = 1024 * 1024;
  long start_addr = 0xa0000000;
  long entry_addr = 0;
  void *app_dst;
  void *app_src;
  size_t app_size;
  void *app_entry;
  void *app_stack_top;
  void *buff;
  cmd_io_t* io_raw = cmd_io;
  void (*move_and_exec_fnc)(void *dst, void *src, size_t size, void *entry, void *stack_top);
  lt_mstime_t ltime;

  {
    int mcs_idx;
    pxmc_state_t *mcs;

    pxmc_for_each_mcs(mcs_idx, mcs)
    {
      if (mcs == NULL)
        continue;
      if(mcs->pxms_flg & (PXMS_BSY_m | PXMS_ENR_m))
        return -CMDERR_BSYREG;
    }
  }

  do {
    si_skspace(&ps);
    if (!*ps)
      break;
    if (si_ulong(&ps, &max_size, 0) < 0)
      return -CMDERR_BADPAR;

    si_skspace(&ps);
    if (!*ps)
      break;
    if (si_ulong(&ps, &start_addr, 0) < 0)
      return -CMDERR_BADPAR;

    si_skspace(&ps);
    if (!*ps)
      break;
    if (si_ulong(&ps, &entry_addr, 0) < 0)
      return -CMDERR_BADPAR;
  } while (0);

  move_and_exec_fnc = move_and_exec;

  buff = malloc(max_size + (&move_and_exec_end - &move_and_exec_start) + 0x20);
  if (buff < 0) {
    printf("No enoug memory to allocate %"PRIi32" bytes\n", max_size);
    return -CMDERR_NOMEM;
  }

  if(cmd_io->priv.ed_line.io_stack != NULL)
    io_raw=cmd_io->priv.ed_line.io_stack;

  res = load_zlib_binary(&app_size, cmd_do_rungzipapp_read_char,
           io_raw, buff, max_size, 0,
           cmd_do_rungzipapp_progress_fnc, NULL);

  if (res != 0) {
    free(buff);
    printf("Load of copressed image failed, code %d\n", res);
    return -1;
  }

  app_src = buff;
  app_dst = (void*)start_addr;
  if (entry_addr) {
    app_entry = (void*)entry_addr;
    app_stack_top = (void*)__get_MSP();
  } else {
    uint32_t sp_vect;
    uint32_t res_vect;
    memcpy(&sp_vect, app_src, sizeof(res_vect));
    app_stack_top = (void*)sp_vect;
    memcpy(&res_vect, app_src + 4, sizeof(res_vect));
    app_entry = (void*)res_vect;
  }

  if (((void*)&move_and_exec_start < (void*)app_dst + app_size) &&
      ((void*)&move_and_exec_end > (void*)app_dst)) {
     void *move_and_exec_from = &move_and_exec_start;
     void *move_and_exec_to = (void*)(((uintptr_t)buff + app_size + 0x1f) & ~0xf);

     move_and_exec_fnc = (void*)move_and_exec_fnc + (move_and_exec_to - move_and_exec_from);
     printf("Relocating move and exec code to %p from %p fnc %p .. %p\n",
            move_and_exec_to, move_and_exec_from, move_and_exec_fnc, move_and_exec);
     memcpy(move_and_exec_to, move_and_exec_from,
            &move_and_exec_end - &move_and_exec_start);
  }

  printf("Ready to proceed jump to application\n");
  printf("app_dst=%p, app_src=%p, app_size=%ld, app_entry=%p\n",
         app_dst, app_src, (long)app_size, app_entry);

  {
    int i;
    uint8_t *p;
    p = app_src;
    for (i = 0; i < 16; i++)
      printf("%02x%c", *(p++), (i & 0xf) == 0xf? '\n': ' ');
    p = app_src + app_size - 16;
    for (i = 0; i < 16; i++)
      printf("%02x%c", *(p++), (i & 0xf) == 0xf? '\n': ' ');
  }

  lt_mstime_update();
  ltime = actual_msec;

  while ((lt_msdiff_t)(actual_msec-ltime) < 1000) {
    keep_system_alive();
    lt_mstime_update();
  }

#ifdef APPL_WITH_USB
  if (usb_enable_flag) {
    usb_app_stop();
  }
#endif /*APPL_WITH_USB*/

  lt_mstime_update();
  ltime = actual_msec;

  while ((lt_msdiff_t)(actual_msec-ltime) < 100) {
    keep_system_alive();
    lt_mstime_update();
  }

  /* cli(); */

  /* disable interrupts at NVIC */
  NVIC->ICER[0] = 0xffffffff;
  NVIC->ICER[1] = 0x000001ff;

  move_and_exec_fnc(app_dst, app_src, app_size, app_entry, app_stack_top);

  return 0;
}
