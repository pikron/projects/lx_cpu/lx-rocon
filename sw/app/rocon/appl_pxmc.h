/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmcc_rocon.h - multi axis motion controller coprocessor
               RoCoN specific extensions

  (C) 2001-2014 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2014 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _APPL_PXMC_H_
#define _APPL_PXMC_H_

#include <stdint.h>
#include <pxmc.h>

typedef struct pxmc_rocon_state_t {
  pxmc_state_t base;
  uint32_t steps_pos_prev;
  uint32_t cur_d_cum_prev;
  uint32_t cur_q_cum_prev;
  int32_t  cur_d_err_sum;
  int32_t  cur_q_err_sum;
  short    cur_d_p;
  short    cur_d_i;
  short    cur_q_p;
  short    cur_q_i;
  short    cur_hold;
} pxmc_rocon_state_t;

#define pxmc_rocon_state_offs(_fld) \
                (((size_t)&((pxmc_rocon_state_t *)0L)->_fld) - \
                 ((size_t)&((pxmc_rocon_state_t *)0L)->base))

extern int pxmc_rocon_vin_act;
extern unsigned pxmc_rocon_lxpwr_chips;

long pxmc_rocon_read_irc(int chan);
long pxmc_rocon_read_irc_index(int chan, unsigned int *idx_couter_p,
                               int clear_event);


int pxmc_rocon_pwm_direct_wr(unsigned chan, unsigned pwm, int en);

int pxmc_rocon_wait_rx_done(void);

int pxmc_get_reg_type(pxmc_state_t *mcs);

int pxmc_set_reg_type(pxmc_state_t *mcs, int reg_type);

int pxmc_regpwron_set(int enable);
int pxmc_regpwron_get(void);

int pxmc_inp_rocon_is_mark_direct(int chan);

#endif /*_APPL_PXMC_H_*/
