#include <types.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <stdio.h>
#include <endian.h>
#include <hal_gpio.h>

#include "appl_defs.h"
#include "appl_version.h"
#include "appl_fpga.h"

#define SWAB32(x) ((x >> 24) | ((x & 0x00FF0000) >> 8) | ((x & 0x0000FF00) << 8) | (x << 24))

/* Registers in FPGA */
volatile uint32_t *fpga_tumbl_control = (volatile uint32_t *)FPGA_TUMBL_CONTROL_REG;
volatile uint32_t *fpga_tumbl_trace_kick = (volatile uint32_t *)FPGA_TUMBL_TRACE_KICK_REG;
volatile uint32_t *fpga_tumbl_pc = (volatile uint32_t *)FPGA_TUMBL_PC;
volatile uint32_t *fpga_tumbl_base = (volatile uint32_t *)FPGA_TUMBL_IMEM_BASE;
volatile uint32_t *fpga_tumbl_imem = (volatile uint32_t *)FPGA_TUMBL_IMEM_BASE;
volatile uint32_t *fpga_tumbl_dmem = (volatile uint32_t *)FPGA_TUMBL_DMEM_BASE;

volatile struct irc_register *fpga_irc[] = 
{
  (volatile struct irc_register *)FPGA_IRC0_BASE,
  (volatile struct irc_register *)FPGA_IRC1_BASE,
  (volatile struct irc_register *)FPGA_IRC2_BASE,
  (volatile struct irc_register *)FPGA_IRC3_BASE,
  (volatile struct irc_register *)FPGA_IRC4_BASE,
  (volatile struct irc_register *)FPGA_IRC5_BASE,
  (volatile struct irc_register *)FPGA_IRC6_BASE,
  (volatile struct irc_register *)FPGA_IRC7_BASE
};

volatile uint8_t *fpga_irc_state[] = 
{
  (volatile uint8_t *)(FPGA_IRC0_BASE + 0x40),
  (volatile uint8_t *)(FPGA_IRC1_BASE + 0x40),
  (volatile uint8_t *)(FPGA_IRC2_BASE + 0x40),
  (volatile uint8_t *)(FPGA_IRC3_BASE + 0x40),
  (volatile uint8_t *)(FPGA_IRC4_BASE + 0x40),
  (volatile uint8_t *)(FPGA_IRC5_BASE + 0x40),
  (volatile uint8_t *)(FPGA_IRC6_BASE + 0x40),
  (volatile uint8_t *)(FPGA_IRC7_BASE + 0x40)
};

volatile uint8_t *fpga_irc_reset  = (volatile uint8_t *)(FPGA_IRC_RESET);

/* Variables for configuration */
volatile uint16_t *fpga_configure_line = (volatile uint16_t *)0x80007FF0;
int fpga_configured = 0;
int fpga_reconfiguration_locked = 1;

/* BUS measurement - registers to measure the delay necessary for reading and writing */
volatile uint32_t *fpga_bus_meas_read1  = (volatile uint32_t *)0x80007FF0;
volatile uint32_t *fpga_bus_meas_write1 = (volatile uint32_t *)0x80007FF4;

volatile uint32_t *fpga_bus_meas_read2  = (volatile uint32_t *)0x80007FF8;
volatile uint32_t *fpga_bus_meas_write2 = (volatile uint32_t *)0x80007FFC;

/* LX Master */
volatile uint32_t *fpga_lx_master_transmitter_base = (volatile uint32_t *)FPGA_LX_MASTER_TRANSMITTER_BASE;
volatile uint32_t *fpga_lx_master_transmitter_reg = (volatile uint32_t *)FPGA_LX_MASTER_TRANSMITTER_REG;
volatile uint32_t *fpga_lx_master_transmitter_wdog = (volatile uint32_t *)FPGA_LX_MASTER_TRANSMITTER_WDOG;
volatile uint32_t *fpga_lx_master_transmitter_cycle = (volatile uint32_t *)FPGA_LX_MASTER_TRANSMITTER_CYCLE;
volatile uint32_t *fpga_lx_master_receiver_base = (volatile uint32_t *)FPGA_LX_MASTER_RECEIVER_BASE;
volatile uint32_t *fpga_lx_master_receiver_reg = (volatile uint32_t *)FPGA_LX_MASTER_RECEIVER_REG;
volatile uint32_t *fpga_lx_master_receiver_done_div = (volatile uint32_t *)FPGA_LX_MASTER_RECEIVER_DONE_DIV;
volatile uint32_t *fpga_lx_master_reset = (volatile uint32_t *)FPGA_LX_MASTER_RESET;
volatile uint32_t *fpga_lx_master_conf  = (volatile uint32_t *)FPGA_CONFIGURATION_FILE_ADDRESS;

#define FPGA_FNCAPPROX_BASE               0x80023000

volatile uint32_t *fpga_fncapprox_base  = (volatile uint32_t *)FPGA_FNCAPPROX_BASE;

/* BUS measurement - values (shifting all bits) */
#define MEAS_VAL1 0xAAAAAAAA
#define MEAS_VAL2 0x55555555

void fpga_init()
{
  /* Initialze EMC for FPGA */

  /* Settings:
   * 32 bus width
   * CS polarity: LOW (ATTENTION: Must match FPGA setup)
   * Byte line state: Reads are only 32 bits
   * Extended wait: off
   * Buffer: disabled
   * Write protection: disabled
   */
  LPC_EMC->StaticConfig0 = 0x00000002;

  /* Delays - not measured at this point
   * We're running on 72 MHz, FPGA bus is running on 50 MHz async.
   * Read: 32 cycles
   * Write: 33 cycles
   * Turnaround: 2 cycles (cca. 28 ns)
   */
  LPC_EMC->StaticWaitRd0 = 0x1F;
  LPC_EMC->StaticWaitWr0 = 0x1F;
  LPC_EMC->StaticWaitTurn0 = 0x01;

  /* Shift addresses by 2 (32-bit bus) */
  LPC_SC->SCS &= 0xFFFFFFFE;

  printf("EMC for FPGA initialized!\n");
}

int fpga_tumbl_set_reset(int reset)
{   
  if (reset)
    *fpga_tumbl_control |= FPGA_TUMBL_CONTROL_REG_RESET_BIT;
  else
    *fpga_tumbl_control &= ~FPGA_TUMBL_CONTROL_REG_RESET_BIT;
  return 0;
}

int fpga_tumbl_set_halt(int halt)
{
  if (halt)
    *fpga_tumbl_control |= FPGA_TUMBL_CONTROL_REG_HALT_BIT; 
  else
    *fpga_tumbl_control &= ~FPGA_TUMBL_CONTROL_REG_HALT_BIT;
  
  return 0;
}

int fpga_tumbl_set_trace(int trace)
{  
  if (trace)
    *fpga_tumbl_control |= FPGA_TUMBL_CONTROL_REG_TRACE_BIT; 
  else
    *fpga_tumbl_control &= ~FPGA_TUMBL_CONTROL_REG_TRACE_BIT;
  
  return 0;
}

int fpga_tumbl_kick_trace()
{
  int i;
  
  *fpga_tumbl_trace_kick = 1;
  __memory_barrier();

  /* Make sure it's processed */
  for (i = 0; i < 32; i++)
  {}
  
  __memory_barrier();
  printf("Tumbl PC: 0x%08X\n", (unsigned int) *fpga_tumbl_pc);
  return 0;
}

void fpga_tumbl_write(unsigned int offset, unsigned char *ptr, int len)
{
  int i;
  unsigned int *iptr = (unsigned int *)ptr;
  
  for (i = 0; i < len / 4; i++)
    fpga_tumbl_base[(offset / 4) + i] = SWAB32(iptr[i]);
}

/* 
 * Bus measurement - functions can be called via USB interface
 * Proper usage:
 * 1) Measurement read
 * 2) Measurement write
 * 3) Set turnaround
 *      bus is not pipelined, therefore
 *      just necessary delay for I/O to enter
 *      high impedance state (synchronous clocking => only 1 cycle necessary)
 */

/* Cannot be on stack due to memory barrier for gcc */
static uint32_t a, b;

int fpga_measure_bus_read()
{
  int i;
  
  /* Set the delays are set to highest (default) value */
  LPC_EMC->StaticWaitRd0 = 0x1F;
  
  while (LPC_EMC->StaticWaitRd0 >= 0)
  {
    for (i = 0; i < 1024; i++)
    {
      /* Reset the values */
      __memory_barrier();
      a = 0xFFFFFFFF;
      b = 0xFFFFFFFF;
      __memory_barrier();
      /* Read the values several times - so there are two flips at least
       * NOTE: SDRAM reads / writes may occur in between!
       */
      a = *fpga_bus_meas_read1;
      b = *fpga_bus_meas_read2;
      a = *fpga_bus_meas_read1;
      b = *fpga_bus_meas_read2;
      a = *fpga_bus_meas_read1;
      b = *fpga_bus_meas_read2;
      a = *fpga_bus_meas_read1;
      b = *fpga_bus_meas_read2;
      __memory_barrier();
      
      /* Verify */
      if (a != MEAS_VAL1 || b != MEAS_VAL2)
      {
        if (LPC_EMC->StaticWaitRd0 == 0x1F)
        {
          printf("ERROR: FPGA bus is not working properly!\n"); 
          return 1;
        }
        else
        {
          LPC_EMC->StaticWaitRd0++;
          printf("FPGA bus: StaticWaitRd0 set to 0x%02X\n", (unsigned int) LPC_EMC->StaticWaitRd0);
          return 0;
        }
      }
    }
    
    /* We're good, lower it */
    if (LPC_EMC->StaticWaitRd0 == 0)
    {
      printf("FPGA bus: StaticWaitRd0 set to 0x%02X\n", (unsigned int) LPC_EMC->StaticWaitRd0);
      break;
    }
    else
      LPC_EMC->StaticWaitRd0--;
  }

  return 0;
}

int fpga_measure_bus_write()
{
  int i;
  
  /* Set the delays are set to highest (default) value */
  LPC_EMC->StaticWaitWr0 = 0x1F;

  while (LPC_EMC->StaticWaitWr0 >= 0)
  {
    for (i = 0; i < 1024; i++)
    {
      /* Make sure there is nothing other going on */
      __memory_barrier();
      *fpga_bus_meas_write1 = 0x00000000;
      *fpga_bus_meas_write2 = 0x00000000;
      __memory_barrier();
      a = 0xFFFFFFFF;
      b = 0xFFFFFFFF;
      __memory_barrier();
      /* Write the values several times - so there are two flips at least
       * NOTE: SDRAM reads / writes may occur in between!
       */
      *fpga_bus_meas_write1 = MEAS_VAL1;
      *fpga_bus_meas_write2 = MEAS_VAL2;
      *fpga_bus_meas_write1 = MEAS_VAL1;
      *fpga_bus_meas_write2 = MEAS_VAL2;
      *fpga_bus_meas_write1 = MEAS_VAL1;
      *fpga_bus_meas_write2 = MEAS_VAL2;
      *fpga_bus_meas_write1 = MEAS_VAL1;
      *fpga_bus_meas_write2 = MEAS_VAL2;
      /* 
       * Strongly ordered memory
       * GCC is blocked by volatilness
       */
      __memory_barrier();
      a = *fpga_bus_meas_write1;
      b = *fpga_bus_meas_write2;
      __memory_barrier();
      
      /* Verify */
      if (a != MEAS_VAL1 || b != MEAS_VAL2)
      {
        if (LPC_EMC->StaticWaitWr0 == 0x1F)
        {
          printf("ERROR: FPGA bus is not working properly!\n");
          printf("a = 0x%08X, b = 0x%08X\n", (unsigned int) a, (unsigned int) b);
          return 1;
        }
        else
        {
          LPC_EMC->StaticWaitWr0++;
          printf("FPGA bus: StaticWaitWr0 set to 0x%02X\n", (unsigned int) LPC_EMC->StaticWaitWr0);
          return 0;
        }
      }
    }
    
    /* We're good, lower it */
    if (LPC_EMC->StaticWaitWr0 == 0)
    {
      printf("FPGA bus: StaticWaitWr0 set to 0x%02X\n", (unsigned int) LPC_EMC->StaticWaitWr0);
      break;
    }
    else
      LPC_EMC->StaticWaitWr0--;
  }

  return 0;
}

void fpga_set_reconfiguration_lock(int lock)
{
  fpga_reconfiguration_locked = lock;
}

int fpga_get_reconfiguration_lock()
{
  return fpga_reconfiguration_locked;
}

int fpga_configure()
{
  int i, j;
  uint16_t *data;
  char *magic;
  uint32_t size;

  if (fpga_configured && fpga_reconfiguration_locked)
    return FPGA_CONF_ERR_RECONF_LOCKED;

  if (fpga_reconfiguaration_initiated != NULL)
    fpga_reconfiguaration_initiated();

  /* Make sure INIT_B is set as input */
  hal_gpio_direction_input(XC_INIT_PIN);

  /* PROGRAM_B to low */
  hal_gpio_set_value(XC_PROGRAM_PIN, 0);

  /* Wait some cycles (minimum: 500 ns) */
  for (i = 0; i < 4096; i++)
    __memory_barrier();

  /* SUSPEND to low (permamently) */
  hal_gpio_set_value(XC_SUSPEND_PIN, 0);

  /* Wait some cycles (minimum: 500 ns) */
  for (i = 0; i < 4096; i++)
    __memory_barrier();

  /* PROGRAM_B to high */
  hal_gpio_set_value(XC_PROGRAM_PIN, 1);

  /* Wait for INIT_B to be high */
  j = 0;

  while (!hal_gpio_get_value(XC_INIT_PIN))
  {
    if (j >= 4096)
    {
      hal_gpio_set_value(XC_SUSPEND_PIN, 1);
      printf("FPGA reset failed!\n");
      return FPGA_CONF_ERR_RESET_FAIL;
    }
    __memory_barrier();
    j++;
  }

  /* Use highest EMC delays */
  LPC_EMC->StaticWaitRd0 = 0x1F;
  LPC_EMC->StaticWaitWr0 = 0x1F;

  /* Assert RWDR to WRITE */
  hal_gpio_set_value(XC_RDWR_PIN, 0);

  /* Wait */
  for (i = 0; i < 500; i++)
    __memory_barrier();

  /* Synchronize clock */
  for (i = 0; i < 32; i++)
    *fpga_configure_line = 0xFFFF;

  /* Send bin file (NOTE: Bits must be reversed!) via EMC)
   *
   * Embedded steps:
   * 1) sync
   * 2) device ID check
   * 3) send configuration data
   * 4) crc check
   *
   * INIT_B is LOW in case of a failure
   * DONE is HIGH in case of a success
   *
   * When DONE is HIGH, deassert RWDR and do GPIO reconfiguration:
   *
   * GPIOs need to be reconfigured:
   *
   * INIT_B - used as reset, triggered LOW right after startup,
   *          change from input to output (OUTPUT DRAIN)
   */

  /* Get size */
  magic = (char *)FPGA_CONFIGURATION_FILE_ADDRESS;

  if (magic[0] != 'F' || magic[1] != 'P' || magic[2] != 'G' || magic[3] != 'A')
  {
    hal_gpio_set_value(XC_SUSPEND_PIN, 1);
    return 1;
  }

  size = (*(uint32_t *)(FPGA_CONFIGURATION_FILE_ADDRESS + 4)) >> 1;
  data = (uint16_t *)(FPGA_CONFIGURATION_FILE_ADDRESS + 4 + sizeof(uint32_t));

  /* Periodically check for failure */
  i = 0;
  j = 0;

  while (i < size)
  {
    *fpga_configure_line = *data;

    if (j >= 128)
    {
      /* Check state */
      if (!hal_gpio_get_value(XC_INIT_PIN))
      {
        hal_gpio_set_value(XC_SUSPEND_PIN, 1);
        printf("FPGA configuration write word %d failed!\n", i);
        return FPGA_CONF_ERR_WRITE_ERR;
      }

      j = 0;
    }

    j++;
    i++;
    data++;
  }

  /* We're done, deassert RDWR */
  hal_gpio_set_value(XC_RDWR_PIN, 1);

  while (!hal_gpio_get_value(XC_DONE_PIN))
  {
    if (!hal_gpio_get_value(XC_INIT_PIN))
    {
      hal_gpio_set_value(XC_SUSPEND_PIN, 1);
      printf("FPGA CRC check failed!\n");
      return FPGA_CONF_ERR_CRC_ERR;
    }
  }

  /* Issue startup clocks with data all 1s (at least 8 recommended) */
  for (i = 0; i < 16; i++)
    *fpga_configure_line = 0xFFFF;

  /* In our design, INIT_B is used as reset, convert it to output, and trigger it */
  hal_gpio_direction_output(XC_INIT_PIN, 0);

  /* Hold it for some time */
  for (i = 0; i < 128; i++)
    __memory_barrier();

  /* Use EMC delays obtained through measurement */
  LPC_EMC->StaticWaitWr0 =   0x02;
  LPC_EMC->StaticWaitWen0 =  0x01;
  LPC_EMC->StaticWaitRd0 =   0x08;
  LPC_EMC->StaticWaitPage0 = 0x07;
  LPC_EMC->StaticWaitOen0 =  0x01;
  LPC_EMC->StaticWaitTurn0 = 0x01;

  /* Lift the reset */
  hal_gpio_direction_output(XC_INIT_PIN, 1);

   /* Give it some time */
  for (i = 0; i < 1024; i++)
    __memory_barrier();

  fpga_configured = 1;
  printf("FPGA configured!\n");

  if (fpga_reconfiguaration_finished != NULL)
    fpga_reconfiguaration_finished();

  return FPGA_CONF_SUCESS;
}
