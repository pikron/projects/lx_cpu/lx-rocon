#include <system_def.h>
#include <stdio.h>
#include <string.h>

#include "appl_defs.h"
#include "appl_version.h"
#include "appl_fpga.h"

#ifdef CONFIG_OC_MTD_DRV_SYSLESS
#include <mtd_spi_drv.h>
#endif

#ifndef APPL_WITH_SIM_POSIX
#include <hal_machperiph.h>
#endif /*APPL_WITH_SIM_POSIX*/

#ifdef CONFIG_KEYVAL
#include <lpciap.h>
#include <keyval_id.h>
#include <keyval_loc.h>
#include <lpciap_kvpb.h>
#include <bbconf_info.h>

#endif /*CONFIG_KEYVAL*/

#ifdef CONFIG_OC_I2C_DRV_SYSLESS
#include <i2c_drv.h>
extern i2c_drv_t i2c_drv;
#endif

#ifdef CONFIG_OC_SPI_DRV_SYSLESS
#include <spi_drv.h>
#endif

#ifdef APPL_WITH_PSINDCTL
#include "psindctl_spi.h"
#ifdef APPL_WITH_PSINDCTL_PAST
#include "psindctl_past.h"
#endif /*APPL_WITH_PSINDCTL_PAST*/
#endif /*APPL_WITH_PSINDCTL*/

#include <ul_log.h>
#include <ul_logreg.h>

#if defined(APPL_WITH_SUITK) && defined(OMK_FOR_TARGET)
#define UL_LOG_TO_STDOUT
extern FILE *ul_log_default_file;
#endif

/***********************************/
// global variables
#ifdef CONFIG_KEYVAL
kvpb_block_t kvpb_block_global;
kvpb_block_t *kvpb_block = &kvpb_block_global;
#endif /*CONFIG_KEYVAL*/

#ifdef APPL_WITH_ULAN
uloi_con_ulan_t uloi_con_ulan_global;
uloi_coninfo_t *coninfo = &uloi_con_ulan_global.con;

#ifdef CONFIG_ULAN_DY
UL_DYAC_VAR_LOC ul_dyac_t ul_dyac_global;
ul_dyac_t *ul_dyac = &ul_dyac_global;
char ul_dyac_gst_reply[4 + 2];
#endif /*CONFIG_ULAN_DY*/

/* ulan variables */
ul_fd_t ul_fd, ul_fd1;
ul_msginfo umsginfo;
uint8_t uaddr = 3;

#ifdef APPL_WITH_UL_FD4TIMEPROG
ul_fd_t ul_fd4timeprog;
#endif
#endif /*APPL_WITH_ULAN*/

#ifdef CONFIG_OC_MTD_DRV_SYSLESS
struct mtd_spi_state_t mtd_spi_state = {
  .spi_addr = 0x08,
  .page_size = 256,
};
#endif

uint32_t usn; /* uLAN/USB device serial number */

int usb_enable_flag = 1;

LT_TIMER_IMP(lt_10msec)

/***********************************/
/* alternative memory reservation for framebuffer and uLan*/
#ifndef APPL_WITH_SIM_POSIX
void *(* const gr_driver_reserve_ram)(size_t size) =
  lpc_reserve_usb_ram;

void *(* const ul_drv_reserve_ram)(size_t size) =
  lpc_reserve_usb_ram;

void *(* const timeprog_reserve_ram)(size_t size) =
  lpc_reserve_usb_ram;

void *(* const appl_distore_reserve_ram)(size_t size) =
  lpc_reserve_usb_ram;
#endif /*APPL_WITH_SIM_POSIX*/

/***********************************/
int sys_err()
{
  printf("System error at %p\n", __builtin_return_address(0));

  while (1);
}

/***********************************/
char ul_save_sn(uint32_t usn)
{
#ifdef CONFIG_KEYVAL
  kvpb_set_key(kvpb_block, KVPB_KEYID_ULAN_SN, 4, &usn);
#endif /*CONFIG_KEYVAL*/
  return 0;
}

/***********************************/
char ul_save_adr(uint8_t uaddr)
{
#ifdef CONFIG_KEYVAL
  kvpb_set_key(kvpb_block, KVPB_KEYID_ULAN_ADDR, 1, &uaddr);
#endif /*CONFIG_KEYVAL*/
  return 0;
}

void save_cfg()
{
  // kvpb_set_key(kvpb_block,KVPB_KEYID_APPL_PARAM,sizeof(appl_param_t),&appl_param);
}

/***********************************/
#ifdef CONFIG_OC_SPI_DRV_SYSLESS
// received data bits 28..31 KBD variant (1)
//               bits  1..25 keys
//               bit       0 sticky 1
// send data     bits 12..15 LED indicators
//               bits  0.. 7 LED indicators
//               bit      10 backlight full
//               bit      11 backlight medium
//               bit       8 beep on (0)
//               bit       9 tone freq select
// LEDs RUN 0, HOLD 1, PURGE 2, ERROR 3, A 5, B 6, C 7
//      D 12, MOTOR 14
// Keys '1'-'9' 1-9, '.' 10, '0' 11, ENTER 12, STOP 13, START 14,
//      down 15, right 16, up 17, lesft 18, ESC 19, P/M 20,
//      D 21, C 22, B 23, A 24, MENU 25
int check_spi_kbd_presence()
{
  int res;
  uint8_t tx_buff[4] = {0x00, 0x00, 0x09, 0x55};
  uint8_t rx_buff[4];
  spi_drv_t *spi_drv = spi_find_drv(NULL, 0);

  if (spi_drv == NULL)
    return -1;

  res = spi_transfer(spi_drv, 4, 4, tx_buff, rx_buff);

  if (res < 0)
    return -1;

  printf("kbd_presence %02x %02x %02x %02x\n",
         rx_buff[0], rx_buff[1], rx_buff[2], rx_buff[3]);

  return 0;
}
#endif /*CONFIG_OC_SPI_DRV_SYSLESS*/

/***********************************/

#ifndef APPL_WITH_SIM_POSIX
#include <hal_gpio.h>
int appl_adc_init()
{
  hal_pin_conf(ADC0_PIN);
  hal_pin_conf(ADC1_PIN);
  hal_pin_conf(ADC2_PIN);
  hal_pin_conf(ADC3_PIN);
  hal_pin_conf(ADC7_PIN);

  LPC_SC->PCONP |= (1 << 12); /*PCADC*/

  LPC_ADC->CR = (1 << 21); /*PDN*/

  LPC_ADC->CR |= 0x8F; /*SEL selsect channels 0 to 3 and 7*/
  LPC_ADC->CR |= (9 << 8); /*CLKDIV*/
  LPC_ADC->CR |= (1 << 16); /*BURST start burst mode*/

  return 0;
}
#endif /*APPL_WITH_SIM_POSIX*/

/***********************************/

#if !defined(APPL_WITH_SIM_POSIX) && \
    defined(APPL_RUN_AT_FAST_SFI)
extern void (*timer0_isr_appl_call)(void);

#ifdef APPL_RUN_AT_SLOW_SFI
long appl_run_fast_sfi_usec = 1000000 / SYS_TIMER_HZ;
long appl_run_slow_sfi_usec = APPL_SLOW_SFI_USEC;
long appl_run_fast2slow_sfi_accum;
#endif /*APPL_RUN_AT_SLOW_SFI*/

void appl_run_at_fast_sfi(void)
{
  APPL_RUN_AT_FAST_SFI;
#if defined(SLOW_SFI_INVOKE_IRQn) && defined(APPL_RUN_AT_SLOW_SFI)
  {
    appl_run_fast2slow_sfi_accum += appl_run_fast_sfi_usec;

    if (appl_run_fast2slow_sfi_accum >= appl_run_slow_sfi_usec)
    {
      appl_run_fast2slow_sfi_accum -= appl_run_slow_sfi_usec;
      NVIC->STIR = SLOW_SFI_INVOKE_IRQn;
    }
  }
#endif /*SLOW_SFI_INVOKE_IRQn*/
}

int appl_run_at_fast_sfi_setup(void)
{
  timer0_isr_appl_call = appl_run_at_fast_sfi;
  return 0;
}

#else /*APPL_WITH_SIM_POSIX APPL_RUN_AT_FAST_SFI */
int appl_run_at_fast_sfi_setup(void)
{
  return 0;
}
#endif /*APPL_WITH_SIM_POSIX APPL_RUN_AT_FAST_SFI */

/***********************************/

#if !defined(APPL_WITH_SIM_POSIX) && \
    defined(APPL_RUN_AT_SLOW_SFI)

IRQ_HANDLER_FNC(appl_run_at_slow_sfi_isr)
{
  APPL_RUN_AT_SLOW_SFI;
}

int appl_run_at_slow_sfi_setup(void)
{
  if (request_irq(SLOW_SFI_INVOKE_IRQn, appl_run_at_slow_sfi_isr,
                  0, "slow_sfi", NULL) < 0)
    return -1;

  return 0;
}
#else /* APPL_WITH_SIM_POSIX APPL_RUN_AT_SLOW_SFI */

int appl_run_at_slow_sfi_setup(void)
{
  return 0;
}

#endif /* APPL_WITH_SIM_POSIX APPL_RUN_AT_SLOW_SFI */

/***********************************/

/*
UL_LOG_CUST(ulogd_main)
*/

#define UL_LOGL_DEF UL_LOGL_MSG

#include "log_domains.inc"

static void register_logdomains(void)
{
  ul_logreg_domains_static(ul_log_domains_array, sizeof(ul_log_domains_array) / sizeof(ul_log_domains_array[0]));
}


/***********************************/
// Load FPGA configuration from SPI Flash

#ifdef CONFIG_OC_MTD_DRV_SYSLESS
int fpga_load_config_from_flash(void)
{
  uint32_t size;
  char *magic;
  void *fpga_data = (void*)FPGA_CONFIGURATION_FILE_ADDRESS;
  uint32_t fpga_flash_addr = 0x4000;
  int res;
  uint32_t tumbl_flash_addr = 0x2000;
  uint32_t tumbl_flash_size = 0x0800;
  uint32_t u;

  res = mtd_spi_read(&mtd_spi_state, fpga_data, 8, fpga_flash_addr, 0);
  if (res < 0)
    return res;

  magic = (char *)fpga_data;
  if (magic[0] != 'F' || magic[1] != 'P' || magic[2] != 'G' || magic[3] != 'A')
  {
    return 0;
  }

  size = *(uint32_t *)(fpga_data + 4);
  if (size > 512 * 1024) {
    return -8;
  }

  res = mtd_spi_read(&mtd_spi_state, fpga_data, 8 + size, fpga_flash_addr, 0);
  if (res < 0)
    return res;

  res = fpga_configure();
  if (res != FPGA_CONF_SUCESS)
    return -100 - res;

  res = mtd_spi_read(&mtd_spi_state, fpga_data, tumbl_flash_size,
                     tumbl_flash_addr, 0);
  if (res < 0)
    return res;

  if ((*(uint32_t *)(fpga_data) == 0x00000000) ||
      (*(uint32_t *)(fpga_data) == 0xffffffff))
    return 2;

  fpga_tumbl_set_reset(1);

  for (u = 0; u < 0x1000 >> 2; u++)
    fpga_tumbl_dmem[u] = 0;

  fpga_tumbl_write(0, (unsigned char *)fpga_data, tumbl_flash_size);

  fpga_tumbl_set_reset(0);

 #ifdef WATCHDOG_ENABLED
  watchdog_feed();
 #endif /* WATCHDOG_ENABLED */

  if (pxmcc_curadc_zero(1) < 0)
    return -200;

  return 1;
}
#endif

/***********************************/
int main()
{
  int i;

#ifndef APPL_WITH_SIM_POSIX

  /* Setup default interrupt priority into middle of the range */
  for (i = 0; i < sizeof(NVIC->IP); i++)
    NVIC->IP[i] = 0x80;

#ifdef SLOW_SFI_INVOKE_IRQn
  /* The lowes priority to IRQ invoking slow activities */
  NVIC->IP[SLOW_SFI_INVOKE_IRQn] = 0xc0;
#endif

  /* Higher priority for SPI to gradient valves */
  NVIC->IP[SSP1_IRQn] = 0x40;

  /* LXPWR master receive interrupt (deadline under 50 usec) */
  NVIC->IP[TIMER2_IRQn] = 0x30;

  /* Start of the cam period */
  NVIC->IP[MCPWM_IRQn] = 0x20;

  /* The most critical - position compare events */
  NVIC->IP[QEI_IRQn] = 0x00;
#endif /*APPL_WITH_SIM_POSIX*/

  /*
  unsigned long bb_val;
  bbconf_get_param(BBCONF_PTTAG_BBVER, &bb_val);
  */
#ifdef UL_LOG_TO_STDOUT
  ul_log_default_file = stdout;
#endif
  register_logdomains();

  printf(APP_VER_ID " initializing\n");

#ifdef SDRAM_BASE

  if (((void *)main >= (void *)SDRAM_BASE) &&
      ((void *)main <= (void *)(SDRAM_BASE + SDRAM_SIZE)))
    printf("running from SDRAM\n");

#endif /*SDRAM_BASE*/

#ifdef CONFIG_KEYVAL
  /***********************************/
  // kvpb init
  kvpb_block->base = (uint8_t *)KEYVAL_START;
  kvpb_block->size = KEYVAL_PAGE_LEN;
  kvpb_block->flags = KVPB_DESC_DOUBLE | KVPB_DESC_CHUNKWO;
  kvpb_block->chunk_size = KVPB_CHUNK_SIZE;
  kvpb_block->erase = lpcisp_kvpb_erase;
  kvpb_block->copy = lpcisp_kvpb_copy;
  kvpb_block->flush = lpcisp_kvpb_flush;

  if (kvpb_check(kvpb_block, 1) < 0) sys_err();

  if (kvpb_check(kvpb_block, 1) < 0) sys_err();

  printf("Keyval ready\n");

  /***********************************/
  // set configuration for device
#ifdef APPL_WITH_ULAN
  kvpb_get_key(kvpb_block, KVPB_KEYID_ULAN_ADDR, 1, &uaddr);
  kvpb_get_key(kvpb_block, KVPB_KEYID_ULAN_SN, 4, &usn);
#endif /*APPL_WITH_ULAN*/

//  kvpb_get_key(kvpb_block,KVPB_KEYID_APPL_PARAM,sizeof(appl_param_t),&appl_param);

  printf("Keyval variables read\n");
#endif /*CONFIG_KEYVAL*/

  /***********************************/
  // timers
  lt_10msec_init();

#ifdef APPL_WITH_ULAN
  /***********************************/
  // ulan init
  // uld_printk_flush();
  ul_fd = ul_open(NULL, NULL);

  if (ul_fd == UL_FD_INVALID) sys_err();

  ul_fd1 = ul_open(NULL, NULL);

  if (ul_fd1 == UL_FD_INVALID) sys_err();

  ul_setidstr(ul_fd, ul_idstr);
#ifdef APPL_WITH_UL_FD4TIMEPROG
  ul_fd4timeprog = ul_open(NULL, NULL);

  if (ul_fd4timeprog == UL_FD_INVALID) sys_err();

#endif /*APPL_WITH_UL_FD4TIMEPROG*/

  printf("uLAN open\n");

  ul_setmyadr(ul_fd, uaddr);
  printf("Setting address %d\n", uaddr);

  //umsginfo.sadr=0;
  //umsginfo.dadr=0;
  //umsginfo.cmd=0;
  //ul_addfilt(ul_fd,&umsginfo);

#ifndef APPL_WITH_SIM_POSIX
  ul_stroke(ul_fd);
#endif /*APPL_WITH_SIM_POSIX*/

  umsginfo.dadr = 2;
  umsginfo.cmd = 5;
  umsginfo.flg = UL_BFL_SND;
  //ul_newmsg(ul_fd,&umsginfo);
  //ul_freemsg(ul_fd);

  /***********************************/
#ifdef CONFIG_ULAN_DY
  // uLan dyac init
  uldy_init(ul_dyac, ul_fd, ul_save_sn, ul_save_adr, (char *)ul_idstr, usn);

  memset(&umsginfo, 0, sizeof(umsginfo));
  umsginfo.cmd = UL_CMD_NCS;
  ul_addfilt(ul_fd, &umsginfo);

  ul_opdata_add_iac(ul_fd, UL_CMD_GST, UL_IAC_OP_SND, ul_iac_call_gst, ul_dyac_gst_reply, sizeof(ul_dyac_gst_reply), 0, ul_dyac);

  printf("uLAN dynamic adressing initialized\n");
#endif /*CONFIG_ULAN_DY*/

  /***********************************/
  // uLan object interface init
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);

  memset(&umsginfo, 0, sizeof(umsginfo));
  umsginfo.cmd = UL_CMD_OISV;
  ul_addfilt(uloi_con_ulan_rdfd(coninfo), &umsginfo);
#endif /*APPL_WITH_ULAN*/
  /***********************************/
  // DINFO and other persistent data
#ifdef APPL_WITH_DISTORE_EEPROM_USER
  appl_distore_init();
#endif /* APPL_WITH_DISTORE_EEPROM_USER */

  /***********************************/
  // SPI connected FLASH
#ifdef CONFIG_OC_MTD_DRV_SYSLESS
  mtd_spi_state.spi_drv = spi_find_drv(NULL, 0);
#endif

  /***********************************/
  // Micro Windows GUI

#ifdef CONFIG_OC_MWENGINE
#ifdef CONFIG_OC_SPI_DRV_SYSLESS
  check_spi_kbd_presence();
#endif /*CONFIG_OC_SPI_DRV_SYSLESS*/
  i = gui_init();
  printf("gui_init ret %d\n", i);
#endif /*CONFIG_OC_MWENGINE*/

  /***********************************/
  // USB initialization

#ifdef APPL_WITH_USB
  usb_app_fill_serial_number(usn);

  if (usb_enable_flag)
  {
    usb_app_init();
    printf("usb_app_init\n");
  }

#endif /*APPL_WITH_USB*/

  /***********************************/
  // LCP engine initialization

#ifndef APPL_WITH_SIM_POSIX
  appl_adc_init();
#ifdef APPL_WITH_AUX_IO
  aux_io_init();
#endif /*APPL_WITH_AUX_IO*/
#ifdef APPL_WITH_DIO
  dio_init();
#endif /*APPL_WITH_DIO*/
#endif /*APPL_WITH_SIM_POSIX*/

#ifdef APPL_WITH_DISTORE_KEYVAL_SERVICE
  setup_distore_restore();
#endif /*APPL_WITH_DISTORE_KEYVAL_SERVICE*/

#ifdef APPL_WITH_DISTORE_EEPROM_USER
  appl_distore_user_restore();
#endif /* APPL_WITH_DISTORE_EEPROM_USER */

#ifdef APPL_WITH_TIMEPROG_EEPROM_STORE
  appl_timeprog_eeprom_restore();
#endif /*APPL_WITH_TIMEPROG_EEPROM_STORE*/

  /* Init FPGA */
  fpga_init();

#ifdef CONFIG_PXMC
  fpga_reconfiguaration_initiated = pxmc_done;
  fpga_reconfiguaration_finished = pxmc_initialize;
  printf("setup pxmc_initialize hook for FPGA\n");
#endif /*CONFIG_PXMC*/

  appl_run_at_slow_sfi_setup();
  appl_run_at_fast_sfi_setup();

  /***********************************/
  // Load FPGA configuration
#ifdef CONFIG_OC_MTD_DRV_SYSLESS
  i = fpga_load_config_from_flash();
  printf("fpga_load_config_from_flash %d\n", i);
#endif

#ifdef APPL_WITH_AUX_IO
  aux_out_set_init_val();
#endif /*APPL_WITH_AUX_IO*/

#ifdef APPL_WITH_PSINDCTL
 #ifdef APPL_WITH_PSINDCTL_PAST
  psindctl_past_init();
 #endif /*APPL_WITH_PSINDCTL_PAST*/
#endif /*APPL_WITH_PSINDCTL*/

#ifdef APPL_WITH_LWIP
  i = lwip_app_init();
  printf("flwip_app_init %d\n", i);
#endif /*APPL_WITH_LWIP*/

#ifdef APPL_WITH_UART1CMD
  i = uart1_app_init();
  printf("uart1_app_init %d\n", i);
#endif

  /***********************************/
  // Setup command processor
  cmdproc_init();

  /***********************************/
  // Main application loop
  mloop();

  /***********************************/
  // Greace finalization of application to check memory leakage
#ifdef APPL_WITH_FINALIZATION_CHECK

#ifdef CONFIG_OC_MWENGINE
  gui_done();
#endif /*CONFIG_OC_MWENGINE*/

#endif /*APPL_WITH_FINALIZATION_CHECK*/

  return 0;
}

