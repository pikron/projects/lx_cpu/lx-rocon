#include <system_def.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cmd_proc.h>
#include <ul_list.h>

#include "appl_defs.h"
#include "cmd_instances.h"
#include "appl_cmdaltproc.h"
#include "appl_dprint.h"

#ifdef APPL_WITH_DIO
#include <gen_dio.h>
#endif /*APPL_WITH_DIO*/

#ifdef APPL_WITH_CMPQUE
#include <pxmc_cmpque.h>
extern cmd_io_t *cmpque_last_used_cmd_io;
#endif /*APPL_WITH_CMPQUE*/

typedef struct cmdproc_instance_state_t {
  ul_list_node_t cmdproc_peers;
  unsigned long axes_rq_mask;
  int global_rq;
  unsigned dbg_prt_flg;
  cmd_io_t* cmd_io;
  cmd_altlnproc_fnc_in_t *altlnproc_in;
  cmd_altlnproc_fnc_out_t *altlnproc_out;
  cmd_altlnproc_fnc_done_t *altlnproc_done;
  void *altlnproc_ptr_arg;
  long altlnproc_long_arg;
} cmdproc_instance_state_t;

typedef struct cmdproc_instances_t {
  ul_list_head_t instances;
  cmd_des_t const **commands;
} cmdproc_instances_t;

UL_LIST_CUST_DEC(cmdproc_instance, cmdproc_instances_t, cmdproc_instance_state_t,
                 instances, cmdproc_peers)

cmdproc_instances_t cmdproc_instances;

#ifndef APPL_WITH_SIM_POSIX
extern cmd_io_t cmd_io_usbcon;
extern cmd_io_t cmd_io_uartcon;
extern cmd_io_t cmd_io_uart1con;

cmdproc_instance_state_t cmdproc_instance_uartcon = {
  .cmd_io = &cmd_io_uartcon,
};

#ifdef APPL_WITH_USB
cmdproc_instance_state_t cmdproc_instance_usbcon = {
  .cmd_io = &cmd_io_usbcon,
};
#endif /*APPL_WITH_USB*/

#ifdef APPL_WITH_UART1CMD
cmdproc_instance_state_t cmdproc_instance_uart1con = {
  .cmd_io = &cmd_io_uart1con,
};
#endif /*APPL_WITH_UART1CMD*/

#else /*APPL_WITH_SIM_POSIX*/
extern cmd_io_t cmd_io_std_line;

cmdproc_instance_state_t cmdproc_instance_std_line = {
  .cmd_io = &cmd_io_std_line,
};

#endif /*APPL_WITH_SIM_POSIX*/

extern cmd_des_t const *cmd_pxmc_base[];
extern cmd_des_t const *cmd_appl_tests[];
extern cmd_des_t const *cmd_pxmc_ptable[];
extern cmd_des_t const *cmd_pxmc_coordmv[];
extern cmd_des_t const *cmd_cmpque_tab[];
extern cmd_des_t const *cmd_dio_tab[];
extern cmd_des_t const *cmd_appl_specific[];
extern cmd_des_t const *cmd_appl_pxmc[];
extern cmd_des_t const *cmd_appl_lwip[];

cmd_des_t const **cmd_list;

cmd_des_t const cmd_des_help =
{
  0, 0,
  "help", "prints help for commands",
  cmd_do_help,
  {
    (char *) &cmd_list
  }
};

cmdproc_instance_state_t *cmdproc_instance_state4cmd_io(cmd_io_t *cmd_io)
{
  cmdproc_instance_state_t *cmdinst;

  ul_list_for_each(cmdproc_instance, &cmdproc_instances, cmdinst) {
    if (cmdinst->cmd_io == cmd_io)
      return cmdinst;
  }
  return NULL;
}

int cmdproc_instances_insert_cmdio(cmd_io_t *cmd_io)
{
  cmdproc_instance_state_t *cmdinst;

  cmdinst = (typeof(cmdinst))malloc(sizeof(*cmdinst));
  if (cmdinst == NULL)
    return -1;
  memset(cmdinst, 0, sizeof(*cmdinst));
  cmdinst->cmd_io = cmd_io;
  cmdproc_instance_insert(&cmdproc_instances, cmdinst);

  return 0;
}

int cmdproc_instances_remove_cmdio(cmd_io_t *cmd_io)
{
  cmdproc_instance_state_t *cmdinst;

  cmdinst = cmdproc_instance_state4cmd_io(cmd_io);
  if (cmdinst == NULL)
    return -1;

  cmdproc_altproc_done(cmd_io);

 #ifdef APPL_WITH_DIO
  if (cmd_io == dio_last_used_cmd_io)
    dio_last_used_cmd_io = NULL;
 #endif /*APPL_WITH_DIO*/

 #ifdef APPL_WITH_CMPQUE
  if (cmd_io == cmpque_last_used_cmd_io)
    cmpque_last_used_cmd_io = NULL;
  cmpque_fromtrig_cmd_io_ceased(cmd_io);
  cmpque_dio_cmd_io_ceased(cmd_io);
 #endif /*APPL_WITH_CMPQUE*/

  cmdproc_instance_delete(&cmdproc_instances, cmdinst);
  memset(cmdinst, 0, sizeof(*cmdinst));
  free (cmdinst);
  return 0;
}

int cmdproc_altproc_setup(cmd_io_t *cmd_io, cmd_altlnproc_fnc_in_t *altlnproc_in,
        cmd_altlnproc_fnc_out_t *altlnproc_out,  cmd_altlnproc_fnc_done_t *altlnproc_done,
        void *altlnproc_ptr_arg, long altlnproc_long_arg)
{
  cmdproc_instance_state_t *cmdinst;
  if ((cmdinst = cmdproc_instance_state4cmd_io(cmd_io)) == NULL)
    return -1;

  cmdinst->altlnproc_in = altlnproc_in;
  cmdinst->altlnproc_out = altlnproc_out;
  cmdinst->altlnproc_done = altlnproc_done;
  cmdinst->altlnproc_ptr_arg = altlnproc_ptr_arg;
  cmdinst->altlnproc_long_arg = altlnproc_long_arg;

  return 0;
}

int cmdproc_altproc_set_ptr_arg(cmd_io_t *cmd_io, void *altlnproc_ptr_arg)
{
  cmdproc_instance_state_t *cmdinst;
  if ((cmdinst = cmdproc_instance_state4cmd_io(cmd_io)) == NULL)
    return -1;

  cmdinst->altlnproc_ptr_arg = altlnproc_ptr_arg;

  return 0;
}

int cmdproc_altproc_set_long_arg(cmd_io_t *cmd_io, long altlnproc_long_arg)
{
  cmdproc_instance_state_t *cmdinst;
  if ((cmdinst = cmdproc_instance_state4cmd_io(cmd_io)) == NULL)
    return -1;

  cmdinst->altlnproc_long_arg = altlnproc_long_arg;

  return 0;
}

void cmdproc_altproc_done(cmd_io_t *cmd_io)
{
  cmdproc_instance_state_t *cmdinst;
  if ((cmdinst = cmdproc_instance_state4cmd_io(cmd_io)) == NULL)
    return;

  if (cmdinst->altlnproc_done != NULL)
    cmdinst->altlnproc_done(cmd_io, cmdinst->altlnproc_ptr_arg, cmdinst->altlnproc_long_arg);

  cmdinst->altlnproc_in = NULL;
  cmdinst->altlnproc_out = NULL;
  cmdinst->altlnproc_done = NULL;
  cmdinst->altlnproc_ptr_arg = NULL;
  cmdinst->altlnproc_long_arg = 0;
}

unsigned *cmdproc_dbg_prt_flg_for_cmd_io(cmd_io_t *cmd_io)
{
  cmdproc_instance_state_t *cmdinst;
  if ((cmdinst = cmdproc_instance_state4cmd_io(cmd_io)) == NULL)
    return NULL;

  return &cmdinst->dbg_prt_flg;
}

#undef putc

int cmdproc_setup_nonblock_cmd_io_proxy_putc(cmd_io_t *cmd_io, int ch)
{
  int res;
  cmd_io_t *cmd_io_target = (cmd_io_t *)cmd_io->priv.device.ptr;
  if (cmd_io->priv.device.pos == 0) {
    res = cmd_io_target->putc(cmd_io_target, ch);
    if (res < 0)
      cmd_io->priv.device.pos = 1;
  }
  return 0;
}

int cmdproc_setup_nonblock_cmd_io_proxy(cmd_io_t *cmd_io, cmd_io_t *cmd_io_target)
{
  memset(cmd_io, 0, sizeof(*cmd_io));
  cmd_io->priv.device.ptr = cmd_io_target;
  cmd_io->putc = cmdproc_setup_nonblock_cmd_io_proxy_putc;
  cmd_io->write = cmd_io_write_bychar;
  return 0;
}

int cmd_do_r_one(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned chan;
  unsigned long val;
  cmdproc_instance_state_t *ckst;

  if (*param[2] != ':') return -CMDERR_OPCHAR;
  chan = *param[1] - 'A';
  if(chan >= 31) return -CMDERR_BADREG;

  ckst = cmdproc_instance_state4cmd_io(cmd_io);
  if (ckst == NULL)
    return -CMDERR_BADDIO;

  ckst->axes_rq_mask |= 1l << chan;
  return 0;
}

int cmd_do_r_all(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  cmdproc_instance_state_t *ckst;

  if(*param[2] != ':') return -CMDERR_OPCHAR;

  ckst = cmdproc_instance_state4cmd_io(cmd_io);
  if (ckst == NULL)
    return -CMDERR_BADDIO;

  ckst->global_rq = 1;
  return 0;
}

cmd_des_t const cmd_des_r_one={0, CDESM_OPCHR,
			"R?","send R?! or FAIL?! at axis finish",cmd_do_r_one,{}};

cmd_des_t const cmd_des_r_all={0, CDESM_OPCHR,
			"R","send R! or FAIL! at finish",cmd_do_r_all,{}};

cmd_des_t const *cmd_list_main[] =
{
  &cmd_des_help,
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_tests),
#ifdef CONFIG_PXMC
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_base),
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_ptable),
#endif
  &cmd_des_r_one,
  &cmd_des_r_all,
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_coordmv),
#ifdef APPL_WITH_DIO
  CMD_DES_INCLUDE_SUBLIST(cmd_cmpque_tab),
  CMD_DES_INCLUDE_SUBLIST(cmd_dio_tab),
#endif
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_specific),
#ifdef APPL_WITH_LWIP
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_lwip),
#endif /*APPL_WITH_LWIP*/
#ifndef APPL_WITH_SIM_POSIX
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_pxmc),
  &cmd_des_dprint,
#endif /*APPL_WITH_SIM_POSIX*/
  NULL
};

cmdproc_instances_t cmdproc_instances = {
  .commands = cmd_list_main,
};

cmd_des_t const **cmd_list = cmd_list_main;


int cmdproc_init(void)
{
  cmdproc_instance_init_head(&cmdproc_instances);
 #ifndef APPL_WITH_SIM_POSIX
  cmdproc_instance_insert(&cmdproc_instances, &cmdproc_instance_uartcon);
  #ifdef APPL_WITH_USB
  cmdproc_instance_insert(&cmdproc_instances, &cmdproc_instance_usbcon);
  #endif /*APPL_WITH_USB*/
  #ifdef APPL_WITH_UART1CMD
  cmdproc_instance_insert(&cmdproc_instances, &cmdproc_instance_uart1con);
  #endif /*APPL_WITH_UART1CMD*/
 #else /*APPL_WITH_SIM_POSIX*/
  cmdproc_instance_insert(&cmdproc_instances, &cmdproc_instance_std_line);
 #endif /*APPL_WITH_SIM_POSIX*/
  return 0;
}

int cmdproc_process_ready(cmd_io_t* cmd_io,
                  cmdproc_instance_state_t *ckst,
                  unsigned long busy_bits, unsigned long error_bits)
{
  unsigned int chan;
  unsigned long send_bits = ckst->axes_rq_mask &
                            (error_bits | ~busy_bits);
  int send_global = ckst->global_rq && (error_bits || !busy_bits);

  if (cmd_io->priv.ed_line.out->inbuf || (!send_bits && !send_global))
    return 0;

  if (send_bits) {
    chan = ffsl(send_bits) - 1;
    ckst->axes_rq_mask &= ~(1l << chan);
    if (error_bits & (1l << chan))
      cmd_io_write(cmd_io,"FAIL",4);
    else
      cmd_io_write(cmd_io,"R",1);
    cmd_io_putc(cmd_io,chan+'A');
    cmd_io_putc(cmd_io,'!');
    cmd_io_putc(cmd_io,'\r');
    cmd_io_putc(cmd_io,'\n');
    return 1;
  }

  ckst->global_rq = 0;
  if (error_bits)
    cmd_io_write(cmd_io,"FAIL",4);
  else
    cmd_io_write(cmd_io,"R",1);
  cmd_io_putc(cmd_io,'!');
  cmd_io_putc(cmd_io,'\r');
  cmd_io_putc(cmd_io,'\n');
  return 1;
}

/**
 * run command processor instance with support for alternative line processing functions
 */
int cmdproc_run4cmdinst(cmdproc_instance_state_t * cmdinst, cmd_des_t const **commands)
{
  int val;
  cmd_io_t *cmd_io = cmdinst->cmd_io;

  if(cmd_io_line_out(cmd_io))
    return 1; /* Not all the output has been sent. */

  if (cmdinst->altlnproc_out != NULL) {
    val = cmdinst->altlnproc_out(cmd_io, cmdinst->altlnproc_ptr_arg, cmdinst->altlnproc_long_arg);
    if (val < 0)
      cmdproc_altproc_done(cmd_io);
    cmd_processor_run_finish_line(cmd_io, val);
    if (val)
      return 1; /* Not all the output has been sent. */
  }

  if(cmd_io_line_in(cmd_io)<=0)
    return 0; /* Input line not finished or error. */

  if (cmdinst->altlnproc_in != NULL) {
    val = cmdinst->altlnproc_in(cmd_io, cmdinst->altlnproc_ptr_arg,
             cmdinst->altlnproc_long_arg, cmd_io->priv.ed_line.in->buf);
  } else {
    if(commands){
      val = proc_cmd_line(cmd_io, commands, cmd_io->priv.ed_line.in->buf);
    }else{
      val = -CMDERR_BADCMD;
    }
  }

  cmd_processor_run_finish_line(cmd_io, val);

  return 1; /* Command line processed */
}

int cmdproc_poll(void)
{
  cmdproc_instance_state_t * cmdinst;
  static typeof(actual_msec) dbg_prt_last_msec;
  static typeof(actual_msec) old_check_time;
  typeof(actual_msec) new_check_time;
  int ret = 0;
  unsigned long busy_bits, error_bits;

  lt_mstime_update();

  new_check_time = actual_msec;
  if (new_check_time != old_check_time) {
    old_check_time = new_check_time;
    pxmc_process_state_check(&busy_bits, &error_bits);
    ul_list_for_each(cmdproc_instance, &cmdproc_instances, cmdinst) {
      ret |= cmdproc_process_ready(cmdinst->cmd_io, cmdinst,
                                   busy_bits, error_bits);
    }
  }

  {
    cmd_io_t *cmd_io;

   #ifdef APPL_WITH_DIO
    cmd_io = dio_last_used_cmd_io;
    if (cmd_io == NULL) {
      cmd_io = cmdproc_instance_uartcon.cmd_io;
    }

    /* Check for trigger occurences */
    if(!cmd_io->priv.ed_line.out->inbuf && dio_trig_occured && !cmd_io_line_out(cmd_io)){
      do_dio_trig_send(cmd_io);
      if(cmd_io->priv.ed_line.out->inbuf){
        cmd_io_putc(cmd_io, '\r');
        cmd_io_putc(cmd_io, '\n');
        cmd_io_line_out(cmd_io);
      }
    }

    /* Check for comparator events */
    if(!cmd_io->priv.ed_line.out->inbuf && dio_cmp_occured && !cmd_io_line_out(cmd_io)){
      do_dio_cmp_send(cmd_io);
      if(cmd_io->priv.ed_line.out->inbuf){
        cmd_io_putc(cmd_io, '\r');
        cmd_io_putc(cmd_io, '\n');
        cmd_io_line_out(cmd_io);;
      }
    }
   #endif /* WITH_DIO */

   #ifdef APPL_WITH_CMPQUE
    cmd_io = cmpque_last_used_cmd_io;
    if (cmd_io == NULL) {
      cmd_io = cmdproc_instance_uartcon.cmd_io;
    }

    if(!cmd_io->priv.ed_line.out->inbuf && pxmc_cmpque_bottom_needed  && !cmd_io_line_out(cmd_io)) {
      pxmc_cmpque_bottom();
      if(cmd_io->priv.ed_line.out->inbuf){
        cmd_io_putc(cmd_io, '\r');
        cmd_io_putc(cmd_io, '\n');
        cmd_io_line_out(cmd_io);
      }
    }
 #endif
  }

  ul_list_for_each(cmdproc_instance, &cmdproc_instances, cmdinst) {
   #if 0
    /* ret |= cmd_processor_run(cmdinst->cmd_io, cmdproc_instances.commands); */
   #else
    ret |= cmdproc_run4cmdinst(cmdinst, cmdproc_instances.commands);
   #endif
  }

 #ifndef APPL_WITH_SIM_POSIX
  #ifndef APPL_WITH_DPRINT_PER_CMDIO
  if (!ret && (labs((int)(actual_msec - dbg_prt_last_msec)) > 2000) &&
    !cmd_io_uartcon.priv.ed_line.in->lastch) {
     cmd_io_t *cmd_io = &cmd_io_uartcon;
     dbg_prt_last_msec = actual_msec;

     if (cmd_io->priv.ed_line.io_stack)
       cmd_io = cmd_io->priv.ed_line.io_stack;
     run_dbg_prt(cmd_io);
  }
  #else /*APPL_WITH_DPRINT_PER_CMDIO*/
  if (labs((int)(actual_msec - dbg_prt_last_msec)) > 2000) {
    dbg_prt_last_msec = actual_msec;
    ul_list_for_each(cmdproc_instance, &cmdproc_instances, cmdinst) {
      cmd_io_t *cmd_io = cmdinst->cmd_io;
      if (cmdinst->dbg_prt_flg && !cmd_io->priv.ed_line.in->lastch &&
          !cmd_io_line_out(cmd_io)) {
        cmd_io_t cmd_io_noblock;
        if (cmd_io->priv.ed_line.io_stack)
          cmd_io = cmd_io->priv.ed_line.io_stack;
        cmdproc_setup_nonblock_cmd_io_proxy(&cmd_io_noblock, cmd_io);
        cmd_io = &cmd_io_noblock;
        run_dbg_prt_flg(cmd_io, cmdinst->dbg_prt_flg);
      }
    }
    /*
    if (cmdproc_instance_uart1con.dbg_prt_flg)
      cmd_io_puts(cmd_io_uart1con.priv.ed_line.io_stack,"test 1\r\n");
    else
      cmd_io_puts(cmd_io_uart1con.priv.ed_line.io_stack,"test 0\r\n");
    */
  }
  #endif /*APPL_WITH_DPRINT_PER_CMDIO*/
 #endif /*APPL_WITH_SIM_POSIX*/

  return ret;
}
