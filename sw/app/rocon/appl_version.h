#ifndef _APPL_VERSION_H
#define _APPL_VERSION_H

#include "appl_defs.h"
#include "system_def.h"

#ifndef ULAN_ID
#define ULAN_ID   ROCON
#endif /*ULAN_ID*/

#define APP_VER_ID  "ROCON"
#define APP_VER_MAJOR 0
#define APP_VER_MINOR 9
#define APP_VER_PATCH 9
#define APP_VER_CODE  VER_CODE(APP_VER_MAJOR,APP_VER_MINOR,APP_VER_PATCH)

#endif /*_APPL_VERSION_H*/
