/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  as5_spi.c - AMS AS5048 HAL based angular sensor

  (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <spi_drv.h>
#include <inttypes.h>

#include "as5_spi.h"

const unsigned int as5_spi_cmd2reg[] = {
 [AS5_SPI_CMD_RDANGLE] = AS5_SPI_REG_RDANGLE,
 [AS5_SPI_CMD_RDMAG]   = AS5_SPI_REG_RDMAG,
 [AS5_SPI_CMD_RDDIAG]  = AS5_SPI_REG_RDDIAG,
 [AS5_SPI_CMD_CLRERR]  = AS5_SPI_REG_CLRERR,
};

static int
as5_spi_transfer_prepare(as5_spi_state_t *as5st)
{
  unsigned int val_tx;
  unsigned int cmd;
  unsigned int rq_mask;

  if (as5st->chan_cnt >= as5st->chan_num) {
    as5st->rq_pend = 0;
    as5st->chan_cnt = 0;
  }

  as5st->spi_msg.addr = as5st->chan_addr[as5st->chan_cnt];

  cmd = AS5_SPI_CMD_RDANGLE;
  rq_mask = as5st->chan[as5st->chan_cnt].rq_mask;
  if (rq_mask) {
    if (rq_mask & AS5_SPI_MASK_CLRERR)
      cmd = AS5_SPI_CMD_CLRERR;
    else if (rq_mask & AS5_SPI_MASK_RDMAG)
      cmd = AS5_SPI_CMD_RDMAG;
    else if (rq_mask & AS5_SPI_MASK_RDDIAG)
      cmd = AS5_SPI_CMD_RDDIAG;
  }
  as5st->chan[as5st->chan_cnt].act_cmd = cmd;
  val_tx = as5_spi_cmd2reg[cmd];

  as5st->tx_buff[0] = val_tx;
  as5st->tx_buff[1] = val_tx >> 8;

  if(spi_msg_rq_ins(as5st->spi_drv, &as5st->spi_msg) < 0) {
    as5st->msg_inpr = -1;
    return -1;
  }

  return 1;
}

static int
as5_spi_transfer_callback(struct spi_drv *drv, int code, struct spi_msg_head *msg)
{
  as5_spi_state_t *as5st = UL_CONTAINEROF(msg, as5_spi_state_t, spi_msg);
  unsigned int val;
  unsigned char par;

  par = as5st->rx_buff[0] ^ as5st->rx_buff[1];
  par = par ^ (par >> 4);
  par = par ^ (par >> 2);
  par = par ^ (par >> 1);
  par &= 1;
  val = (as5st->rx_buff[0] << 0) | (as5st->rx_buff[1] << 8);
  val &= ~AS5_SPI_REPLY_PAR;

  if (val & AS5_SPI_REPLY_ERR) {
    as5st->chan[as5st->chan_cnt].err_cnt++;
    as5st->chan[as5st->chan_cnt].rq_mask |= AS5_SPI_MASK_CLRERR;
  } else if (!par) {
    switch (as5st->chan[as5st->chan_cnt].prev_cmd) {
      case AS5_SPI_CMD_RDANGLE:
        as5st->chan[as5st->chan_cnt].pos = val;
        break;
      case AS5_SPI_CMD_CLRERR:
        as5st->chan[as5st->chan_cnt].err = val;
        as5st->chan[as5st->chan_cnt].rq_mask &= ~AS5_SPI_MASK_CLRERR;
        break;
      case AS5_SPI_CMD_RDMAG:
        as5st->chan[as5st->chan_cnt].mag = val;
        as5st->chan[as5st->chan_cnt].rq_mask &= ~AS5_SPI_MASK_RDMAG;
        break;
      case AS5_SPI_CMD_RDDIAG:
        as5st->chan[as5st->chan_cnt].diag = val;
        as5st->chan[as5st->chan_cnt].rq_mask &= ~AS5_SPI_MASK_RDDIAG;
        break;
    }
  }
  as5st->chan[as5st->chan_cnt].prev_cmd = as5st->chan[as5st->chan_cnt].act_cmd;

  as5st->chan_cnt++;
  if ((as5st->chan_cnt >= as5st->chan_num) && !as5st->rq_pend) {
    as5st->msg_inpr = 0;
    return 1;
  }

  as5_spi_transfer_prepare(as5st);

  return 1;
}

int
as5_spi_state_setup(as5_spi_state_t *as5st, spi_drv_t *spi_drv)
{
  as5st->spi_msg.flags = SPI_MSG_MODE_SET;
  as5st->spi_msg.size_mode = SPI_MODE_1 | SPI_MODE_16BIT;
  as5st->spi_msg.addr = 0;
  as5st->spi_msg.rq_len = 2;
  as5st->spi_msg.tx_buf = as5st->tx_buff;
  as5st->spi_msg.rx_buf = as5st->rx_buff;
  as5st->spi_msg.callback = as5_spi_transfer_callback;
  as5st->spi_msg.private = 0;
  as5st->chan_cnt = 0;

  as5st->spi_drv = spi_drv;

  return 0;
}

static int
as5_spi_transfer_request(as5_spi_state_t *as5st)
{
  unsigned long flags;
  int ready_fl;

  save_and_cli(flags);
  ready_fl = !as5st->msg_inpr;
  if (!as5st->msg_inpr) {
    as5st->msg_inpr = 1;
  } else {
    as5st->rq_pend = 1;
  }
  restore_flags(flags);

  if (!ready_fl)
    return 0;

  return as5_spi_transfer_prepare(as5st);
}

int
as5_spi_add2rq_mask(as5_spi_state_t *as5st, int chan, unsigned int rq_mask)
{
  unsigned long flags;
  if ((chan >= as5st->chan_num) || (chan < 0))
    return -1;

  save_and_cli(flags);
  as5st->chan[chan].rq_mask |= rq_mask;
  restore_flags(flags);
  return 0;
}

/*******************************************************************/

#define AS5_SPI_CHANNELS 1
#define AS5_SPI_NUMBER   1

unsigned int as5_spi_chan_addr[AS5_SPI_CHANNELS] = {
  1,
};

as5_chan_state_t as5_chan_state[AS5_SPI_CHANNELS];

as5_spi_state_t as5_spi_state = {
  .chan_num = AS5_SPI_CHANNELS,
  .chan = as5_chan_state,
  .chan_addr = as5_spi_chan_addr
};

int as5_spi_initialized;

int
as5_spi_init(void)
{
  spi_drv_t *spi_drv;
  as5_spi_state_t *as5st = &as5_spi_state;

  if (as5_spi_initialized <= 0) {

    spi_drv = spi_find_drv(NULL, AS5_SPI_NUMBER);
    if (spi_drv == NULL)
      return -1;

    as5_spi_state_setup(as5st, spi_drv);

    as5_spi_initialized = 1;
  }

  return 0;
}

int
as5_spi_pos_update(void)
{
  as5_spi_state_t *as5st = &as5_spi_state;
  if (as5st->spi_drv == NULL)
    return -1;
  return as5_spi_transfer_request(as5st);
}

unsigned int as5_spi_get_pos(int chan)
{
  as5_spi_state_t *as5st = &as5_spi_state;
  if ((chan >= as5st->chan_num) || (chan < 0))
    return 0;

  return as5st->chan[chan].pos;
}

