/*******************************************************************
  Motion and Robotic System (MARS) aplication components

  appl_coordmv.c - application specific coordinated
                   motions control support

  Copyright (C) 2001-2014 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2014 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <pxmc.h>
#include <pxmc_coordmv.h>
#include <cmd_proc.h>
#include <pxmc_cmds.h>
#include <utils.h>
#include <string.h>

#include "appl_defs.h"

long cmd_time_mask=0xffff;
long cmd_coordmv_sqn_mask=0xffff;
long cmd_coordmv_par_bits=24;

int pxmc_coordmv_process(void)
{
  /* coordinator bottom handling */
  if (pxmc_coordmv_state.mcs_flg & MCS_BOTT_m){
    pxmc_coordmv_bottom(&pxmc_coordmv_state);
  }

  return 0;
}

int cmd_do_coordmv(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state = &pxmc_coordmv_state;
  int con_cnt = mcs_state->mcs_con_cnt;
  int i;
  long final[con_cnt], val;
  long mintim = 0;
  char *s;
  pxmc_state_t *mcs;
  if(*param[2]!=':') return -CMDERR_OPCHAR;
  s = param[3];
  if ((long)(des->info[1]) & 1) {	/* coordinated movement with time */
    if (si_long(&s, &mintim, 10) <0 ) return -CMDERR_BADPAR;
    if (mintim < 0) return -CMDERR_BADPAR;
    if (si_fndsep(&s,",") <=0 ) return -CMDERR_BADSEP;
  }
  if (!con_cnt) return -CMDERR_BADCFG;
  i = 0;
  while (1) {
    if(si_long(&s, &val, 10) <0 ) return -CMDERR_BADPAR;
    mcs = pxmc_coordmv_indx2mcs(mcs_state, i);
    if(!mcs) return -CMDERR_BADREG;
    final[i] = val << PXMC_SUBDIV(mcs);
    if(++i >= con_cnt) break;
    if(si_fndsep(&s, ",") <= 0) return -CMDERR_BADSEP;
  }
  si_skspace(&s);
  if(*s) return -CMDERR_GARBAG;

  if ((long)(des->info[1]) & 2){
    if (pxmc_coordmv_relmv(mcs_state, con_cnt, final, mintim)<0)
      return -1;
  } else {
    if (pxmc_coordmv_absmv(mcs_state, con_cnt, final, mintim)<0)
      return -1;
  }
  return 0;
}


int cmd_do_coordspline(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state = &pxmc_coordmv_state;
  int con_cnt = mcs_state->mcs_con_cnt;
  long spline_param[con_cnt * PXMC_SPLINE_ORDER_MAX];
  int i;
  int o;
  int a;
  long val;
  long mintim=0;
  long order;
  char *s;
  pxmc_state_t *mcs;
  if (*param[2] != ':') return -CMDERR_OPCHAR;
  s = param[3];
  if ((long)(des->info[1]) & 1) {	/* coordinated movement with time */
    if (si_long(&s,&mintim, 10)<0) return -CMDERR_BADPAR;
    if (mintim<0) return -CMDERR_BADPAR;
    if (si_fndsep(&s, ",")<=0) return -CMDERR_BADSEP;
  }
  if (!con_cnt) return -CMDERR_BADCFG;

  if (si_long(&s,&order, 10) <0 ) return -CMDERR_BADPAR;
  if (mintim <0 ) return -CMDERR_BADPAR;
  if (si_fndsep(&s, ",") <= 0) return -CMDERR_BADSEP;

  if ((order <= 0) || (order > PXMC_SPLINE_ORDER_MAX))
    return -CMDERR_BADPAR;

  i=0; o=0; a=0;
  while (1) {
    if (si_long(&s, &val, 10) < 0) return -CMDERR_BADPAR;
    mcs = pxmc_coordmv_indx2mcs(mcs_state, a);
    if (!mcs) return -CMDERR_BADREG;
    spline_param[i] = val << PXMC_SUBDIV(mcs);;
    i++;
    if (++o >= order) {
      o = 0;
      if (++a >= con_cnt) break;
    }
    if (si_fndsep(&s, ",") <= 0) return -CMDERR_BADSEP;
  }
  si_skspace(&s);
  if(*s) return -CMDERR_GARBAG;

  if (pxmc_coordmv_spline(mcs_state, i, spline_param, order, mintim) < 0)
    return -1;
  return 0;
}


int cmd_do_coordgrp(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state = &pxmc_coordmv_state;
  pxmc_state_list_t *reg_list = mcs_state->mcs_con_list;
  int reg_max = reg_list->pxml_cnt;
  int ret;
  unsigned chan;
  int con_indx[reg_max];
  int con_cnt=0;
  char *s;
  if (*param[2] != ':') return -CMDERR_OPCHAR;
  s = param[3];
  do{
    si_skspace(&s);
    if (!*s) break;
    chan = *(s++) - 'A';
    if (chan >= reg_max) return -CMDERR_BADREG;
    con_indx[con_cnt] = chan;
    con_cnt++;
    if ((ret = si_fndsep(&s, ",")) < 0) return -CMDERR_BADSEP;
    if (ret == 0) break;
  } while (1);

  return pxmc_coordmv_grp(mcs_state, con_cnt, con_indx);
}

int cmd_do_coordgetpos(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  pxmc_coordmv_state_t *mcs_state=&pxmc_coordmv_state;
  int con_cnt=mcs_state->mcs_con_cnt;
  pxmc_coordmv_seg_t *mcseg;
  unsigned long msec_time_a;
  unsigned long msec_time_b = 0;
  int is_iddle;
  unsigned long seg_sqn_a;
  unsigned long seg_sqn_b = 0;
  unsigned long seg_par_a;
  unsigned long seg_par_b = 0;
  long val_array_a[con_cnt*2];
  long val_array_b[con_cnt*2];
  long *p;
  int try_cnt = 0;
  int query_mode = (int)des->info[1];
  char str[20];

  if(*param[2]!='?') return -CMDERR_OPCHAR;

  do {
    __memory_barrier();
    lt_mstime_update();
    mcseg = mcs_state->mcs_seg_inpr;
    msec_time_a = actual_msec;
    __memory_barrier();
    if (mcseg!=NULL) {
      seg_sqn_a = mcseg->mcseg_sqn;
      seg_par_a = mcs_state->mcs_par;
      is_iddle = 0;
    } else {
      seg_sqn_a = mcs_state->mcs_next_sqn;
      seg_par_a = 0;
      is_iddle = 1;
    }
    if (query_mode & 1)
      seg_par_a = mcs_state->mcs_next_sqn;
    if (try_cnt & 1)
      p = val_array_a;
    else
      p = val_array_b;
    pxmc_coordmv_foreach_get_pos(mcs_state, p, con_cnt,
                                  (unsigned int)des->info[0], 1);
    if (query_mode & 2)
      pxmc_coordmv_foreach_get_long(mcs_state, p + con_cnt, con_cnt,
                                    pxmc_state_offs(pxms_as));
    if (try_cnt++) {
      if ((msec_time_b == msec_time_a) &&
          (seg_sqn_b == seg_sqn_a) &&
          (seg_par_b == seg_par_a)) {
        long *pa = val_array_a;
        long *pb = val_array_b;
        int cnt = con_cnt;
        if (query_mode & 2)
          cnt *= 2;
        while(cnt && (*(pa++) == *(pb++))) {
          cnt--;
        }
        if (!cnt)
          break;
      }
    }

    msec_time_b = msec_time_a;
    seg_sqn_b = seg_sqn_a;
    seg_par_b = seg_par_a;
  } while(try_cnt<3);

  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  i2str(str, msec_time_a & cmd_time_mask, 0, 0);
  cmd_io_write(cmd_io, str, strlen(str));
  cmd_io_putc(cmd_io,',');
  i2str(str, seg_sqn_a & cmd_coordmv_sqn_mask, 0, 0);
  cmd_io_write(cmd_io, str, strlen(str));
  cmd_io_putc(cmd_io,',');
  if (query_mode & 1) {
    seg_par_a &= cmd_coordmv_sqn_mask;
    i2str(str, seg_par_a, 0, 0);
  } else {
    seg_par_a >>= 31 - cmd_coordmv_par_bits;
    if (!is_iddle)
      i2str(str, seg_par_a, 0, 0);
    else
      i2str(str, -1, 0, 0);
  }
  cmd_io_write(cmd_io, str, strlen(str));
  {
    int cnt = con_cnt;
    if (query_mode & 2)
      cnt *= 2;
    while (cnt--) {
      cmd_io_putc(cmd_io,',');
      i2str(str, *(p++), 0, 0);
      cmd_io_write(cmd_io, str, strlen(str));
    }
  }

  return 0;
}

int cmd_do_coordmv_queue_max(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  int opchar;
  char *s;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar==':'){
    s = param[3];
    if (si_long(&s,&val, 10)<0) return -CMDERR_BADPAR;
    si_skspace(&s);
    if(*s) return -CMDERR_GARBAG;
    if (val < 100)
      val = 100;
    if (val > 30000)
      val = 30000;
    if (val - 10 < pxmc_coordmv_state.mcs_seg_warn)
      pxmc_coordmv_state.mcs_seg_warn = val - 10;
    pxmc_coordmv_state.mcs_seg_max=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, pxmc_coordmv_state.mcs_seg_max, 0, 0);
  }
  return 0;
}

int cmd_do_coordmv_queue_warn(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  int opchar;
  char *s;

  if((opchar=cmd_opchar_check(cmd_io,des,param))<0) return opchar;
  if(opchar==':'){
    s = param[3];
    if (si_long(&s,&val, 10)<0) return -CMDERR_BADPAR;
    si_skspace(&s);
    if(*s) return -CMDERR_GARBAG;
    if (val < 10)
      val = 10;
    if (val + 10 > pxmc_coordmv_state.mcs_seg_max)
      val = pxmc_coordmv_state.mcs_seg_max - 10;
    pxmc_coordmv_state.mcs_seg_warn=val;
  }else{
    return cmd_opchar_replong(cmd_io, param, pxmc_coordmv_state.mcs_seg_warn, 0, 0);
  }
  return 0;
}

cmd_des_t const cmd_des_coordmv={0, CDESM_OPCHR|CDESM_WR,
			"COORDMV","initiate coordinated movement to point f1,f2,...",cmd_do_coordmv,
			{0,0}};
cmd_des_t const cmd_des_coordmvt={0, CDESM_OPCHR|CDESM_WR,
			"COORDMVT","coord. movement with time to point mintime,f1,f2,...",cmd_do_coordmv,
			{0,(char*)1}};
cmd_des_t const cmd_des_coordrelmvt={0, CDESM_OPCHR|CDESM_WR,
			"COORDRELMVT","coord. relative movement with time to point mintime,f1,f2,...",
			cmd_do_coordmv,
			{0,(char*)3}};
cmd_des_t const cmd_des_coordsplinet={0, CDESM_OPCHR|CDESM_WR,
			"COORDSPLINET","coord. spline movement with time to point mintime,order,a11,a12,...,a21,..",
			cmd_do_coordspline,
			{0,(char*)1}};
cmd_des_t const cmd_des_coordgrp={0, CDESM_OPCHR|CDESM_WR,
			"COORDGRP","group axes for COORDMV, for ex. C,D,F",cmd_do_coordgrp,
			{0,0}};
cmd_des_t const cmd_des_coorddiscont={0, CDESM_OPCHR|CDESM_RW,
			"COORDDISCONT","max relative discontinuity between segs",cmd_do_rw_long,
			{(char*)&pxmc_coordmv_state.mcs_disca,
			 0}};
cmd_des_t const cmd_des_coordgetap={0, CDESM_OPCHR|CDESM_RD,
			"COORDAP","report actual position, format msec_time,seg_sqn,seg_par,p1,p2,...",cmd_do_coordgetpos,
			{(void*)pxmc_state_offs(pxms_ap),0}};
cmd_des_t const cmd_des_coordgetaps={0, CDESM_OPCHR|CDESM_RD,
			"COORDAPS","report actual position and speed, format msec_time,seg_sqn,seg_par,p1,p2,...,s1,s2,...",cmd_do_coordgetpos,
			{(void*)pxmc_state_offs(pxms_ap),(void*)2}};
cmd_des_t const cmd_des_coordgetrp={0, CDESM_OPCHR|CDESM_RD,
			"COORDRP","report requestes position, format msec_time,seg_sqn,seg_par,p1,p2,...",cmd_do_coordgetpos,
			{(void*)pxmc_state_offs(pxms_rp),0}};
cmd_des_t const cmd_des_coordgetep={0, CDESM_OPCHR|CDESM_RD,
			"COORDEP","report planed end position, format msec_time,next_sqn,seg_par,p1,p2,...",cmd_do_coordgetpos,
			{(void*)pxmc_state_offs(pxms_ep),(void*)1}};
cmd_des_t const cmd_des_time_mask={0, CDESM_OPCHR|CDESM_RW,
			"CMDTIMEMASK","mask to reduce reported time",cmd_do_rw_long,
			{(char*)&cmd_time_mask,
			 0}};
cmd_des_t const cmd_des_coordmv_sqn_mask={0, CDESM_OPCHR|CDESM_RW,
			"COORDSQNMASK","mask to reduce reported time",cmd_do_rw_long,
			{(char*)&cmd_coordmv_sqn_mask,
			 0}};
cmd_des_t const cmd_des_coordmv_par_bits={0, CDESM_OPCHR|CDESM_RW,
			"COORDPARBITS","bits reported as parametr unit",cmd_do_rw_long,
			{(char*)&cmd_coordmv_par_bits,
			 0}};
cmd_des_t const cmd_des_coordmv_queue_max={0, CDESM_OPCHR|CDESM_RW,
                        "COORDQUEMAX","capacity of queue for segments",cmd_do_coordmv_queue_max,
                        {0,0}};
cmd_des_t const cmd_des_coordmv_queue_warn={0, CDESM_OPCHR|CDESM_RW,
                        "COORDQUWARN","capacity of queue for segments",cmd_do_coordmv_queue_warn,
                        {0,0}};

cmd_des_t const *const cmd_pxmc_coordmv[]={
  &cmd_des_coordmv,
  &cmd_des_coordmvt,
  &cmd_des_coordrelmvt,
  &cmd_des_coordsplinet,
  &cmd_des_coordgrp,
  &cmd_des_coorddiscont,
  &cmd_des_coordgetap,
  &cmd_des_coordgetaps,
  &cmd_des_coordgetrp,
  &cmd_des_coordgetep,
  &cmd_des_time_mask,
  &cmd_des_coordmv_sqn_mask,
  &cmd_des_coordmv_par_bits,
  &cmd_des_coordmv_queue_max,
  &cmd_des_coordmv_queue_warn,
  NULL
};
