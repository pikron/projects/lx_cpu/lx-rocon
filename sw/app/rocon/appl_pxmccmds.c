/*******************************************************************
  Motion and Robotic System (MARS) aplication components.

  appl_pxmccmds.c - position controller RoCoN specific commands

  Copyright (C) 2001-2013 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2013 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <system_def.h>
#include <pxmc.h>
#include <stdlib.h>
#include <string.h>
#include "pxmc_cmds.h"

#include "appl_defs.h"
#include "appl_fpga.h"
#include "appl_pxmc.h"


int cmd_do_irc_read(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  pxmc_state_t *mcs;
  int chan;

  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;

  if(*param[2]!='?')  return -CMDERR_OPCHAR;

  chan=mcs->pxms_inp_info;
  val=pxmc_rocon_read_irc(chan);

  return cmd_opchar_replong(cmd_io, param, val, 0, 0);
}

int cmd_do_irc_index_read(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  long val;
  pxmc_state_t *mcs;
  int chan;
  unsigned idx_couter;
  char str[20];
  int clear_event=(int)(intptr_t)des->info[0];

  if((mcs=cmd_opchar_getreg(cmd_io,des,param))==NULL) return -CMDERR_BADREG;

  if(*param[2]!='?')  return -CMDERR_OPCHAR;

  chan=mcs->pxms_inp_info;
  val=pxmc_rocon_read_irc_index(chan, &idx_couter, clear_event);

  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io, '=');

  i2str(str, val, 0, 0);
  cmd_io_write(cmd_io,str,strlen(str));
  cmd_io_putc(cmd_io, ',');
  i2str(str, idx_couter, 0, 0);
  cmd_io_write(cmd_io,str,strlen(str));

  return 0;
}

cmd_des_t const cmd_des_irc_read={0, CDESM_OPCHR|CDESM_RD,
                        "IRC?","direct read of IRC counter for given axis",
                        cmd_do_irc_read,
                        {0,0}};

cmd_des_t const cmd_des_irc_index_read={0, CDESM_OPCHR|CDESM_RD,
                        "IRCIDX?","axis captured IRC value at index",
                        cmd_do_irc_index_read,
                        {(void*)0,0}};

cmd_des_t const cmd_des_irc_index_read_and_clear={0, CDESM_OPCHR|CDESM_RD,
                        "IRCIDXCLR?", "axis captured IRC value at index with event clear",
                        cmd_do_irc_index_read,
                        {(void*)1,0}};

cmd_des_t const cmd_des_regcurdp={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURDP?","current controller d component p parameter", cmd_do_reg_short_val,
                        {(char*)pxmc_rocon_state_offs(cur_d_p),
                         0}};

cmd_des_t const cmd_des_regcurdi={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURDI?","current controller d component i parameter", cmd_do_reg_short_val,
                        {(char*)pxmc_rocon_state_offs(cur_d_i),
                         0}};

cmd_des_t const cmd_des_regcurqp={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURQP?","current controller q component p parameter", cmd_do_reg_short_val,
                        {(char*)pxmc_rocon_state_offs(cur_q_p),
                         0}};

cmd_des_t const cmd_des_regcurqi={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURQI?","current controller q component i parameter", cmd_do_reg_short_val,
                        {(char*)pxmc_rocon_state_offs(cur_q_i),
                         0}};

cmd_des_t const cmd_des_regcurhold={0, CDESM_OPCHR|CDESM_RW,
                        "REGCURHOLD?","current steady hold value for stepper", cmd_do_reg_short_val,
                        {(char*)pxmc_rocon_state_offs(cur_hold),
                         0}};

cmd_des_t const cmd_des_vin={0, CDESM_OPCHR|CDESM_RD,
                        "VIN","read actual input voltage value", cmd_do_rw_int,
                        {(char*)&pxmc_rocon_vin_act,
                         0}};


cmd_des_t const *cmd_appl_pxmc[] =
{
  &cmd_des_irc_read,
  &cmd_des_irc_index_read,
  &cmd_des_irc_index_read_and_clear,
  &cmd_des_regcurdp,
  &cmd_des_regcurdi,
  &cmd_des_regcurqp,
  &cmd_des_regcurqi,
  &cmd_des_regcurhold,
  &cmd_des_vin,
  NULL
};
