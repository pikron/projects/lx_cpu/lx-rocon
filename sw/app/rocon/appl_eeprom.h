#ifndef _APPL_EEPROM_H
#define _APPL_EEPROM_H

#include <string.h>

#include "appl_defs.h"

#ifdef CONFIG_OC_I2C_DRV_SYSLESS
#include <i2c_drv.h>

typedef struct appl_eeprom_chip_context_t
{
  unsigned char opstate;
  i2c_drv_t *i2c_drv;
  unsigned char i2c_addr;
  i2c_msg_head_t i2c_msg;
  unsigned char *rxtx_buf;
  unsigned short page_size;
  volatile signed char tx_inpr;
  volatile unsigned char retry_cnt;
  void *data_buf;
  size_t data_start;
  size_t data_len;
  size_t pos;
  int (*finish_callback)(struct appl_eeprom_chip_context_t *chip, void *context, int result);
  void *callback_context;
} appl_eeprom_chip_context_t;

enum appl_eeprom_st
{
  APPL_EEPROM_ST_IDLE      = 0,
  APPL_EEPROM_ST_START     = 1,
  APPL_EEPROM_ST_WRDATA    = 2,
  APPL_EEPROM_ST_WRHEADER  = 3,
};

int appl_eeprom_chip_init(appl_eeprom_chip_context_t *chip);

int appl_eeprom_chip_load(appl_eeprom_chip_context_t *chip, unsigned int start_addr,
                          void *data, unsigned int len);

int appl_eeprom_chip_transfer(appl_eeprom_chip_context_t *chip,
                              unsigned int start_addr, const void *data, unsigned int len);

int appl_eeprom_chip_process(appl_eeprom_chip_context_t *chip);

int appl_eeprom_chip_write_transfer_setup(appl_eeprom_chip_context_t *chip,
    void *buf, size_t start_addr, size_t len,
    int (*finish_cb)(struct appl_eeprom_chip_context_t *chip, void *context, int result),
    void *ctx);

#endif /*CONFIG_OC_I2C_DRV_SYSLESS*/

#endif /*_APPL_EEPROM_H*/
