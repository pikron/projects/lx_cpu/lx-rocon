#ifndef _APPL_FPGA_H
#define _APPL_FPGA_H

#include "appl_defs.h"

/* Register defines */

/* Tumbl */

#define FPGA_TUMBL_IMEM_BASE      0x80000000
#define FPGA_TUMBL_DMEM_BASE      0x80001000
#define FPGA_TUMBL_CONTROL_REG    0x80003000
#define FPGA_TUMBL_TRACE_KICK_REG 0x80003004
#define FPGA_TUMBL_PC             0x80003008

#define FPGA_TUMBL_CONTROL_REG_RESET_BIT 0x01
#define FPGA_TUMBL_CONTROL_REG_INT_BIT   0x02
#define FPGA_TUMBL_CONTROL_REG_HALT_BIT  0x04
#define FPGA_TUMBL_CONTROL_REG_TRACE_BIT 0x08

extern volatile uint32_t *fpga_tumbl_control;
extern volatile uint32_t *fpga_tumbl_trace_kick;
extern volatile uint32_t *fpga_tumbl_pc;
extern volatile uint32_t *fpga_tumbl_imem;
extern volatile uint32_t *fpga_tumbl_dmem;

/* IRC */

#define FPGA_IRC0_BASE    0x80022000
#define FPGA_IRC1_BASE    0x80022008
#define FPGA_IRC2_BASE    0x80022010
#define FPGA_IRC3_BASE    0x80022018
#define FPGA_IRC4_BASE    0x80022020
#define FPGA_IRC5_BASE    0x80022028
#define FPGA_IRC6_BASE    0x80022030
#define FPGA_IRC7_BASE    0x80022038

#define FPGA_IRC_RESET    0x80022044

struct irc_register
{
  int32_t count;
  int32_t count_index;
};

extern volatile struct irc_register *fpga_irc1;
extern volatile struct irc_register *fpga_irc2;
extern volatile struct irc_register *fpga_irc3;
extern volatile struct irc_register *fpga_irc4;
extern volatile struct irc_register *fpga_irc5;
extern volatile struct irc_register *fpga_irc6;
extern volatile struct irc_register *fpga_irc7;
extern volatile struct irc_register *fpga_irc8;

extern volatile struct irc_register *fpga_irc[8];

/* Masks */
#define FPGA_IRC_STATE_MARK_MASK              0x00000001
#define FPGA_IRC_STATE_AB_ERROR_MASK          0x00000002
#define FPGA_IRC_STATE_INDEX_EVENT_MASK       0x00000004
#define FPGA_IRC_STATE_INDEX_MASK             0x00000008

#define FPGA_IRC_STATE_RESET_AB_ERROR_MASK    0x00000002
#define FPGA_IRC_STATE_RESET_INDEX_EVENT_MASK 0x00000004

extern volatile uint8_t *fpga_irc1_state;
extern volatile uint8_t *fpga_irc2_state;
extern volatile uint8_t *fpga_irc3_state;
extern volatile uint8_t *fpga_irc4_state;
extern volatile uint8_t *fpga_irc6_state;
extern volatile uint8_t *fpga_irc5_state;
extern volatile uint8_t *fpga_irc7_state;
extern volatile uint8_t *fpga_irc8_state;

extern volatile uint8_t *fpga_irc_state[8];

extern volatile uint8_t *fpga_irc_reset;

/* LX Master */

#if 0 /* FPGA design version 2 */
#define FPGA_LX_MASTER_TRANSMITTER_BASE   0x80023000
#define FPGA_LX_MASTER_RECEIVER_BASE      0x80023804
#define FPGA_LX_MASTER_RESET              0x80023800
#else /* FPGA design version 3 */
#define FPGA_LX_MASTER_TRANSMITTER_BASE   0x80024000
#define FPGA_LX_MASTER_RECEIVER_BASE      0x80024800
#define FPGA_LX_MASTER_RESET              0x80025000
#endif
#define FPGA_LX_MASTER_TRANSMITTER_REG    0x80025004
#define FPGA_LX_MASTER_TRANSMITTER_WDOG   0x80025008
#define FPGA_LX_MASTER_TRANSMITTER_CYCLE  0x8002500C
#define FPGA_LX_MASTER_RECEIVER_REG       0x80025010
#define FPGA_LX_MASTER_RECEIVER_DONE_DIV  0x80025014

extern volatile uint32_t *fpga_lx_master_transmitter_base;
extern volatile uint32_t *fpga_lx_master_transmitter_reg;
extern volatile uint32_t *fpga_lx_master_transmitter_cycle;
extern volatile uint32_t *fpga_lx_master_transmitter_wdog;
extern volatile uint32_t *fpga_lx_master_receiver_base;
extern volatile uint32_t *fpga_lx_master_receiver_reg;
extern volatile uint32_t *fpga_lx_master_reset;
extern volatile uint32_t *fpga_lx_master_conf;

extern volatile uint32_t *fpga_lx_master_transmitter_control_reg;
extern volatile uint32_t *fpga_lx_master_receiver_control_reg;
extern volatile uint32_t *fpga_lx_master_receiver_done_div;

#define FPGA_LX_MASTER_CONTROL_ADDRESS_MASK     0x0000FF00
#define FPGA_LX_MASTER_CONTROL_DATA_LENGTH_MASK 0x000000FF

/* Function approximation block */

#define FPGA_FNCAPPROX_BASE               0x80023000

extern volatile uint32_t *fpga_fncapprox_base;

/* Configuration defines */

#define FPGA_CONFIGURATION_FILE_ADDRESS 0xA1C00000

#define FPGA_CONF_SUCESS              0
#define FPGA_CONF_ERR_RECONF_LOCKED   1
#define FPGA_CONF_ERR_RESET_FAIL      2
#define FPGA_CONF_ERR_WRITE_ERR       3
#define FPGA_CONF_ERR_CRC_ERR         4

int fpga_tumbl_set_reset(int reset);
int fpga_tumbl_set_halt(int halt);
int fpga_tumbl_set_trace(int trace);
int fpga_tumbl_kick_trace();

void fpga_tumbl_write(unsigned int offset, unsigned char *ptr, int len);

int (*fpga_reconfiguaration_initiated)(void);
int (*fpga_reconfiguaration_finished)(void);

void fpga_init();
int fpga_configure();
int fpga_measure_bus_read();
int fpga_measure_bus_write();
void fpga_set_reconfiguration_lock(int lock);
int fpga_get_reconfiguration_lock();

#endif /*_APPL_FPGA_H*/
