#include <types.h>
#include <cpu_def.h>
#include <system_def.h>
#include <hal_gpio.h>

#include <gen_dio.h>
#include <appl_pxmc.h>

#include "appl_defs.h"
#include "appl_dio.h"

#define DIO_O0_PIN          PORT_PIN(5,4,PORT_CONF_GPIO_OUT_HI)
#define DIO_O1_PIN          PORT_PIN(1,30,PORT_CONF_GPIO_OUT_HI)
#define DIO_O2_PIN          PORT_PIN(5,1,PORT_CONF_GPIO_OUT_HI)
#define DIO_O3_PIN          PORT_PIN(5,0,PORT_CONF_GPIO_OUT_HI)
#define DIO_O13_PIN         PORT_PIN(0,21,PORT_CONF_GPIO_OUT_HI)
#define DIO_O14_PIN         PORT_PIN(5,2,PORT_CONF_GPIO_OUT_HI)

#define DIO_I2_PIN          PORT_PIN(2,3,PORT_CONF_GPIO_IN)
#define DIO_I3_PIN          PORT_PIN(2,1,PORT_CONF_GPIO_IN)
#define DIO_I4_PIN          PORT_PIN(1,19,PORT_CONF_GPIO_IN)
#define DIO_I5_PIN          PORT_PIN(1,18,PORT_CONF_GPIO_IN)
#define DIO_I6_PIN          PORT_PIN(0,26,PORT_CONF_GPIO_IN)
#define DIO_I7_PIN          PORT_PIN(0,14,PORT_CONF_GPIO_IN)

//#define DIO_I7_PIN          PORT_PIN(5,3,PORT_CONF_GPIO_IN)

static const unsigned dio_pin_setup[] = {
  DIO_O0_PIN,
  DIO_O1_PIN,
  DIO_O2_PIN,
  DIO_O3_PIN,
  DIO_O13_PIN,
  DIO_O14_PIN,
  DIO_I2_PIN,
  DIO_I3_PIN,
  DIO_I4_PIN,
  DIO_I5_PIN,
  DIO_I6_PIN,
  DIO_I7_PIN,
};

unsigned dio_get_rocon(struct dio_hw_des* des)
{
  unsigned val = 0;
  val |= pxmc_inp_rocon_is_mark_direct(0) << 0;
  val |= pxmc_inp_rocon_is_mark_direct(1) << 1;
  val |= hal_gpio_get_value(DIO_I2_PIN) << 2;
  val |= hal_gpio_get_value(DIO_I3_PIN) << 3;
  val |= hal_gpio_get_value(DIO_I4_PIN) << 4;
  val |= hal_gpio_get_value(DIO_I5_PIN) << 5;
  val |= hal_gpio_get_value(DIO_I6_PIN) << 6;
  val |= hal_gpio_get_value(DIO_I7_PIN) << 7;
  return val;
}

void dio_set_rocon(struct dio_hw_des* des, unsigned val)
{
  hal_gpio_set_value(DIO_O0_PIN,!(val & (1 << 0)));
  hal_gpio_set_value(DIO_O1_PIN,!(val & (1 << 1)));
  hal_gpio_set_value(DIO_O2_PIN,!(val & (1 << 2)));
  hal_gpio_set_value(DIO_O3_PIN,!(val & (1 << 3)));

  hal_gpio_set_value(DIO_O13_PIN,!(val & (1 << 13)));
  hal_gpio_set_value(DIO_O14_PIN,!(val & (1 << 14)));
}

void dio_mod_rocon(struct dio_hw_des* des, unsigned  mask, unsigned  xor)
{
  unsigned invert = ~mask & xor;

  if (mask) {
    if (mask & (1 << 0))
      hal_gpio_set_value(DIO_O0_PIN,!(xor & (1 << 0)));
    if (mask & (1 << 1))
      hal_gpio_set_value(DIO_O1_PIN,!(xor & (1 << 1)));
    if (mask & (1 << 2))
      hal_gpio_set_value(DIO_O2_PIN,!(xor & (1 << 2)));
    if (mask & (1 << 3))
      hal_gpio_set_value(DIO_O3_PIN,!(xor & (1 << 3)));

    if (mask & (1 << 13))
      hal_gpio_set_value(DIO_O13_PIN,!(xor & (1 << 13)));
    if (mask & (1 << 14))
      hal_gpio_set_value(DIO_O14_PIN,!(xor & (1 << 14)));
  }
  if (invert) {
    if (xor & (1 << 0))
      hal_gpio_set_value(DIO_O0_PIN,!hal_gpio_get_value(DIO_O0_PIN));
    if (xor & (1 << 1))
      hal_gpio_set_value(DIO_O1_PIN,!hal_gpio_get_value(DIO_O1_PIN));
    if (xor & (1 << 2))
      hal_gpio_set_value(DIO_O2_PIN,!hal_gpio_get_value(DIO_O2_PIN));
    if (xor & (1 << 3))
      hal_gpio_set_value(DIO_O3_PIN,!hal_gpio_get_value(DIO_O3_PIN));

    if (xor & (1 << 13))
      hal_gpio_set_value(DIO_O13_PIN,!hal_gpio_get_value(DIO_O13_PIN));
    if (xor & (1 << 14))
      hal_gpio_set_value(DIO_O14_PIN,!hal_gpio_get_value(DIO_O14_PIN));
  }
}

unsigned dio_backrd_rocon(struct dio_hw_des* des)
{
  unsigned val = 0;
  val |= (hal_gpio_get_value(DIO_O0_PIN) ^ 1) << 0;
  val |= (hal_gpio_get_value(DIO_O1_PIN) ^ 1) << 1;
  val |= (hal_gpio_get_value(DIO_O2_PIN) ^ 1) << 2;
  val |= (hal_gpio_get_value(DIO_O3_PIN) ^ 1) << 3;

  val |= (hal_gpio_get_value(DIO_O13_PIN) ^ 1) << 13;
  val |= (hal_gpio_get_value(DIO_O14_PIN) ^ 1) << 14;
  return val;
}

int dio_init(void)
{
  int i;
  struct dio_hw_des* des = &dio_hw_des[0];

  for (i = 0; i < sizeof(dio_pin_setup) / sizeof(dio_pin_setup[0]); i++) {
    hal_pin_conf(dio_pin_setup[i]);
  }

  des->rdaddr = NULL;
  des->wraddr = NULL;
  des->get = dio_get_rocon;
  des->set = dio_set_rocon;
  des->mod = dio_mod_rocon;
  des->backrd = dio_backrd_rocon;

  return 0;
}
