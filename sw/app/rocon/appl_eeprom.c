#include <string.h>

#include "appl_defs.h"

#include "appl_eeprom.h"

#ifdef WATCHDOG_ENABLED
#include <hal_machperiph.h>
#endif /* WATCHDOG_ENABLED */

#include <ul_log.h>
extern UL_LOG_CUST(ulogd_distore)

#ifdef CONFIG_OC_I2C_DRV_SYSLESS

void *(* const appl_distore_reserve_ram)(size_t size);

int appl_eeprom_chip_init(appl_eeprom_chip_context_t *chip)
{
  size_t pgbufflen = chip->page_size + APPL_EEPROM_ADDR_SIZE;

  if (appl_distore_reserve_ram != NULL)
    chip->rxtx_buf = appl_distore_reserve_ram(pgbufflen);

  if (chip->rxtx_buf == NULL)
    chip->rxtx_buf = malloc(pgbufflen);

  if (chip->rxtx_buf == NULL)
  {
    ul_logerr("appl_eeprom_chip_init RAM allocation failed\n");
    return -1;
  }

  return 0;
}

#define APPL_EEPROM_CHUNK_SIZE 512

int appl_eeprom_chip_load(appl_eeprom_chip_context_t *chip, unsigned int start_addr,
                          void *data, unsigned int len)
{
  unsigned char u[APPL_EEPROM_ADDR_SIZE];
  int wrc, rdc;
  int res;

  while (len)
  {
    int chunk = APPL_EEPROM_CHUNK_SIZE;

    if (len < chunk)
      chunk = len;

#ifdef WATCHDOG_ENABLED
    watchdog_feed();
#endif /* WATCHDOG_ENABLED */


    if (APPL_EEPROM_ADDR_SIZE >= 2)
    {
      u[0] = start_addr >> 8;
      u[1] = start_addr & 0xff;
    }
    else
    {
      u[0] = start_addr & 0xff;
    }

    res = i2c_drv_master_transfer(chip->i2c_drv, chip->i2c_addr,
                                  APPL_EEPROM_ADDR_SIZE, chunk, u, data, &wrc, &rdc);

    if ((res < 0) || (wrc != APPL_EEPROM_ADDR_SIZE) || (rdc != chunk))
    {
      ul_logerr("appl_eeprom_chip_load address 0x%x failed\n", start_addr);
      return -1;
    }

    len -= chunk;
    start_addr += chunk;
    data = (char *)data + chunk;
  }

  return 0;
}

int appl_eeprom_chip_transfer_callback(struct i2c_drv *drv, int code, struct i2c_msg_head *msg);

int appl_eeprom_chip_transfer(appl_eeprom_chip_context_t *chip,
                              unsigned int start_addr, const void *data, unsigned int len)
{
  unsigned char *p;

  chip->tx_inpr = 1;

//  msg.flags = I2C_MSG_CB_PROC | I2C_MSG_NOPROC | I2C_MSG_MS_TX;
  chip->i2c_msg.flags = I2C_MSG_CB_END | I2C_MSG_NOPROC | I2C_MSG_MS_TX;
  chip->i2c_msg.addr = chip->i2c_addr;
  chip->i2c_msg.tx_rq = len + APPL_EEPROM_ADDR_SIZE;
  chip->i2c_msg.rx_rq = 0;
  chip->i2c_msg.tx_buf = chip->rxtx_buf;
  chip->i2c_msg.rx_buf = 0;
  chip->i2c_msg.on_queue = NULL;
  chip->i2c_msg.callback = appl_eeprom_chip_transfer_callback;
  chip->i2c_msg.private = (unsigned long)chip;

  p = chip->rxtx_buf;

  if (APPL_EEPROM_ADDR_SIZE >= 2)
    *(p++) = start_addr >> 8;

  *(p++) = start_addr & 0xff;

  if (data != NULL)
    memcpy(p, data, len);

  if (i2c_drv_master_msg_ins(chip->i2c_drv, &chip->i2c_msg) < 0)
    return -1;

  return 0;
}

int appl_eeprom_chip_transfer_callback(struct i2c_drv *drv, int code, struct i2c_msg_head *msg)
{
  appl_eeprom_chip_context_t *chip = (appl_eeprom_chip_context_t *)msg->private;

  if (msg->flags & I2C_MSG_FAIL)
  {
    chip->tx_inpr = -1;
    return 0;
  }

  if (msg->tx_len != msg->tx_rq)
  {
    chip->tx_inpr = -1;
    return 0;
  }

  chip->tx_inpr = 0;
  return 0;
}

int appl_eeprom_chip_process(appl_eeprom_chip_context_t *chip)
{
  size_t ma, len;
  void *data;
  int res;

  if (chip->tx_inpr > 0)
    return 2;

  switch (chip->opstate)
  {
    case APPL_EEPROM_ST_IDLE:
      return 0;
    case APPL_EEPROM_ST_START:
      chip->opstate = APPL_EEPROM_ST_WRDATA;
      chip->tx_inpr = 0;
      chip->retry_cnt = 0;
      chip->pos = 0;
    case APPL_EEPROM_ST_WRDATA:

      if (chip->tx_inpr < 0)
      {
        if (chip->retry_cnt++ > 2000)
        {
          chip->opstate = APPL_EEPROM_ST_IDLE;
          ul_logerr("appl_eeprom_chip write failed at addr 0x%lx\n",
                    (unsigned long)(chip->data_start + chip->pos));

          if (chip->finish_callback)
            chip->finish_callback(chip, chip->callback_context, -1);

          return -1;
        }
      }
      else
      {
        chip->pos += chip->page_size;
        chip->retry_cnt = 0;
      }

      if (chip->pos >= chip->data_len)
      {
        chip->opstate = APPL_EEPROM_ST_WRHEADER;
        chip->pos = 0;
      }

    retry_transfer:

      len = chip->page_size;

      if (len > chip->data_len - chip->pos)
        len = chip->data_len - chip->pos;

      ma = chip->data_start + chip->pos;
      data = (char *)chip->data_buf + chip->pos;

      ul_logdeb("appl_eeprom_chip write chunk at %p to 0x%lx len %ld\n", data, (long)ma, (long)len);

      res = appl_eeprom_chip_transfer(chip, ma, data, len);

      if (res < 0)
      {
        chip->opstate = APPL_EEPROM_ST_IDLE;
        ul_logerr("appl_eeprom_chip write transfer request failed %d\n", res);

        if (chip->finish_callback)
          chip->finish_callback(chip, chip->callback_context, -2);

        return -1;
      }

      break;

    case APPL_EEPROM_ST_WRHEADER:

      if (chip->tx_inpr < 0)
      {
        if (chip->retry_cnt++ > 2000)
        {
          chip->opstate = APPL_EEPROM_ST_IDLE;
          ul_logerr("appl_eeprom_chip write failed at addr 0x%lx\n",
                    (unsigned long)(chip->data_start + chip->pos));

          if (chip->finish_callback)
            chip->finish_callback(chip, chip->callback_context, -1);

          return -1;
        }

        goto retry_transfer;
      }

      chip->opstate = APPL_EEPROM_ST_IDLE;

      if (chip->finish_callback)
        chip->finish_callback(chip, chip->callback_context, 0);

      break;
  }

  return 1;
}

int appl_eeprom_chip_write_transfer_setup(appl_eeprom_chip_context_t *chip,
    void *buf, size_t start_addr, size_t len,
    int (*finish_cb)(struct appl_eeprom_chip_context_t *chip, void *context, int result),
    void *ctx)
{
  if (chip->tx_inpr > 0)
    return -1;

  chip->opstate = APPL_EEPROM_ST_START;
  chip->data_buf = buf;
  chip->data_start = start_addr;
  chip->data_len = len;
  chip->finish_callback = finish_cb;
  chip->callback_context = ctx;

  return 0;
}

#endif /*CONFIG_OC_I2C_DRV_SYSLESS*/
