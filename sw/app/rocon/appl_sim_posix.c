#include <stdio.h>
#include <cmd_proc.h>
#include <pxmc.h>
#include <pxmc_internal.h>

int uart0Getch(void)
{
  return -1;
}

int uart0PutchNW(int ch)
{
  putchar(ch);
  return (unsigned char)ch;
}

int
pxmc_hh_gi(pxmc_state_t *mcs)
{
  return 0;
}

pxmc_call_t *pxmc_get_hh_gi_4axis(pxmc_state_t *mcs)
{
  return pxmc_hh_gi;
}

int
pxmc_pid_con(pxmc_state_t *mcs)
{
  return 0;
}

int pxmc_process_state_check(unsigned long *pbusy_bits,
                             unsigned long *perror_bits)
{
  return 0;
}

int pxmc_clear_power_stop(void)
{
  return 0;
}

int pxmc_axis_rdmode(pxmc_state_t *mcs)
{
  return 0;
}

int pxmc_rocon_pthalalign(pxmc_state_t *mcs, int periods)
{
  return 0;
}

/*
int pxmc_initialize(void)
{
  return 0;
}
*/

cmd_des_t const *cmd_appl_tests[] =
{
  NULL
};

int appl_distore_init(void)
{
  return 0;
}
int appl_distore_user_set_check4change(void)
{
  return 0;
}

int appl_distore_user_restore(void)
{
  return 0;
}

int appl_distore_user_change_check(void)
{
  return 0;
}

void fpga_init(void)
{
  pxmc_initialize();
}

int pxmc_done(void)
{
  return 0;
}
