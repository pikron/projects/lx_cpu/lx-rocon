#include <string.h>
#include <limits.h>
#include <suiut/sui_dinfo.h>
#include <suiut/sui_di_text.h>

#define DISTORE_LONG_BIT (sizeof(long)*CHAR_BIT)

#include "distore_simple.h"

#include <ul_log.h>
UL_LOG_CUST(ulogd_distore)

static int
distore_des_array_bsearch_indx(const distore_des_array_t *array,
                               const distore_id_t key)
{
  unsigned a, b, c;
  int r;

  if (!array->array.items || !array->array.count)
  {
    return -1;
  }

  a = 0;
  b = array->array.count;

  while (1)
  {
    c = (a + b) / 2;
    r = array->array.items[c].id - key;

    if (!r)
      break;

    if (r <= 0)
      a = c + 1;
    else
      b = c;

    if (a == b)
    {
      return -a - 1;
    }
  }

  return c;
}

const distore_des_item_t *
distore_des_array_at(const distore_des_array_t *array, unsigned indx)
{
  if (indx >= array->array.count)
    return NULL;

  return &array->array.items[indx];
}

const distore_des_item_t *
distore_des_array_find(const distore_des_array_t *array, distore_id_t const *key)
{
  int indx;
  indx = distore_des_array_bsearch_indx(array, *key);

  if (indx < 0)
    return NULL;

  return &array->array.items[indx];
}

static ssize_t distore_encform_maxsize(unsigned int encform)
{
  unsigned int enccode;

  if (!((encform ^ DISTORE_ENC_TEXT_CODE) & ~DISTORE_ENC_TEXT_LEN))
    return (encform & DISTORE_ENC_TEXT_LEN) + 1;

  enccode = (encform & ~DISTORE_ENC_NUM_LEN);

  if ((enccode == DISTORE_ENC_UNSIGNED_CODE) ||
      (enccode == DISTORE_ENC_SIGNED_CODE) ||
      (enccode == DISTORE_ENC_FLOAT_CODE))
    return encform & DISTORE_ENC_NUM_LEN;

  return -1;
}

int distore_write_unsigned(distore_wrfnc_t *wrfnc, void *context,
                           unsigned long val, int len)
{
  unsigned char buf[DISTORE_ENC_NUM_LEN + 1];

  if ((len > 4) || (len > DISTORE_LONG_BIT / 8))
  {
    int i;

    for (i = 0; i < len; i++)
    {
      buf[i] = val & 0xff;
      val >>= 8;
    }
  }
  else
  {
    buf[0] = val;
    buf[1] = val >> 8;
    buf[2] = val >> 16;
    buf[3] = val >> 24;
  }

  return (*wrfnc)(context, buf, len) >= 0 ? 0 : -1;
}

int distore_read_unsigned(distore_rdfnc_t *rdfnc, void *context,
                          unsigned long *pval, int len)
{
  unsigned long val;
  unsigned char buf[DISTORE_ENC_NUM_LEN + 1];

  if ((*rdfnc)(context, buf, len) < 0)
    return -1;

  if (len * 8 != DISTORE_LONG_BIT)
  {
    int i;
    val = 0;

    for (i = len; i --;)
    {
      val <<= 8;
      val |= buf[i] & 0xff;
    }
  }
  else
  {
    val = buf[0];
    val |= (unsigned)buf[1] << 8;
    val |= (unsigned long)buf[2] << 16;
    val |= (unsigned long)buf[3] << 24;
  }

  *pval = val;
  return 0;
}

int distore_write_signed(distore_wrfnc_t *wrfnc, void *context,
                         long val, int len)
{
  unsigned char buf[DISTORE_ENC_NUM_LEN + 1];

  if ((len > 4) || (len > DISTORE_LONG_BIT / 8))
  {
    int i;

    for (i = 0; i < len; i++)
    {
      buf[i] = val & 0xff;
      val >>= 8;
    }
  }
  else
  {
    buf[0] = val;
    buf[1] = val >> 8;
    buf[2] = val >> 16;
    buf[3] = val >> 24;
  }

  return (*wrfnc)(context, buf, len) >= 0 ? 0 : -1;
}

int distore_read_signed(distore_rdfnc_t *rdfnc, void *context,
                        signed long *pval, int len)
{
  signed long val;
  unsigned char buf[DISTORE_ENC_NUM_LEN + 1];

  if ((*rdfnc)(context, buf, len) < 0)
    return -1;

  if (len * 8 != DISTORE_LONG_BIT)
  {
    int i;
    val = 0;

    if (len && (len * 8 < DISTORE_LONG_BIT) && (buf[len - 1] & 0x80))
      val = -1;

    for (i = len; i--;)
    {
      val <<= 8;
      val |= buf[i] & 0xff;
    }
  }
  else
  {
    val = buf[0];
    val |= (unsigned int)buf[1] << 8;
    val |= (unsigned long)buf[2] << 16;
    val |= (unsigned long)buf[3] << 24;
  }

  *pval = val;
  return 0;
}

int distore_write_dinfo(distore_wrfnc_t *wrfnc, void *context,
                        sui_dinfo_t *dinfo, long idx, unsigned int encform)
{
  unsigned int enccode;
  int res = -1;
  int len;

  if (!((encform ^ DISTORE_ENC_TEXT_CODE) & ~DISTORE_ENC_TEXT_LEN))
  {
    utf8 *val;

    if (sui_rd_utf8(dinfo, idx, &val) != SUI_RET_OK)
      return -1;

    len = sui_utf8_bytesize(val);

    if (len > (encform & DISTORE_ENC_TEXT_LEN))
      encform = DISTORE_ENC_TEXT_LEN;

    if (distore_write_unsigned(wrfnc, context, len, 1) < 0)
      goto error_utf8;

    if ((*wrfnc)(context, sui_utf8_get_text(val), len) < 0)
      goto error_utf8;

    res = 0;
  error_utf8:
    sui_utf8_dec_refcnt(val);
    return res;
  }

  enccode = encform >> DISTORE_ENC_NUM_LEN_BITS;
  len = encform & DISTORE_ENC_NUM_LEN;

  switch (enccode)
  {
    case DISTORE_ENC_UNSIGNED_CODE >> DISTORE_ENC_NUM_LEN_BITS:
    {
      unsigned long val;

      if (sui_rd_ulong(dinfo, idx, &val) != SUI_RET_OK)
        return -1;

      if (distore_write_unsigned(wrfnc, context, val, len) < 0)
        return -1;

      return 0;
    }
    case DISTORE_ENC_SIGNED_CODE >> DISTORE_ENC_NUM_LEN_BITS:
    {
      long val;

      if (sui_rd_long(dinfo, idx, &val) != SUI_RET_OK)
        return -1;

      if (distore_write_signed(wrfnc, context, val, len) < 0)
        return -1;

      return 0;
    }
    case DISTORE_ENC_FLOAT_CODE >> DISTORE_ENC_NUM_LEN_BITS:
    {
      return -1;
    }
    default:
      return -1;
  }
}

int distore_read_dinfo(distore_rdfnc_t *rdfnc, void *context,
                       sui_dinfo_t *dinfo, long idx, unsigned int encform)
{
  unsigned int enccode;
  int res = -2;
  int len;
  unsigned long u;

  if (!((encform ^ DISTORE_ENC_TEXT_CODE) & ~DISTORE_ENC_TEXT_LEN))
  {
    utf8 *val;

    if (distore_read_unsigned(rdfnc, context, &u, 1) < 0)
      return -2;

    val = sui_utf8_dynamic(NULL, -1, u);

    if (val == NULL)
      return -2;

    if ((*rdfnc)(context, sui_utf8_get_text(val), u) < 0)
      goto error_utf8;

    res = -1;

    if (dinfo)
      if (sui_wr_utf8(dinfo, idx, &val) != SUI_RET_OK)
        goto error_utf8;

    res = 0;
  error_utf8:
    sui_utf8_dec_refcnt(val);
    return res;
  }

  enccode = encform >> DISTORE_ENC_NUM_LEN_BITS;
  len = encform & DISTORE_ENC_NUM_LEN;

  switch (enccode)
  {
    case DISTORE_ENC_UNSIGNED_CODE >> DISTORE_ENC_NUM_LEN_BITS:
    {
      unsigned long val;

      if (distore_read_unsigned(rdfnc, context, &val, len) < 0)
        return -2;

      if (dinfo)
        if (sui_wr_ulong(dinfo, idx, &val) != SUI_RET_OK)
          return -1;

      return 0;
    }
    case DISTORE_ENC_SIGNED_CODE >> DISTORE_ENC_NUM_LEN_BITS:
    {
      long val;

      if (distore_read_signed(rdfnc, context, &val, len) < 0)
        return -2;

      if (dinfo)
        if (sui_wr_long(dinfo, idx, &val) != SUI_RET_OK)
          return -1;

      return 0;
    }
    case DISTORE_ENC_FLOAT_CODE >> DISTORE_ENC_NUM_LEN_BITS:
    {
      return -2;
    }
    default:
      return -2;
  }
}

ssize_t distore_count_maxsize(const distore_des_array_t *des)
{
  ssize_t count = 0;
  ssize_t ims;
  int idx;
  const distore_des_item_t *item;
  int idxlimit;

  for (idx = 0; idx < des->array.count; idx++)
  {
    item = distore_des_array_at(des, idx);

    if (item == NULL)
      return -1;

    idxlimit = item->idxlimit;

    if (!idxlimit)
    {
      idxlimit = item->dinfo->idxsize;
    }

    ims = distore_encform_maxsize(item->encform);

    if (ims < 0)
      return -1;

    count += 1 + 1 + 2;
    count += ims * idxlimit;
  }

  count += 1;
  return count;
}

int distore_store_data(const distore_des_array_t *des,
                       distore_wrfnc_t *wrfnc, void *context)
{
  int idx;
  int i;
  const distore_des_item_t *item;
  int idxlimit;

  for (idx = 0; idx < des->array.count; idx++)
  {
    item = distore_des_array_at(des, idx);

    if (item == NULL)
      return -1;

    if (item->flags & DISTORE_FLG_LOAD_ONLY)
      continue;

    idxlimit = item->idxlimit;

    if (!idxlimit || (idxlimit > item->dinfo->idxsize))
    {
      idxlimit = item->dinfo->idxsize;
    }

    if (distore_write_unsigned(wrfnc, context, item->encform, 1) < 0)
      goto error;

    if (distore_write_unsigned(wrfnc, context, idxlimit, 1) < 0)
      goto error;

    if (distore_write_unsigned(wrfnc, context, item->id, 2) < 0)
      goto error;

    for (i = 0; i < idxlimit; i++)
    {
      if (distore_write_dinfo(wrfnc, context, item->dinfo, i, item->encform) < 0)
        goto error;
    }
  }

  if (distore_write_unsigned(wrfnc, context, 0, 1) < 0)
    goto error;

  return 0;

error:
  return -1;
}

int distore_load_data(const distore_des_array_t *des,
                      distore_rdfnc_t *rdfnc, void *context, int mode)
{
  unsigned long u;
  unsigned int encform;
  unsigned int idxlimit;
  distore_id_t id;
  const distore_des_item_t *item;
  sui_dinfo_t *dinfo;
  int i;

  do
  {
    dinfo = NULL;

    if (distore_read_unsigned(rdfnc, context, &u, 1) < 0)
      goto error;

    encform = u;

    if (encform == DISTORE_ENC_END)
      break;

    if (distore_read_unsigned(rdfnc, context, &u, 1) < 0)
      goto error;

    idxlimit = u;

    if (distore_read_unsigned(rdfnc, context, &u, 2) < 0)
      goto error;

    id = u;
    item = distore_des_array_find(des, &id);

    do
    {
      if (item == NULL)
      {
        if (mode & DISTORE_LOAD_IGNORE_UNKNOWN)
          break;

        goto error;
      }

      if (encform != item->encform)
      {
        if (!((encform ^ DISTORE_ENC_TEXT_CODE) & ~DISTORE_ENC_TEXT_LEN))
        {
          if ((encform ^ item->encform) & ~DISTORE_ENC_TEXT_LEN)
            break;
        }
        else
          if ((encform == DISTORE_ENC_UNSIGNED_CODE) ||
              (encform == DISTORE_ENC_SIGNED_CODE) ||
              (encform == DISTORE_ENC_FLOAT_CODE))
          {
            if ((encform ^ item->encform) & ~DISTORE_ENC_NUM_LEN)
              break;
          }
          else
            break;
      }

      dinfo = item->dinfo;
    }
    while (0);

    for (i = 0; i < idxlimit; i++)
    {
      int res = distore_read_dinfo(rdfnc, context, dinfo, i, encform);

      if ((res <= -2) || ((res < 0) && !
                          (mode & DISTORE_LOAD_IGNORE_WRITE_ERR)))
        goto error;
    }
  }
  while (1);

  return 0;

error:
  return -1;
}
