#include <stdio.h>
#include <stdlib.h>

#ifdef OMK_FOR_TARGET
 #include <cpu_def.h>
 #include <system_def.h>
 #include <lt_timer.h>
 unsigned long sqrtll(unsigned long long x);
 unsigned long sqrtll_approx(unsigned long long x);
 #define main sqrtll_main
 #ifdef LPC_TIM0
  #define sqrtll_ticks() (LPC_TIM0->TC)
 #endif /*LPC_TIM0*/
#else /*OMK_FOR_TARGET*/
 #if 0
  #include "math_sqrtll.c"
 #else
  #define main sqrtll_main
 #endif
 #define lt_mstime_update() do {} while(0)
 #define cli() do {} while(0)
 #define sti() do {} while(0)
 int actual_msec;
#endif /*OMK_FOR_TARGET*/

#ifndef sqrtll_ticks
  #define sqrtll_ticks() 0
#endif

int main(int argc, char *argv[])
{
  unsigned long long sqr;
  unsigned long res,inp,incr,cnt,time;
  unsigned short spent;

  char *s="a";

  if (argc>=2)
    s=argv[1];

  if(*s!='a'){
    inp=strtoul(s,NULL,0);
    sqr=(unsigned long long int)inp*inp;
    cli();
    spent=sqrtll_ticks();
    res=sqrtll_approx(sqr);
    spent=sqrtll_ticks()-spent;
    sti();
    printf("sqrtll: inp=%lu, res=%lu, spent=%u\n",inp,res,(int)spent);
  }else{
    inp=0; incr=1; cnt=0;
    int eps=1;
    lt_mstime_update();
    time=actual_msec;
    do{
      sqr=(unsigned long long int)inp * inp;
      res=sqrtll_approx(sqr);
      if(0||(labs(inp-res)) >= eps){
        printf("\nsqrt error %lu -> %lu diff %ld sqr 0x%llx\n",inp,res,inp-res,sqr);
        eps<<=1;
        res=sqrtll_approx(sqr);
      }
      cnt++;
      if(inp>0xffffffff-incr)
        break;
      inp+=incr;
      if(inp&0xfff) continue;
      if(inp&((incr<<19)-1)) continue;
      incr<<=1;
      printf(".");fflush(NULL);
    }while(1);
    lt_mstime_update();
    time=actual_msec-time;
    printf("\nsqrtll OK, spent %ld cnt %ld\n",time,cnt);
  }
  return 0;
}
