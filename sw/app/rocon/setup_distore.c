#include <string.h>

#include "appl_defs.h"

#ifdef APPL_WITH_DISTORE_KEYVAL_SERVICE

#include <keyvalpb.h>
#include "distore_simple.h"

#include <ul_log.h>
extern UL_LOG_CUST(ulogd_distore)

extern kvpb_block_t *kvpb_block;

typedef struct setup_distore_context_t
{
  size_t pos;
  size_t limit;
  void *buf;
} setup_distore_context_t;

extern const distore_des_array_t setup_distore_service_des;

ssize_t setup_distore_wrfnc(void *context, const void *buf, size_t count)
{
  setup_distore_context_t *c = (setup_distore_context_t *)context;

  if (c->limit - c->pos < count)
    return -1;

  memcpy((char *)c->buf + c->pos, buf, count);
  c->pos += count;
  return count;
}

ssize_t setup_distore_rdfnc(void *context, void *buf, size_t count)
{
  setup_distore_context_t *c = (setup_distore_context_t *)context;

  if (c->limit - c->pos < count)
    return -1;

  memcpy(buf, (char *)c->buf + c->pos, count);
  c->pos += count;
  return count;
}

int setup_distore_store(void)
{
  setup_distore_context_t ctx;
  setup_distore_context_t *c = &ctx;
  const distore_des_array_t *des = &setup_distore_service_des;
  ssize_t sz;
  int res = -1;

  if (kvpb_block == NULL)
    return -1;

  c->buf = NULL;
  c->limit = 0;
  c->pos = 0;
  sz = distore_count_maxsize(des);

  if (sz < 0)
  {
    ul_logerr("setup_distore_store can not compute maxsize\n");
    return -1;
  }

  c->buf = malloc(sz);

  if (c->buf == NULL)
  {
    ul_logerr("setup_distore_store malloc failed\n");
    return -1;
  }

  c->limit = sz;

  if (distore_store_data(des, setup_distore_wrfnc, c) < 0)
  {
    ul_logerr("setup_distore_store DINFO encoding failed\n");
    goto error;
  }

  if (kvpb_set_key(kvpb_block, KVPB_KEYID_SETUP_SERVICE, c->pos, c->buf) < 0)
  {
    ul_logerr("setup_distore_store kvpb_set_key failed\n");
    goto error;
  }

  ul_logmsg("setup_distore_store proceed OK\n");
  res = 0;
error:

  if (c->buf != NULL)
    free(c->buf);

  return res;
}

int setup_distore_restore(void)
{
  setup_distore_context_t ctx;
  setup_distore_context_t *c = &ctx;
  const distore_des_array_t *des = &setup_distore_service_des;
  ssize_t sz;
  int res = -1;

  if (kvpb_block == NULL)
    return -1;

  c->buf = NULL;
  c->limit = 0;
  c->pos = 0;

  sz = kvpb_get_key(kvpb_block, KVPB_KEYID_SETUP_SERVICE, 0, NULL);

  if (sz < 0)
  {
    ul_logmsg("setup_distore_restore SETUP_SERVICE key not found\n");
    return -1;
  }

  if (sz > KVPB_SETUP_SERVICE_MAX_SIZE)
  {
    ul_logmsg("setup_distore_restore SETUP_SERVICE key size exceed\n");
    return -1;
  }

  c->buf = malloc(sz);

  if (c->buf == NULL)
  {
    ul_logerr("setup_distore_restore malloc failed\n");
    return -1;
  }

  c->limit = sz;

  if (kvpb_get_key(kvpb_block, KVPB_KEYID_SETUP_SERVICE, sz, c->buf) != sz)
  {
    ul_logerr("setup_distore_restore kvpb_get_key error\n");
    goto error;
  }

  res = distore_load_data(des, setup_distore_rdfnc, c,
                          DISTORE_LOAD_IGNORE_UNKNOWN | DISTORE_LOAD_IGNORE_WRITE_ERR);

  if (res < 0)
    ul_logerr("setup_distore_restore DINFO decode failed (%d)\n", res);
  else
    ul_logmsg("setup_distore_restore proceed OK\n");

error:

  if (c->buf != NULL)
    free(c->buf);

  return res;
}

#endif /* APPL_WITH_DISTORE_KEYVAL_SERVICE */
