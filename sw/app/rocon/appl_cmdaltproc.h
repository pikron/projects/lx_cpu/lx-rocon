#ifndef _APPL_CMDALTPROC_H
#define _APPL_CMDALTPROC_H

#include <cmd_proc.h>

typedef int cmd_altlnproc_fnc_in_t(cmd_io_t* cmd_io, void *ptr_arg, long long_arg, char *line);
typedef int cmd_altlnproc_fnc_out_t(cmd_io_t* cmd_io, void *ptr_arg, long long_arg);
typedef int cmd_altlnproc_fnc_done_t(cmd_io_t* cmd_io, void *ptr_arg, long long_arg);

int cmdproc_altproc_setup(cmd_io_t *cmd_io, cmd_altlnproc_fnc_in_t *altlnproc_in,
        cmd_altlnproc_fnc_out_t *altlnproc_out,  cmd_altlnproc_fnc_done_t *altlnproc_done,
        void *altlnproc_ptr_arg, long altlnproc_long_arg);
int cmdproc_altproc_set_ptr_arg(cmd_io_t *cmd_io, void *altlnproc_ptr_arg);
int cmdproc_altproc_set_long_arg(cmd_io_t *cmd_io, long altlnproc_long_arg);

void cmdproc_altproc_done(cmd_io_t *cmd_io);

#endif /*_APPL_CMDALTPROC_H*/