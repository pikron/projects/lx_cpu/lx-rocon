/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  load_zlib_binary.h - load gzipped binary image

  Copyright (C) 2001-2017 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2017 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _LOAD_ZLIB_BINARY_H
#define _LOAD_ZLIB_BINARY_H

#include <string.h>

int load_zlib_binary(size_t *bytes_read,
	int (*read_char)(void *read_char_arg), void *read_char_arg,
	void *base_addr, size_t bytes_max, int flags,
	int (*progress_fnc)(void *context, size_t bytes, char *msg),
	void *context);

#endif /* _LOAD_ZLIB_BINARY_H */
