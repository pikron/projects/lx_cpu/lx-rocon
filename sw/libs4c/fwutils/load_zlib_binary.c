/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  load_zlib_binary.c - load gzipped binary image

  Copyright (C) 2001-2017 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2017 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#include <string.h>
#include <stdint.h>
#include <zlib.h>
#include "load_zlib_binary.h"

int load_zlib_binary(size_t *bytes_read,
	int (*read_char)(void *read_char_arg), void *read_char_arg,
	void *base_addr, size_t bytes_max, int flags,
	int (*progress_fnc)(void *context, size_t bytes, char *msg),
	void *context)
{
  static const int uart_buff_size=128;
  uint8_t uart_buff[uart_buff_size];
  int uart_buff_bytes;
  int err;
  int res;
  int magic_ok=0;
  z_stream d_stream; /* decompression stream */

  memset(&d_stream, 0, sizeof(d_stream));

  d_stream.zalloc = (alloc_func)0;
  d_stream.zfree = (free_func)0;
  d_stream.opaque = (voidpf)0;

  d_stream.next_in  = uart_buff;
  d_stream.avail_in = 0;
  d_stream.next_out = (Bytef*)base_addr;
  d_stream.avail_out = bytes_max;

  /*err = inflateInit(&d_stream);*/
  err = inflateInit2(&d_stream, MAX_WBITS+32);
  if(err!=Z_OK)
    return err<0?err:-100-err;

  uart_buff_bytes=0;

  do{

    if(!d_stream.avail_out){
      err=-200;
      goto error;
    }

    while(uart_buff_bytes<uart_buff_size){
      res=read_char(read_char_arg);
      if(res<-1){
        err=-220;
        goto error;
      }
      if(res==-1)
        break;
      uart_buff[uart_buff_bytes++]=res;
    }

    if(!uart_buff_bytes){
      if(progress_fnc){
        res=progress_fnc(context, d_stream.total_out, NULL);
	if(res<0){
	  err=res;
          goto error;
	}
      }
      continue;
    }

    if(!magic_ok){
      int i;
      for(i=0;i<uart_buff_bytes;i++)
        if(uart_buff[i]==0x1f)
	  break;
      if(i==uart_buff_bytes){
        uart_buff_bytes=0;
	continue;
      }
      if(i){
        uart_buff_bytes-=i;
        memmove(uart_buff,uart_buff+i,uart_buff_bytes);
      }
      if(uart_buff_bytes<2)
        continue;
      if(uart_buff[1]!=0x8b){
        uart_buff_bytes-=2;
        memmove(uart_buff,uart_buff+2,uart_buff_bytes);
      }
      magic_ok=1;
    }

    d_stream.next_in = uart_buff;
    d_stream.avail_in = uart_buff_bytes;
    err = inflate(&d_stream, Z_NO_FLUSH);
    if(d_stream.next_in-uart_buff>uart_buff_bytes){
        err=-260;
        goto error;
    }
    uart_buff_bytes -= d_stream.next_in-uart_buff;
    if(uart_buff_bytes)
      memmove(uart_buff,d_stream.next_in,uart_buff_bytes);

    if (err == Z_STREAM_END) break;
    if(err!=Z_OK)
      goto error;
  }while(1);

  err = inflateEnd(&d_stream);
  if(err!=Z_OK)
    goto error;

  if(progress_fnc)
    res=progress_fnc(context, d_stream.total_out, NULL);

  *bytes_read = d_stream.total_out;
  return 0;

error:
  if(d_stream.msg){
    if(progress_fnc)
      res=progress_fnc(context, d_stream.total_out, d_stream.msg);
  }

  inflateEnd(&d_stream);
  return err<0?err:-100-err;
}

