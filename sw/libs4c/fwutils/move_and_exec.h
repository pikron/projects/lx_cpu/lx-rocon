/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  move_and_exec.h - routine to start new application

  Copyright (C) 2001-2017 by Pavel Pisa pisa@cmp.felk.cvut.cz
            (C) 2001-2017 by PiKRON Ltd. http://www.pikron.com

 *******************************************************************/

#ifndef _MOVE_AND_EXEC_H
#define _MOVE_AND_EXEC_H

#include <string.h>

void move_and_exec(void *dst, void *src, size_t size, void *entry,
                   void *stack_top) __attribute__((noreturn));

extern char move_and_exec_start;
extern char move_and_exec_end;

#endif /* _MOVE_AND_EXEC_H */
