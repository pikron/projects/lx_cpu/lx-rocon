library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.mbl_pkg.all;
use work.lx_rocon_pkg.all;

-- 4 kB data memory for Thumbl core
-- To be flashed from the Master CPU

entity lx_rocon_dmem is
	port
	(
		-- Memory wiring for Tumbl
		clk_i  : in std_logic;
		ce_i   : in std_logic;
		adr_i  : in std_logic_vector(9 downto 0);
		bls_i  : in std_logic_vector(3 downto 0);
		dat_i  : in std_logic_vector(31 downto 0);
		dat_o  : out std_logic_vector(31 downto 0);
		-- Memory wiring for Master CPU
		clk_m  : in std_logic;
    en_m   : in std_logic;
    we_m   : in std_logic_vector(3 downto 0);
    addr_m : in std_logic_vector(9 downto 0);
    din_m  : in std_logic_vector(31 downto 0);
    dout_m : out std_logic_vector(31 downto 0)
	);
end lx_rocon_dmem;

architecture rtl of lx_rocon_dmem is
begin

I_RAMB: xilinx_dualport_bram
	generic map
	(
		we_width => 4,
		byte_width => 8,
		address_width => 10,
		port_a_type => READ_FIRST,
		port_b_type => READ_FIRST
	)
	port map
	(
		-- Tumblr port
		clka => clk_i,
		rsta => '0',
		ena => ce_i,
		wea => bls_i,
		addra => adr_i,
		dina => dat_i,
		douta => dat_o,

		-- Master CPU port
		clkb => clk_m,
		rstb => '0',
		enb => en_m,
		web => we_m,
		addrb => addr_m,
		dinb => din_m,
		doutb => dout_m
	);

end rtl;
