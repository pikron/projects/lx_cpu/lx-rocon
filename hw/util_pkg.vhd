library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package util_pkg is
  -- ceil(log2(n))
  function ceil_log2(n: natural) return natural;
  -- ceil(a/b)
  function ceil_div(a: integer; b: integer) return integer;
  --
  function max(left, right: integer) return integer;
  function min(left, right: integer) return integer;
end;

---

package body util_pkg is

  function ceil_log2(n: natural) return natural is
  begin
    if n <= 1 then
      return 0;
    else
      if n mod 2 = 0 then
        return 1 + ceil_log2(n/2);
      else
        return 1 + ceil_log2((n+1)/2);
      end if;
    end if;
  end function ceil_log2;

  function ceil_div(a: integer; b: integer) return integer is
  begin
    return (a+b-1)/b;
  end function ceil_div;

  function max(left, right: integer) return integer is
  begin
    if left > right then return left;
    else return right;
    end if;
  end;

  function min(left, right: integer) return integer is
  begin
    if left < right then return left;
    else return right;
    end if;
  end;

end util_pkg;
