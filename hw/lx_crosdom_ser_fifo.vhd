-- Clock Cross Domain Synchronization Elastic Buffer/FIFO
--
-- Copyright (c) 2014, Pavel Pisa <pisa@cmp.felk.cvut.cz>
-- Designed for PiKRON company robotic controller
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this
--    list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice,
--    this list of conditions and the following disclaimer in the documentation
--    and/or other materials provided with the distribution.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
-- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
-- DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
-- ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
-- (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
-- LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
-- ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
-- (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
-- SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
-- Can be used and distributed under GPLv3 license as well

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.util_pkg.all;
use work.lx_rocon_pkg.all;

entity lx_crosdom_ser_fifo is
generic
(
	fifo_len_g   : positive := 8;
	sync_adj_g   : integer := 0
);
port
(
	-- Asynchronous clock domain interface
	acd_clock_i  : in std_logic;
	acd_miso_i   : in std_logic;
	acd_sync_i   : in std_logic;
	-- Clock
	clk_i        : in std_logic;
	reset_i      : in std_logic;
	-- Output synchronous with clk_i
	miso_o       : out std_logic;
	sync_o       : out std_logic;
	data_ready_o : out std_logic
);
end lx_crosdom_ser_fifo;

architecture Behavioral of lx_crosdom_ser_fifo is
	signal fifo_bits_s     : std_logic_vector(0 to fifo_len_g - 1);
	signal fifo_bits_r     : std_logic_vector(0 to fifo_len_g - 1);

	signal acd_miso_r      : std_logic;
	signal acd_sync_r      : std_logic;
	signal acd_sync_prev_s : std_logic;
	signal acd_sync_prev_r : std_logic;

	signal acd_in_loc_s    : natural range 0 to fifo_len_g - 1;
	signal acd_in_loc_r    : natural range 0 to fifo_len_g - 1;

	signal out_loc_s       : natural range 0 to fifo_len_g - 1;
	signal out_loc_r       : natural range 0 to fifo_len_g - 1;

	signal out_sync_s      : std_logic_vector(0 to fifo_len_g / 2 - 1);
	signal out_sync_r      : std_logic_vector(0 to fifo_len_g / 2 - 1);

	signal out_miso_s      : std_logic;

  attribute REGISTER_DUPLICATION : string;
	attribute REGISTER_DUPLICATION of fifo_bits_s : signal is "NO";
	attribute REGISTER_DUPLICATION of fifo_bits_r : signal is "NO";
	attribute REGISTER_DUPLICATION of acd_sync_r : signal is "NO";
	attribute REGISTER_DUPLICATION of out_sync_s : signal is "NO";
	attribute REGISTER_DUPLICATION of out_sync_r : signal is "NO";

begin
	sync_o <= out_sync_r(max(-sync_adj_g, 0));

	data_ready_o <= '0';

acd_logic:
	process (acd_miso_r, acd_sync_r, acd_sync_prev_r, acd_in_loc_r, fifo_bits_r)
	begin
		acd_sync_prev_s <= acd_sync_r;
		fifo_bits_s <= fifo_bits_r;
		if (acd_sync_r = '1') and (acd_sync_prev_r = '0') then
			acd_in_loc_s <= 0;
			fifo_bits_s(0) <= acd_miso_r;
		else
			fifo_bits_s(acd_in_loc_r) <= acd_miso_r;
			if acd_in_loc_r /= fifo_len_g - 1 then
				acd_in_loc_s <= acd_in_loc_r + 1;
			else
				acd_in_loc_s <= 0;
			end if;
		end if;
	end process;

acd_update:
	process
	begin
		wait until acd_clock_i'event and acd_clock_i = '1';

		acd_miso_r <= acd_miso_i;
		acd_sync_r <= acd_sync_i;
		acd_sync_prev_r <= acd_sync_prev_s;
		acd_in_loc_r <= acd_in_loc_s;
		fifo_bits_r <= fifo_bits_s;
	end process;

sync_logic:
	process (fifo_bits_r, out_loc_r, out_sync_r, acd_sync_r)
	begin
		out_sync_s <= out_sync_r(1 to out_sync_r'length - 1 ) & acd_sync_r;
		if out_sync_r(max(sync_adj_g, 0)) = '0' then
			out_loc_s <= 0;
			out_miso_s <= '0';
		else
			out_miso_s <= fifo_bits_r(out_loc_r);
			if out_loc_r /= fifo_len_g - 1 then
				out_loc_s <= out_loc_r + 1;
			else
				out_loc_s <= 0;
			end if;
		end if;
	end process;

sync_update:
	process
	begin
		wait until clk_i'event and clk_i = '1';

		out_loc_r <= out_loc_s;
		miso_o <= out_miso_s;
		out_sync_r <= out_sync_s;
	end process;

end Behavioral;
