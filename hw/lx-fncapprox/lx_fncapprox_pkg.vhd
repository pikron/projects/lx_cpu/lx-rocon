library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Entities within lx_fncapprox_pkg

package lx_fncapprox_pkg is

	component rom_table
	generic
	(
		data_width : integer;
		addr_width : integer;
		init_file  : string
	);
	port
	(
		ack_o  : out std_logic;
		addr_i : in  std_logic_vector (addr_width-1 downto 0);
		clk_i  : in  std_logic;
		data_o : out std_logic_vector (data_width-1 downto 0);
		stb_i  : in  std_logic
	);
	end component;

	component lx_fncapprox_dsp48
	port (
		P         : out std_logic_vector(47 downto 0);
		A         : in  std_logic_vector(17 downto 0) := (others => '0');
		B         : in  std_logic_vector(17 downto 0) := (others => '0');
		C         : in  std_logic_vector(47 downto 0) := (others => '0');
		CLK       : in  std_logic;
		CE        : in  std_logic
	);
	end component;

end lx_fncapprox_pkg;

package body lx_fncapprox_pkg is

end lx_fncapprox_pkg;
