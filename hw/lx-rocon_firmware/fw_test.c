/* Firmware file for lx-rocon tumbl coprocessor */

#include <stdint.h>

typedef struct
{
	int16_t p;
	int16_t i;
	int32_t irc_period;
	int16_t req_current;
	int16_t acc_dev;
	int16_t max_acc;
} axis_settings;

typedef struct
{
	/* Input */
	int16_t current;
	int32_t irc;
	int32_t irc_last;
	int32_t irc_norm;

	/* Output */
	uint16_t pwm[3];
} axis_io;

axis_settings a_settings;

axis_io a_io;
int32_t irc_a_reg;
int16_t current_a_reg;
uint32_t count;
int16_t phase_table[1][1];

void init_defvals()
{
	a_settings.p = 20;
	a_settings.i = 2;
	a_settings.irc_period = 7000;
	a_settings.req_current = 500;
	a_settings.acc_dev = 0;
}

void read_input(axis_io* io)
{
	io->irc_last = io->irc;
	io->irc = *((int32_t*)0x8000);
}

void update_axis(axis_settings *settings, axis_io* io)
{
	int i;
	int16_t dev;
	int32_t mag, irc_diff;

	dev = settings->req_current - io->current;
	settings->acc_dev += dev;

	if (settings->acc_dev > settings->max_acc)
		settings->acc_dev = settings->max_acc;
	else if (-(settings->acc_dev) <= -(settings->max_acc))
		settings->acc_dev = -(settings->max_acc);

	mag = settings->acc_dev * settings->i + dev * settings->p;

	irc_diff = io->irc - io->irc_last;
	io->irc_last = io->irc;
	io->irc_norm += irc_diff;

	if (io->irc_norm > settings->irc_period)
		io->irc_norm -= settings->irc_period;
	else if (io->irc_norm < 0)
		io->irc_norm += settings->irc_period;

	for (i = 0; i < 3; i++)
		io->pwm[i] = (uint16_t)((mag * phase_table[/*i*/ 0][/*irc_norm*/ 0]) >> 16);
}

void main()
{
	while (1)
	{
		read_input(&a_io);
		update_axis(&a_settings, &a_io);
		a_io.irc = irc_a_reg;
		a_io.current = current_a_reg;
		count++;
	}
}
