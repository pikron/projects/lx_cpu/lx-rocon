/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmcc_types.h - multi axis motion controller coprocessor
        for FPGA tumble CPU of lx-rocon system - data types

  (C) 2001-2014 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2014 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMCC_TYPES_H_
#define _PXMCC_TYPES_H_

#include <stdint.h>

#define PXMCC_FWVERSION       0xACCE0001
#define PXMCC_AXIS_COUNT      4
#define PXMCC_CURADC_CHANNELS 16

#define PXMCC_MODE_IDLE              2
#define PXMCC_MODE_BLDC              0
#define PXMCC_MODE_STEPPER_WITH_IRC  1
#define PXMCC_MODE_STEPPER           3
#define PXMCC_MODE_DC_CUR            4

typedef struct pxmcc_common_data_t {
  uint32_t  fwversion;
  uint32_t  pwm_cycle;
  uint32_t  act_idle;
  uint32_t  min_idle;
  uint32_t  rx_done_sqn;
  uint32_t  rx_err_cnt;
  uint32_t  irc_base;
} pxmcc_common_data_t;

typedef struct pxmcc_axis_data_t {
  uint32_t  ccflg;
  uint32_t  mode;
  uint32_t  pwm_dq;	/* D and Q components of PWM (pwm_d << 16) | (pwm_q) & 0xffff */
  uint32_t  cur_dq;	/* D and Q components current (cur_d << 16) | (cur_q) & 0xffff */
  uint32_t  ptindx;	/* index into phase table / irc in the cycle */
  uint32_t  ptirc;	/* IRC count per phase table */
  uint32_t  ptreci;	/* Reciprocal value of ptirc * 63356  */
  uint32_t  ptofs;	/* offset between table and IRC counter */
  int32_t   ptsin;
  int32_t   ptcos;
  uint32_t  ptphs;
  uint32_t  cur_d_cum;
  uint32_t  cur_q_cum;
  uint32_t  inp_info;	/* which irc to use */
  uint32_t  out_info;	/* output index */
  uint32_t  pwmtx_info;	/* offsets of pwm1 .. pwm4 from FPGA_LX_MASTER_TX */
  uint32_t  pwm_prew[4];
  uint32_t  steps_inc;	/* increments for selfgenerated stepper motor */
  uint32_t  steps_pos;	/* self generated position for stepper motor */
  uint32_t  steps_sqn_next; /* when to apply steps_inc_next */
  uint32_t  steps_inc_next; /* increment to apply at steps_sqn_next */
  uint32_t  steps_pos_next; /* base position to apply at steps_sqn_next */
} pxmcc_axis_data_t;

typedef struct pxmcc_curadc_data_t {
  int32_t   cur_val;
  int32_t   siroladc_offs;
  uint32_t  siroladc_last;
} pxmcc_curadc_data_t;

typedef struct pxmcc_data_t {
  pxmcc_common_data_t common;
  pxmcc_axis_data_t   axis[PXMCC_AXIS_COUNT];
  pxmcc_curadc_data_t curadc[PXMCC_CURADC_CHANNELS];
} pxmcc_data_t;

#endif /*_PXMCC_TYPES_H_*/
