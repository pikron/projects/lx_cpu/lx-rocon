#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
fprintbin(FILE *stream, uint32_t num, unsigned dig)
{
  uint32_t mask = 1 << (dig - 1);
  for(; mask; mask >>= 1) {
    if (fputc((num & mask)?'1':'0', stream) == EOF)
      return -1;
  }
  return dig;
}

int main(int argc, char *argv[])
{
  char *fin_fname = NULL;
  FILE *fin;
  int ret = 0;
  int vector_bits      = 32;
  size_t vector_bytes  = 0;
  int vector_be        = 1;
  int vector_be_ralign = 0;

  if (argc >= 2)
    fin_fname = argv[1];

  if (!vector_bytes)
    vector_bytes = (vector_bits + 7) / 8;

  if (!vector_bits)
    vector_bits = vector_bytes * 8;

  if (fin_fname != NULL) {
    fin = fopen(fin_fname, "rb");
    if (fin == NULL) {
      fprintf(stderr, "%s: cannot open input file %s\n", argv[0], fin_fname);
      exit(1);
    }
  } else {
    fin = stdin;
  }

  {
    uint8_t buff[vector_bytes];
    size_t read_res;
    uint8_t *p;
    uint8_t b;
    size_t br;
    size_t bn;

    do {
      read_res = fread(buff, 1, vector_bytes, fin);
      if (read_res == 0)
        break;
      if (read_res == -1) {
        fprintf(stderr, "%s: data read error\n", argv[0]);
        ret = 1;
        break;
      }
      if (read_res < vector_bytes) {
        fprintf(stderr, "%s: partial read of last element\n", argv[0]);
        ret = 1;
        memset(buff + read_res, 0, vector_bytes - read_res);
      }

      p = buff + (vector_be? 0: vector_bytes - 1);
      bn = (vector_be & !vector_be_ralign)? 8: vector_bits % 8;

      for (br = vector_bits; br; br -= bn, bn = 8) {
        if (bn > br)
          bn = br;
        b = *p;
        if (vector_be) {
          if (!vector_be_ralign)
            b >>= (8 - bn);
          p++;
        } else {
          p--;
        }
        if (fprintbin(stdout, b, bn) < 0) {
          fprintf(stderr, "%s: output stream write error\n", argv[0]);
          ret = 1;
          break;
        }
      }

      if (fputc('\n', stdout) == EOF) {
        ret = -1;
        break;
      }
    } while(1);
  }

  if (fin_fname != NULL)
    fclose(fin);

  return ret;
}
