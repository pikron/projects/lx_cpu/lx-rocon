--
-- * CRC-8 generator *
--
-- part of LXPWR motion control board (c) PiKRON Ltd
-- idea by Pavel Pisa PiKRON Ltd <pisa@cmp.felk.cvut.cz>
-- code by Marek Peca <mp@duch.cz>
-- 01/2013
--
-- license: GNU GPLv3
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity crc is
	port
	(
		clk_i   : in std_logic;
		reset_i : in std_logic;
		input_i : in std_logic;
		crc_o   : out std_logic_vector(7 downto 0)
	);
end crc;

architecture behavioral of crc is
	signal reg_s, next_reg_s : std_logic_vector(7 downto 0);
begin
	crc_o <= next_reg_s;

comb: process (reset_i, input_i, reg_s)
	begin
		if reset_i = '1' then
			next_reg_s <= (others => '1');
		else
			next_reg_s(4 downto 0) <= reg_s(5 downto 1);
			next_reg_s(5) <= reg_s(0) xor reg_s(6);
			next_reg_s(6) <= reg_s(0) xor reg_s(7);
			next_reg_s(7) <= reg_s(0) xor input_i;
		end if;
	end process;

seq: process
  begin
		wait until clk_i'event and clk_i = '1';
		reg_s <= next_reg_s;
	end process;

end behavioral;
