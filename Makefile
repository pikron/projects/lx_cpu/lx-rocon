# TOP LEVEL Makefile for lx-rocon
# Files are copied to _build directory

# Installation steps are copied to this makefile

CROSS_COMPILE ?= arm-elf-gcc
BUILDDIR := $(CURDIR)/_build
PATH := $(BUILDDIR):$(PATH)

USB_BOOT_VID_PID ?= 0xdead:0x2263
USB_APP_VID_PID ?= 0x1669:0x1023

PROG_BASE ?= 0x00009000
PROG_SIZE ?= 0x00037000

# Attempt to create a build directory.
$(shell [ -d ${BUILDDIR} ] || mkdir -p ${BUILDDIR})

# Verify if it was successful.
BUILDDIR_TEST := $(shell cd $(BUILDDIR) && /bin/pwd)
$(if $(BUILDDIR_TEST),,$(error build directory "$(BUILDDIR)" does not exist))

.PHONY: all
all: ulboot sw hw host

.PHONY: ulboot
ulboot:
	make -C ulboot V=1
	cp -a ulboot/_compiled/bin/ulboot-boot.bin $(BUILDDIR)/

.PHONY: install-ulboot
install-ulboot: $(BUILDDIR)/lpc21isp $(BUILDDIR)/ulboot-boot.bin
	lpc21isp -bin $(BUILDDIR)/ulboot-boot.bin /dev/ttyUSB0 38400 12000

.PHONY: sw
sw:
	make -C sw V=1
	cp -a sw/_compiled/bin/rocon-app.bin $(BUILDDIR)/
	cp -a sw/_compiled/bin/rocon-sdram.bin $(BUILDDIR)/

.PHONY: install-app
install-app: $(BUILDDIR)/usb_sendhex $(BUILDDIR)/rocon-app.bin 
	-usb_sendhex -d $(USB_APP_VID_PID) -r # Fails if we're already in bootloader
	sleep 1
	usb_sendhex -w -d $(USB_BOOT_VID_PID) -s $(PROG_BASE) -l $(PROG_SIZE) -e
	sleep 2
	usb_sendhex -d $(USB_BOOT_VID_PID) -s $(PROG_BASE) -f binary $(BUILDDIR)/rocon-app.bin

.PHONY: run-app
run-app:
	-usb_sendhex -d $(USB_BOOT_VID_PID) -r # FIXME: broken pipe yo-ho

.PHONY: install-sdram
install-sdram: $(BUILDDIR)/usb_sendhex $(BUILDDIR)/rocon-sdram.bin 
	-usb_sendhex -d $(USB_APP_VID_PID) -r # Fails if we're already in bootloader
	usb_sendhex -w -d $(USB_BOOT_VID_PID) -t 1 -s 0xA0000000 -f binary $(BUILDDIR)/rocon-sdram.bin

.PHONY: run-sdram
run-sdram:
	-usb_sendhex -d $(USB_BOOT_VID_PID) -g `usb_sendhex -d $(USB_BOOT_VID_PID) \
	-s 0xA0000004 -l 4 -t 1 -u -f dump - | \
	sed -n -e 's/^.*:\(..\) \(..\) \(..\) \(..\) */0x\4\3\2\1/p'`  # FIXME: broken pipe yo-ho

.PHONY: install-fpga
install-fpga: $(BUILDDIR)/usb_sendhex $(BUILDDIR)/lx-rocon.pkg
	usb_sendhex -w -d $(USB_APP_VID_PID) -t 1 -s 0xA1C00000 -f binary $(BUILDDIR)/lx-rocon.pkg
	usb_sendhex -d $(USB_APP_VID_PID) -c 0xF000

.PHONY: install-tumbl
install-tumbl: $(BUILDDIR)/usb_sendhex $(BUILDDIR)/imem.bin $(BUILDDIR)/dmem.bin
	usb_sendhex -d $(USB_APP_VID_PID) -c 0xF100 -a 0x0001
	usb_sendhex -w -d $(USB_APP_VID_PID) -t 3 -s 0x00000000 -f binary $(BUILDDIR)/imem.bin
	usb_sendhex -w -d $(USB_APP_VID_PID) -t 3 -s 0x00001000 -f binary $(BUILDDIR)/dmem.bin
	usb_sendhex -d $(USB_APP_VID_PID) -c 0xF100 -a 0x0000

.PHONY: install-firmware
install-firmware: $(BUILDDIR)/usb_sendhex $(BUILDDIR)/fw_lxmaster.bin
	usb_sendhex -w -d $(USB_APP_VID_PID) -t 1 -s 0xA1C00000 -f binary $(BUILDDIR)/fw_lxmaster.bin
	usb_sendhex -d $(USB_APP_VID_PID) -c 0xF024 -a `stat -L -c %s $(BUILDDIR)/fw_lxmaster.bin` # Upload LX MASTER firmware
	usb_sendhex -d $(USB_APP_VID_PID) -c 0xF011 -a 0 # Start IRC
	usb_sendhex -d $(USB_APP_VID_PID) -c 0xF023 -a 0 # Setup LX Master
	usb_sendhex -d $(USB_APP_VID_PID) -c 0xF021 -a 0 # Start LX Master

.PHONY: host
host:
	make -C host V=1
	cp -a host/_compiled/bin/rocon_cmd $(BUILDDIR)/
	cp -a host/_compiled/bin/usb_sendhex $(BUILDDIR)/
	cp -a host/_compiled/bin/lpc21isp $(BUILDDIR)/

.PHONY: hw
hw:
	make -C hw
	cp -a hw/_build/lx-rocon.pkg $(BUILDDIR)/
	cp -a hw/_build/imem.bin $(BUILDDIR)/
	cp -a hw/_build/dmem.bin $(BUILDDIR)/
	cp -a firmware/lxmaster.bin $(BUILDDIR)/fw_lxmaster.bin

.PHONY: clean
clean:
	make -C sw clean
	make -C host clean
	make -C hw clean
	rm -rf $(BUILDDIR)
