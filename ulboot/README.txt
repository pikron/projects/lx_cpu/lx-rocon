uLan embedded source tree v 0.8.3
=================================

uLan is message oriented multi-master communication protocol for
microcontrollers. It uses 9-bit character format for addressing
and link control and RS-485 physical layer.

  http://ulan.sourceforge.net/
  http://sourceforge.net/projects/ulan

This README is focussed on use of the uLAN
protocol in embedded systems.

The uLan embedded tree/archive provides uLan protocol
support for multiple embedded targets.
The code is used as firmware base of some experimental
and more production grade devices distributed by
PiKRON Ltd., Agrosoft Tabor s.r.o. and the Czech
Technical University in Prague.

The ul_embedded archive is more or less only
skeleton/snapshot/example of the applications
and build environment. We use SourceForge GIT
repositories directly in our real projects
and combine that with other repositories
with device specific functions.

If you intend to use uLan embedded code for
something more than structure review or simple
boot-loader compilation, it is suggested to use
full uLan GIT based source tree.

Next commands sequence can be used to obtain combined
uLAN host and embedded source tree. This is preferred
way to start the development

 ULAN_SRIPTS="http://ulan.git.sourceforge.net/git/gitweb.cgi?p=ulan/ulan;a=blob_plain;f=scripts"
 wget $ULAN_SRIPTS/get-git-ulan        -O get-git-ulan
 wget $ULAN_SRIPTS/build-ulan-host     -O build-ulan-host
 wget $ULAN_SRIPTS/build-ulan-embedded -O build-ulan-embedded
 wget $ULAN_SRIPTS/install-ul_drv      -O install-ul_drv
 chmod +x get-git-ulan build-ulan-host build-ulan-embedded install-ul_drv
 ./get-git-ulan
 ./build-ulan-host
 sudo ./install-ul_drv
 sudo modprobe ul_drv
 ./build-ulan-embedded

The top-level sources tree utilizes POSIX symbolic links
to construct whole combined sources tree. This makes
problem to use this whole tree on operating systems
which lacks these standard features (Windows).
We could look for some workaround for this problem
but it is not priority for core uLan project developers
because they use MINGW (i586-mingw32 or i586-mingw32msvc)
cross compiler to build Windows binaries in sane Linux
environment. Only uLAN driver has to be build by freely
available WDF Windows Driver Foundation tools. But WDF
build under WINE is possible and used.

The two distinct embedded microcontrollers categories
are supported by uLAN embedded protocol sources.

 * 32-bit microcontrollers supported by GCC
   ARM NXP LPC-2xxx and LPC-17xx currently used

 * 8-bit 8051 based resource constrained devices
   TI MSC1210 and Atmel AT89C51ed2 curently used

We suggest to use ARM, MIPS or may it be MSP430 for new
designs. The 8051 port is maintained mainly for completeness,
testing of the uLAN code in resource constrained
environment and because more 8051 based applications
are still in production by uLAN developers' companies.


GCC supported microcontrollers build
------------------------------------

The GCC cross compiler for targeted device is required.
The next instructions expect the use of ARM family
microcontroller. The arm-elf-gcc and arm-elf-binutils
are required to build sources. The cross compiler packages
and Linux host system installation instructions can be
found on CTU RTIME server.

  http://rtime.felk.cvut.cz/hw/index.php/Cross_compilers

Copy, link or include Makefile.gcc (in)to Makefile.rules
first. Then correct build target and configuration
has to be selected. The example configurations are found
in a corresponding board directory (i.e. board/arm/ul_usb1/config).

  cd ul_embedded-x.y
  rm Makefile.rules config.target config.omk
  cp Makefile.gcc Makefile.rules
  cp board/arm/ul_usb1/config/config.u2uv2 config.omk
  make defaul-config
  make

You should find resulting binaries in the "_compiled/bin"
directory. If application to target loader/flasher is
correctly configured then invoking "load" target in
desired application directory programs code into device.

  cd app/ulboot
  make load

The make parameter "V=1" can be used to display
more verbose output from make.

More detailed manual for OMK make system can be
found on OMK rules homepage

  http://rtime.felk.cvut.cz/omk/omk-manual.html

8-bit varinat build
-------------------

The SDCC 3.1.0 or later version is required to build the code.
The GNU make program 3.81beta3 or better is required.
The full build has been tested on Linux based hosts only
for now. The uLan protocol code has been successfully build
by Keil compiler under Windows in the past.

The archive has to be unpacked with symbolic links
for now.

Copy, link or include Makefile.sdcc (in)to Makefile.rules
first. Then correct build target and configuration
has to be selected. The example configurations are found
in a corresponding board directory (i.e. board/mcs51/ulad21/config).

  cd ul_embedded-x.y
  rm Makefile.rules config.target config.omk
  cp Makefile.sdcc Makefile.rules
  cp board/mcs51/ulad21/config/config.u2u config.omk
  make defaul-config
  make

You should find resulting binaries in the "_compiled/bin"
directory. The board and application is selected by used
"config.omk" file.

The next boards and applications are supported

Device: MCS51 TI MSC-1210
   Board: ULAD-21 - AD converter and uLan2USB converter
   Applications:
     board/mcs51/ulad21/config/config.u2u
        - uLan to USB converter
     board/mcs51/ulad21/config/config.ulboot
        - uLan enabled remote boot-loader and boot-block

   Board: HISC - Home Information System Controller
     board/mcs51/hisc/config/config.ulboot
        - boot block for distributed HISC system
     board/mcs51/hisc/config/config.blinder
        - blinder controller
     board/mcs51/hisc/config/config.heating-kswtimer
        - kettle switch timer

Device: MCS51 Atmel AT89C51RD2
     board/mcs51/ps1/config/config.u2u
        - AT89C51RD2 based uLan2USB converter

The procedure to write boot-block into ULAD-21 MSC1210 based board
requires next steps

  FLASHMSC_TTY=/dev/ttyS1
  flashmsc -d $FLASHMSC_TTY -E 0x807f
  flashmsc -d $FLASHMSC_TTY -E 0x7fff
  flashmsc -d $FLASHMSC_TTY -X 18432000 ulad21-hcr.hex
  flashmsc -d $FLASHMSC_TTY -X 18432000 ulad21-boot.hex

The "flashmsc" sources can be found at page
  http://cmp.felk.cvut.cz/~pisa/#mcuutils

To replace application in the converter or other uLan node
over uLan communication protocol, next steps are required

  echo Reset application if running
  ul_sendhex -g 0 -m 0
  echo Waiting to target to go into boot block
  echo If application blocks, reset device at this time
  echo to activate boot block now
  ul_sendhex -o 0 -m 0
  echo Found devices
  ul_sendhex -p 64
  echo Erasing flash
  ul_sendhex -m 62 -t 5 -e -s 0x2000 -l 0x6000
  sleep 2
  echo Loading application
  ul_sendhex -m 62 -t 5 application.hex

Look for uLan driver for host side (Linux, Windows, DOS)
uLan protocol driver. The uLan2USB converter requires
at least version ul_drv-0.7 release.

The code authors

  (C) Copyright 1996-2012 by Pavel Pisa
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2012 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2012 Petr Smolik

